from pathlib import Path

from restrict.compiler.ast import (
    DataConstrainedField,
    File,
    PipedExprList,
    Ref,
    Rel,
    Res,
    SecurityConstraintField,
)
from collections.abc import Mapping
from restrict.compiler.exceptions.compile import InvalidRefCollectionError
from restrict.compiler.types import (
    Datum,
    ModuleDictionary,
    Resource,
    ResourceCollection,
)
from restrict.compiler.visitors import ResolveRefTypeVisitor
from restrict.types.builtins import Boolean
from restrict.types.numeric import Integer


def vis(file: File, globs: Mapping[str, Resource | Datum]):
    return ResolveRefTypeVisitor(
        {file.path: file}, ModuleDictionary(), dict(globs), file.path
    )


def test_ref_single_path_found_from_imported_resource():
    other = Res("", "", "", False, {}, {}, {}, {}, {})
    globs = {"a": other.bridge}
    ref = Ref(["a"])
    field = SecurityConstraintField(PipedExprList([ref]))
    res = Res("", "", "", False, {}, {}, {}, {"list": field}, {})
    file = File(Path(""), [], [res])
    visitor = vis(file, globs)

    visitor.visit_file(file)

    assert ref.res == other.bridge
    assert ref.path_res == [other.bridge]


def test_ref_complex_path_found_from_imported_resource_to_resource():
    carruther = Res("", "C", "", False, {}, {}, {}, {}, {})

    c = Rel("", "", False, False, None, None, "", carruther.bridge)
    another = Res("", "A", "", False, {}, {"c": c}, {}, {}, {})

    b = Rel("", "", False, False, None, None, "", another.bridge)
    other = Res("", "O", "", False, {}, {"b": b}, {}, {}, {})

    ref = Ref(["a", "b", "c"])
    field = SecurityConstraintField(PipedExprList([ref]))
    res = Res("", "R", "", False, {}, {}, {}, {"list": field}, {})
    file = File(Path(""), [], [res])

    globs = {"a": other.bridge}
    visitor = vis(file, globs)

    visitor.visit_file(file)

    assert ref.res == carruther.bridge
    assert ref.path_res == [other.bridge, another.bridge, carruther.bridge]


def test_ref_complex_path_found_from_imported_resource_to_datum():
    d = DataConstrainedField("", "", False, False, None, PipedExprList(), Integer())
    carruther = Res("", "C", "", False, {"d": d}, {}, {}, {}, {})

    c = Rel("", "", False, False, None, None, "", carruther.bridge)
    another = Res("", "A", "", False, {}, {"c": c}, {}, {}, {})

    b = Rel("", "", False, False, None, None, "", another.bridge)
    other = Res("", "O", "", False, {}, {"b": b}, {}, {}, {})

    ref = Ref(["a", "b", "c", "d"])
    field = SecurityConstraintField(PipedExprList([ref]))
    res = Res("", "R", "", False, {}, {}, {}, {"list": field}, {})
    file = File(Path(""), [], [res])

    globs = {"a": other.bridge}
    visitor = vis(file, globs)

    visitor.visit_file(file)

    assert ref.res == Integer()
    assert ref.path_res == [other.bridge, another.bridge, carruther.bridge, Integer()]


def test_ref_complex_path_can_resolve_to_scalar():
    c = DataConstrainedField("", "", False, False, None, PipedExprList(), Boolean())
    another = Res("", "A", "", False, {"c": c}, {}, {}, {}, {})

    b = Rel("", "", False, False, None, None, "", another.bridge)
    other = Res("", "O", "", False, {}, {"b": b}, {}, {}, {})
    globs = {"a": other.bridge}
    ref = Ref(["a", "b", "c"])
    field = SecurityConstraintField(PipedExprList([ref]))
    res = Res("", "R", "", False, {}, {}, {}, {"list": field}, {})
    file = File(Path(""), [], [res])
    visitor = vis(file, globs)

    visitor.visit_file(file)

    assert ref.res == Boolean()
    assert ref.path_res == [other.bridge, another.bridge, Boolean()]


def test_ref_path_errs_on_collection_at_start_of_path():
    c = DataConstrainedField("", "", False, False, list, PipedExprList(), Boolean())
    another = Res("", "A", "", False, {"c": c}, {}, {}, {}, {})

    b = Rel("", "", False, False, None, None, "", another.bridge)
    other = Res("", "O", "", False, {}, {"b": b}, {}, {}, {})

    coll = ResourceCollection(list, other.bridge)
    ref = Ref(["a", "b", "c"])
    field = SecurityConstraintField(PipedExprList([ref]))
    res = Res("", "R", "", False, {}, {}, {}, {"list": field}, {})

    file = File(Path(""), [], [res])
    globs = {"a": coll}
    visitor = vis(file, globs)

    visitor.visit_file(file)

    assert ref.res is None
    assert ref.path_res is None

    assert len(visitor.errors) == 1
    e = visitor.errors[0]
    assert type(e) is InvalidRefCollectionError
    assert e.path == "a.b.c"
    assert e.source == file.path
    assert e.resource == "R"


def test_ref_path_errs_on_collection_mid_path():
    c = DataConstrainedField("", "", False, False, list, PipedExprList(), Boolean())
    another = Res("", "A", "", False, {"c": c}, {}, {}, {}, {})

    coll = ResourceCollection(list, another.bridge)
    b = Rel("", "", False, False, None, None, "", coll)
    other = Res("", "O", "", False, {}, {"b": b}, {}, {}, {})

    ref = Ref(["a", "b", "c"])
    field = SecurityConstraintField(PipedExprList([ref]))
    res = Res("", "R", "", False, {}, {}, {}, {"list": field}, {})

    file = File(Path(""), [], [res])
    globs = {"a": other.bridge}
    visitor = vis(file, globs)

    visitor.visit_file(file)

    print(ref.path_res)
    assert ref.res is None
    assert ref.path_res is None

    assert len(visitor.errors) == 1
    e = visitor.errors[0]
    assert type(e) is InvalidRefCollectionError
    assert e.path == "a.b.c"
    assert e.source == file.path
    assert e.resource == "R"
