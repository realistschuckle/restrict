from itertools import product
from pathlib import Path
from unittest.mock import Mock

import pytest

from restrict.compiler.ast import (
    DataComputedField,
    DataConstrainedField,
    File,
    PipedExprList,
    Rel,
    Res,
    SecurityConstraintField,
)
from restrict.compiler.ast import (
    PipedExprList as PEL,
)
from restrict.compiler.types import (
    Data,
    Datum,
    DatumTuple,
    ModuleDictionary,
    OptionalDatum,
    OptionalResource,
)
from restrict.compiler.visitors import CompileResourceVisitor
from restrict.types.builtins import Boolean
from restrict.types.numeric import Decimal_, Integer
from restrict.types.text import Email, Hash, Text
from restrict.types.time import Interval, Timestamp


def vis(root: File, *files: File):
    for res in root.resources:
        res.file = root
    for f in files:
        for res in f.resources:
            res.file = f
    asts = {root.path: root} | {f.path: f for f in files}
    mods = ModuleDictionary()
    return CompileResourceVisitor(asts, mods, {}, root.path)


def dcsf(type: Datum | None, constraint, opt=False):
    if opt and type is not None:
        type = OptionalDatum(type)
    return DataConstrainedField(
        "", "", opt, False, None, PEL(), res=type, compiled=constraint
    )


def dcpf(type: Datum | None, func, opt=False):
    if opt and type is not None:
        type = OptionalDatum(type)
    return DataComputedField(PEL(), func_res=type, compiled=func)


def res(name, dnc, fields={}, rules={}, field_sort=[]):
    return Res(
        "thing",
        name,
        "",
        False,
        fields,
        dnc,
        {},
        rules,
        {},
        used=True,
        field_sort=field_sort,
    )


def rel(bridge, is_optional=False):
    if is_optional:
        bridge = OptionalResource(bridge)
    return Rel("", "", is_optional, False, None, None, "", bridge)


@pytest.mark.parametrize(
    "t,action",
    product(
        [
            Integer(),
            Boolean(),
            Decimal_(),
            Timestamp(),
            Interval(),
            Text(),
            Hash(),
            Email(),
        ],
        ["list", "details", "create", "modify", "delete"],
    ),
)
def test_app_resource_returns_schema_for_fields(t, action):
    true_mock = Mock(return_value=True)
    t_schema = t.to_schema()
    a = dcsf(t, lambda _: True)
    b = dcpf(t, lambda d: d["self"].a + d["self"].c)
    c = dcsf(t, lambda _: True)
    sec = SecurityConstraintField(PipedExprList(), true_mock)
    rules = {action: sec}
    res = Res(
        "thing",
        "Thing",
        "",
        False,
        {"a": a, "b": b, "c": c},
        {},
        {},
        rules,
        {},
        used=True,
        field_sort=["a", "c", "b"],
    )
    file = File(Path("/root.restrict"), [], [res])
    res.file = file
    visitor = vis(file)

    visitor.visit_file(file)

    assert res.compiled is not None
    instance = res.compiled()
    assert instance._to_schema(action) == {
        "$schema": "https://json-schema.org/draft/2020-12/schema",
        "$id": "/root.restrict/Thing",
        "type": "object",
        "properties": {
            "a": t_schema | {"readOnly": False},
            "b": t_schema | {"readOnly": True},
            "c": t_schema | {"readOnly": False},
        },
        "required": ["a", "c", "b"],
    }


@pytest.mark.parametrize(
    "t,coll,action",
    product(
        [Integer(), Boolean(), Decimal_(), Timestamp(), Interval()],
        [list, set],
        ["list", "details", "create", "modify", "delete"],
    ),
)
def test_app_resource_returns_schema_for_collection_fields(t, coll, action):
    true_mock = Mock(return_value=True)
    sec = SecurityConstraintField(PipedExprList(), true_mock)
    rules = {action: sec}
    c = Data(coll, t)
    c_schema = {"type": "array", "items": t.to_schema(), "uniqueItems": False}
    if coll is set:
        c_schema["uniqueItems"] = True

    a = dcsf(c, lambda _: True)
    b = dcpf(c, lambda d: d["self"].a + 3)
    c = dcsf(c, lambda _: True, True)
    res = Res(
        "thing",
        "Thing",
        "",
        False,
        {"a": a, "b": b, "c": c},
        {},
        {},
        rules,
        {},
        used=True,
        field_sort=["a", "c", "b"],
    )
    file = File(Path("/root.restrict"), [], [res])
    res.file = file
    visitor = vis(file)

    visitor.visit_file(file)

    assert res.compiled is not None
    instance = res.compiled()
    assert instance._to_schema(action) == {
        "$schema": "https://json-schema.org/draft/2020-12/schema",
        "$id": "/root.restrict/Thing",
        "type": "object",
        "properties": {
            "a": c_schema | {"readOnly": False},
            "b": c_schema | {"readOnly": True},
            "c": c_schema | {"readOnly": False},
        },
        "required": ["a", "b"],
    }


@pytest.mark.parametrize("action", ["list", "details", "create", "modify", "delete"])
def test_app_resource_returns_schema_for_collection_of_tuples_fields(action):
    true_mock = Mock(return_value=True)
    sec = SecurityConstraintField(PipedExprList(), true_mock)
    rules = {action: sec}
    tup = DatumTuple([Integer(), Boolean()])
    c = Data(list, tup)
    c_schema = {
        "type": "array",
        "items": {
            "type": "array",
            "prefixItems": [{"type": "integer"}, {"type": "boolean"}],
            "uniqueItems": False,
        },
        "uniqueItems": False,
    }

    a = dcsf(c, lambda _: True)
    b = dcpf(c, lambda d: d["self"].a + 3)
    c = dcsf(c, lambda _: True, True)
    res = Res(
        "thing",
        "Thing",
        "",
        False,
        {"a": a, "b": b, "c": c},
        {},
        {},
        rules,
        {},
        used=True,
        field_sort=["a", "c", "b"],
    )
    file = File(Path("/root.restrict"), [], [res])
    res.file = file
    visitor = vis(file)

    visitor.visit_file(file)

    assert res.compiled is not None
    instance = res.compiled()
    assert instance._to_schema(action) == {
        "$schema": "https://json-schema.org/draft/2020-12/schema",
        "$id": "/root.restrict/Thing",
        "type": "object",
        "properties": {
            "a": c_schema | {"readOnly": False},
            "b": c_schema | {"readOnly": True},
            "c": c_schema | {"readOnly": False},
        },
        "required": ["a", "b"],
    }


@pytest.mark.parametrize("action", ["list", "details", "create", "modify", "delete"])
def test_rel_creates_schema_with_related_defs_from_same_file(action):
    true_mock = Mock(return_value=True)
    sec = SecurityConstraintField(PipedExprList(), true_mock)
    rules = {action: sec}

    yar = res("YetAnotherResource", {})
    yar_src = File(Path("/yar.restrict"), [], [yar])

    another = res("Another", {}, rules=rules)
    other = res("Other", {}, rules=rules)
    thing = res(
        "Thing",
        {"a": rel(another.bridge), "o": rel(other.bridge), "y": rel(yar.bridge)},
        rules=rules,
        field_sort=["a", "o", "y"],
    )
    file = File(Path("/root.restrict"), [], [thing, other, another])
    visitor = vis(file, yar_src)

    visitor.visit_file(file)
    visitor.visit_file(yar_src)

    assert yar.compiled is not None
    assert another.compiled is not None
    assert other.compiled is not None
    assert thing.compiled is not None
    thingy = thing.compiled()
    assert thingy._to_schema(action) == {
        "$schema": "https://json-schema.org/draft/2020-12/schema",
        "$id": "/root.restrict/Thing",
        "type": "object",
        "properties": {
            "a": {
                "$ref": "#/$defs/root.restrict/Another",
            },
            "o": {
                "$ref": "#/$defs/root.restrict/Other",
            },
            "y": {
                "$ref": "#/$defs/yar.restrict/YetAnotherResource",
            },
        },
        "required": ["a", "o", "y"],
        "$defs": {
            "root.restrict": {
                "Another": {
                    "type": "object",
                    "properties": {},
                    "required": [],
                },
                "Other": {
                    "type": "object",
                    "properties": {},
                    "required": [],
                },
            },
            "yar.restrict": {
                "YetAnotherResource": {
                    "type": "object",
                    "properties": {},
                    "required": [],
                }
            },
        },
    }


@pytest.mark.parametrize("action", ["list", "details", "create", "modify", "delete"])
def test_rel_passes_on_optionality(action):
    true_mock = Mock(return_value=True)
    sec = SecurityConstraintField(PipedExprList(), true_mock)
    rules = {action: sec}

    yar = res("YetAnotherResource", {})
    yar_src = File(Path("/yar.restrict"), [], [yar])

    another = res("Another", {}, rules=rules)
    other = res("Other", {}, rules=rules)
    thing = res(
        "Thing",
        {"a": rel(another.bridge, True), "o": rel(other.bridge), "y": rel(yar.bridge)},
        rules=rules,
        field_sort=["a", "o", "y"],
    )
    file = File(Path("/root.restrict"), [], [thing, other, another])
    visitor = vis(file, yar_src)

    visitor.visit_file(file)
    visitor.visit_file(yar_src)

    assert yar.compiled is not None
    assert another.compiled is not None
    assert other.compiled is not None
    assert thing.compiled is not None
    thingy = thing.compiled()
    assert thingy._to_schema(action) == {
        "$schema": "https://json-schema.org/draft/2020-12/schema",
        "$id": "/root.restrict/Thing",
        "type": "object",
        "properties": {
            "a": {
                "$ref": "#/$defs/root.restrict/Another",
            },
            "o": {
                "$ref": "#/$defs/root.restrict/Other",
            },
            "y": {
                "$ref": "#/$defs/yar.restrict/YetAnotherResource",
            },
        },
        "required": ["o", "y"],
        "$defs": {
            "root.restrict": {
                "Another": {
                    "type": "object",
                    "properties": {},
                    "required": [],
                },
                "Other": {
                    "type": "object",
                    "properties": {},
                    "required": [],
                },
            },
            "yar.restrict": {
                "YetAnotherResource": {
                    "type": "object",
                    "properties": {},
                    "required": [],
                }
            },
        },
    }


@pytest.mark.parametrize("action", ["list", "details", "create", "modify", "delete"])
def test_rel_creates_schema_with_nested_defs_from_same_file(action):
    true_mock = Mock(return_value=True)
    sec = SecurityConstraintField(PipedExprList(), true_mock)
    rules = {action: sec}

    yar = res("YetAnotherResource", {}, rules=rules)
    yar_src = File(Path("/yar.restrict"), [], [yar])

    another = res("Another", {"y": rel(yar.bridge)}, rules=rules, field_sort=["y"])
    other = res("Other", {"a": rel(another.bridge)}, rules=rules, field_sort=["a"])
    thing = res("Thing", {"o": rel(other.bridge)}, rules=rules, field_sort=["o"])
    file = File(Path("/root.restrict"), [], [thing, other, another])
    visitor = vis(file, yar_src)

    visitor.visit_file(file)
    visitor.visit_file(yar_src)

    assert yar.compiled is not None
    assert another.compiled is not None
    assert other.compiled is not None
    assert thing.compiled is not None
    thingy = thing.compiled()
    assert thingy._to_schema(action) == {
        "$schema": "https://json-schema.org/draft/2020-12/schema",
        "$id": "/root.restrict/Thing",
        "type": "object",
        "properties": {
            "o": {
                "$ref": "#/$defs/root.restrict/Other",
            },
        },
        "required": ["o"],
        "$defs": {
            "root.restrict": {
                "Another": {
                    "type": "object",
                    "properties": {
                        "y": {
                            "$ref": "#/$defs/yar.restrict/YetAnotherResource",
                        },
                    },
                    "required": ["y"],
                },
                "Other": {
                    "type": "object",
                    "properties": {
                        "a": {
                            "$ref": "#/$defs/root.restrict/Another",
                        },
                    },
                    "required": ["a"],
                },
            },
            "yar.restrict": {
                "YetAnotherResource": {
                    "type": "object",
                    "properties": {},
                    "required": [],
                }
            },
        },
    }


@pytest.mark.parametrize("action", ["list", "details", "create", "modify", "delete"])
def test_schema_returns_only_properties_under_security_for_action(action):
    true_mock = Mock(return_value=True)
    false_mock = Mock(return_value=False)
    false_sec = SecurityConstraintField(PipedExprList(), false_mock)
    true_sec = SecurityConstraintField(PipedExprList(), true_mock)
    rules = {action: {"a": false_sec, "b": true_sec, "c": false_sec}}
    a = dcsf(Integer(), lambda _: True)
    b = dcpf(Integer(), lambda d: d["self"].a + d["self"].c)
    c = dcsf(Integer(), lambda _: True)
    res = Res(
        "thing",
        "Thing",
        "",
        False,
        {"a": a, "b": b, "c": c},
        {},
        {},
        rules,  # type: ignore
        {},
        used=True,
        field_sort=["a", "c", "b"],
    )
    file = File(Path("/root.restrict"), [], [res])
    res.file = file
    visitor = vis(file)

    visitor.visit_file(file)

    assert res.compiled is not None
    instance = res.compiled()
    assert instance._to_schema(action) == {
        "$schema": "https://json-schema.org/draft/2020-12/schema",
        "$id": "/root.restrict/Thing",
        "type": "object",
        "properties": {
            "b": {"type": "integer", "readOnly": True},
        },
        "required": ["b"],
    }
    true_mock.assert_called()
    false_mock.assert_called()


@pytest.mark.parametrize("action", ["list", "details", "create", "modify", "delete"])
def test_schema_returns_no_properties_because_security_defaults_to_false(action):
    a = dcsf(Integer(), lambda _: True)
    b = dcpf(Integer(), lambda d: d["self"].a + d["self"].c)
    c = dcsf(Integer(), lambda _: True)
    res = Res(
        "thing", "Thing", "", False, {"a": a, "b": b, "c": c}, {}, {}, {}, {}, used=True
    )
    file = File(Path("/root.restrict"), [], [res])
    res.file = file
    visitor = vis(file)

    visitor.visit_file(file)

    assert res.compiled is not None
    instance = res.compiled()
    assert instance._to_schema(action) == {
        "$schema": "https://json-schema.org/draft/2020-12/schema",
        "$id": "/root.restrict/Thing",
        "type": "object",
        "properties": {},
        "required": [],
    }
