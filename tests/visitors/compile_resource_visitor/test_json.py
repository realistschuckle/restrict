from itertools import product
from pathlib import Path
from unittest.mock import Mock

import pytest

from restrict.compiler.ast import (
    DataComputedField,
    DataConstrainedField,
    EffectsComputedField,
    File,
    PipedExprList,
    Rel,
    Res,
    SecurityConstraintField,
)
from restrict.compiler.ast import (
    PipedExprList as PEL,
)
from restrict.compiler.types import (
    Data,
    Datum,
    DatumTuple,
    ModuleDictionary,
    OptionalDatum,
)
from restrict.compiler.visitors import CompileResourceVisitor
from restrict.types.builtins import Boolean
from restrict.types.numeric import Integer


def vis(root: File, *files: File):
    for res in root.resources:
        res.file = root
    for f in files:
        for res in f.resources:
            res.file = f
    asts = {root.path: root} | {f.path: f for f in files}
    mods = ModuleDictionary()
    return CompileResourceVisitor(asts, mods, {}, root.path)


def dcsf(type: Datum | None, constraint, opt=False):
    if opt and type is not None:
        type = OptionalDatum(type)
    return DataConstrainedField(
        "", "", opt, False, None, PEL(), res=type, compiled=constraint
    )


def dcpf(type: Datum | None, func, opt=False):
    if opt and type is not None:
        type = OptionalDatum(type)
    return DataComputedField(PEL(), func_res=type, compiled=func)


def res(name, dnc, fields={}, rules={}, effects={}):
    return Res("thing", name, "", False, fields, dnc, effects, rules, {}, used=True)


def rel(bridge):
    return Rel("", "", False, False, None, None, "", bridge)


@pytest.mark.parametrize(
    "action",
    ["list", "details"],
)
def test_app_resource_returns_json_for_fields(action):
    true_mock = Mock(return_value=True)
    a = dcsf(Integer(), lambda _: True)
    b = dcpf(Integer(), lambda d: d["self"].a + d["self"].c)
    c = dcsf(Integer(), lambda _: True)
    sec = SecurityConstraintField(PipedExprList(), true_mock)
    rules = {action: sec, "create": sec}
    res = Res(
        "thing",
        "Thing",
        "",
        False,
        {"a": a, "b": b, "c": c},
        {},
        {},
        rules,
        {},
        used=True,
        field_sort=["a", "c", "b"],
    )
    file = File(Path("/root.restrict"), [], [res])
    res.file = file
    visitor = vis(file)

    visitor.visit_file(file)

    assert res.compiled is not None
    instance = res.compiled._create({"a": 1, "c": 1})
    assert instance._to_json(action) == {"a": 1, "b": 2, "c": 1}


@pytest.mark.parametrize(
    "coll,action",
    product(
        [list, set],
        ["list", "details"],
    ),
)
def test_app_resource_returns_json_for_collection_fields(coll, action):
    true_mock = Mock(return_value=True)
    sec = SecurityConstraintField(PipedExprList(), true_mock)
    rules = {action: sec, "create": sec}
    c = Data(coll, Integer())

    a = dcsf(c, lambda _: True)
    b = dcpf(c, lambda d: coll(map(lambda x: x + d["self"].c, d["self"].a)))
    c = dcsf(Integer(), lambda _: True, True)
    res = Res(
        "thing",
        "Thing",
        "",
        False,
        {"a": a, "b": b, "c": c},
        {},
        {},
        rules,
        {},
        used=True,
        field_sort=["a", "c", "b"],
    )
    file = File(Path("/root.restrict"), [], [res])
    res.file = file
    visitor = vis(file)

    visitor.visit_file(file)

    assert res.compiled is not None
    instance = res.compiled._create({"a": [1, 2, 4, 1], "c": 4})
    assert instance._to_json(action) == {
        "a": list(coll([1, 2, 4, 1])),
        "b": list(coll([5, 6, 8, 5])),
        "c": 4,
    }


@pytest.mark.parametrize("action", ["list", "details"])
def test_app_resource_returns_json_for_collection_of_tuples_fields(action):
    true_mock = Mock(return_value=True)
    sec = SecurityConstraintField(PipedExprList(), true_mock)
    rules = {action: sec, "create": sec}
    tup = DatumTuple([Integer(), Boolean()])
    c = Data(list, tup)

    a = dcsf(c, lambda _: True)
    b = dcpf(
        Data(list, Integer()),
        lambda d: map(
            lambda x: x[0] + d["self"].c if x[1] else x[0] - d["self"].c, d["self"].a
        ),
    )
    c = dcsf(Integer(), lambda _: True, True)
    res = Res(
        "thing",
        "Thing",
        "",
        False,
        {"a": a, "b": b, "c": c},
        {},
        {},
        rules,
        {},
        used=True,
        field_sort=["a", "c", "b"],
    )
    file = File(Path("/root.restrict"), [], [res])
    res.file = file
    visitor = vis(file)

    visitor.visit_file(file)

    assert res.compiled is not None
    instance = res.compiled._create({"a": [[1, True], [2, False], [3, True]], "c": 1})
    assert instance._to_json(action) == {
        "a": [[1, True], [2, False], [3, True]],
        "b": [2, 1, 4],
        "c": 1,
    }


@pytest.mark.parametrize("action", ["list", "details"])
def test_rel_creates_json_with_related_defs_from_same_file(action):
    true_mock = Mock(return_value=True)
    sec = SecurityConstraintField(PipedExprList(), true_mock)
    rules = {action: sec, "modify": sec}

    yar = res("YetAnotherResource", {})
    yar_src = File(Path("/yar.restrict"), [], [yar])

    a = dcsf(Integer(), lambda _: True, False)
    another = res("Another", {}, fields={"a": a}, rules=rules)
    another.field_sort = ["a"]
    o = dcsf(Integer(), lambda _: True, False)
    other = res("Other", {}, fields={"o": o}, rules=rules)
    other.field_sort = ["o"]
    thing = res(
        "Thing",
        {"a": rel(another.bridge), "o": rel(other.bridge), "y": rel(yar.bridge)},
        rules=rules,
    )
    thing.field_sort = ["a", "o", "y"]
    file = File(Path("/root.restrict"), [], [thing, other, another])
    visitor = vis(file, yar_src)

    visitor.visit_file(file)
    visitor.visit_file(yar_src)

    assert yar.compiled is not None
    assert another.compiled is not None
    assert other.compiled is not None
    assert thing.compiled is not None
    thingy = thing.compiled._modify({}, {"a": {"a": 1}, "o": {"o": 2}, "y": {}})
    assert thingy._to_json(action) == {"a": {"a": 1}, "o": {"o": 2}, "y": {}}


@pytest.mark.parametrize("action", ["list", "details"])
def test_rel_creates_schema_with_nested_defs_from_same_file(action):
    true_mock = Mock(return_value=True)
    sec = SecurityConstraintField(PipedExprList(), true_mock)
    rules = {action: sec, "create": sec}

    yar = res("YetAnotherResource", {}, rules=rules)
    yar_src = File(Path("/yar.restrict"), [], [yar])

    another = res("Another", {"y": rel(yar.bridge)}, rules=rules)
    another.field_sort = ["y"]
    other = res("Other", {"a": rel(another.bridge)}, rules=rules)
    other.field_sort = ["a"]
    thing = res("Thing", {"o": rel(other.bridge)}, rules=rules)
    thing.field_sort = ["o"]
    file = File(Path("/root.restrict"), [], [thing, other, another])
    visitor = vis(file, yar_src)

    visitor.visit_file(file)
    visitor.visit_file(yar_src)

    assert yar.compiled is not None
    assert another.compiled is not None
    assert other.compiled is not None
    assert thing.compiled is not None
    thingy = thing.compiled._create({"o": {"a": {"y": {}}}})
    assert thingy._to_json(action) == {"o": {"a": {"y": {}}}}


@pytest.mark.parametrize("action", ["list", "details"])
def test_schema_returns_only_properties_under_security_for_action(action):
    true_mock = Mock(return_value=True)
    false_mock = Mock(return_value=False)
    false_sec = SecurityConstraintField(PipedExprList(), false_mock)
    true_sec = SecurityConstraintField(PipedExprList(), true_mock)
    rules = {
        action: {"a": false_sec, "b": true_sec, "c": false_sec},
        "modify": true_sec,
    }
    a = dcsf(Integer(), lambda _: True)
    b = dcpf(Integer(), lambda d: d["self"].a + d["self"].c)
    c = dcsf(Integer(), lambda _: True)
    res = Res(
        "thing",
        "Thing",
        "",
        False,
        {"a": a, "b": b, "c": c},
        {},
        {},
        rules,  # type: ignore
        {},
        used=True,
        field_sort=["a", "c", "b"],
    )
    file = File(Path("/root.restrict"), [], [res])
    res.file = file
    visitor = vis(file)

    visitor.visit_file(file)

    assert res.compiled is not None
    instance = res.compiled._modify({"a": 10, "c": 12}, {})
    assert instance._to_json(action) == {"b": 22}


def test_json_returns_no_properties_because_security_defaults_to_false():
    a = dcsf(Integer(), lambda _: True)
    b = dcpf(Integer(), lambda d: d["self"].a + d["self"].c)
    c = dcsf(Integer(), lambda _: True)
    res = Res(
        "thing",
        "Thing",
        "",
        False,
        {"a": a, "b": b, "c": c},
        {},
        {},
        {"create": SecurityConstraintField(PipedExprList(), lambda _: True)},
        {},
        used=True,
        field_sort=["a", "c", "b"],
    )
    file = File(Path("/root.restrict"), [], [res])
    res.file = file
    visitor = vis(file)

    visitor.visit_file(file)

    assert res.compiled is not None

    instance = res.compiled._create({"a": 10, "c": 10})
    assert instance._to_json("details") == {}


def test_json_shows_effects_on_rel():
    sec = SecurityConstraintField(PipedExprList(), lambda _: True)
    rules = {"create": sec, "list": sec}
    other = res("Other", {}, fields={"o": dcsf(Integer(), lambda _: True)}, rules=rules)
    other.field_sort = ["o"]
    thing = res(
        "Thing",
        {"a": rel(other.bridge)},
        rules=rules,
        effects={
            "create": {
                "a": EffectsComputedField(
                    PipedExprList(),
                    None,
                    lambda d: d[other.bridge.compiled_name()]._create({"o": 10}),
                )
            }
        },
    )
    thing.field_sort = ["a"]
    file = File(Path("/root.restrict"), [], [other, thing])
    visitor = vis(file)
    visitor.visit_file(file)

    assert thing.compiled is not None
    instance = thing.compiled._create({})

    assert instance._to_json("list") == {"a": {"o": 10}}
