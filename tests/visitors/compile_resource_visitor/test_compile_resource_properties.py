from pathlib import Path
from unittest.mock import Mock

import pytest

from restrict.compiler.ast import (
    DataComputedField,
    DataConstrainedField,
    EffectsComputedField,
    File,
    PipedExprList,
    Res,
    SecurityConstraintField,
)
from restrict.compiler.ast import (
    PipedExprList as PEL,
)
from restrict.compiler.compiled import AppResource
from restrict.compiler.exceptions.compile import CompileError
from restrict.compiler.exceptions.runtime import (
    ComputedFieldAssignmentError,
    InvalidPropertyValueError,
    PropertyValueFailsContraintError,
    RestrictRuntimeErrorGroup,
    UnboundPropertyError,
)
from restrict.compiler.types import (
    Datum,
    ModuleDictionary,
    OptionalDatum,
)
from restrict.compiler.visitors import CompileResourceVisitor
from restrict.types.numeric import Decimal_, Integer


def vis(root: File, *files: File):
    asts = {root.path: root} | {f.path: f for f in files}
    mods = ModuleDictionary()
    return CompileResourceVisitor(asts, mods, {}, root.path)


def dcsf(type: Datum | None, constraint, opt=False):
    if opt and type is not None:
        type = OptionalDatum(type)
    return DataConstrainedField(
        "", "", opt, False, None, PEL(), res=type, compiled=constraint
    )


def dcpf(type: Datum | None, func, opt=False):
    if opt and type is not None:
        type = OptionalDatum(type)
    return DataComputedField(PEL(), func_res=type, compiled=func)


def res_(fields, security={}, field_sort=None, name=None):
    if name is None:
        name = "Thing"
    if field_sort is None:
        field_sort = [a for a in fields]
    return Res(
        "thing",
        name,
        "",
        False,
        fields,
        {},
        {},
        security,
        {},
        used=True,
        field_sort=field_sort,
    )


def test_visitor_does_not_compile_unused_resources():
    field = dcsf(None, lambda _: True)
    res = Res("thing", "Thing", "", False, {"a": field}, {}, {}, {}, {})
    file = File(Path("/root.restrict"), [], [res])
    res.file = file
    visitor = vis(file)

    visitor.visit_file(file)

    assert len(visitor.errors) == 0


def test_dcsf_with_no_res_raises_error():
    field = dcsf(None, lambda _: True)
    res = res_({"a": field})
    file = File(Path("/root.restrict"), [], [res])
    res.file = file
    visitor = vis(file)

    visitor.visit_file(file)

    assert len(visitor.errors) == 1
    assert type(visitor.errors[0]) is CompileError
    assert (
        visitor.errors[0].message
        == "Cannot compile /root.restrict/Thing: a does not have a resovled type"
    )


def test_dcsf_with_no_compiled_constraint_raises_error():
    field = dcsf(Integer(), None)
    res = res_({"a": field})
    file = File(Path("/root.restrict"), [], [res])
    res.file = file
    visitor = vis(file)

    visitor.visit_file(file)

    assert len(visitor.errors) == 1
    assert type(visitor.errors[0]) is CompileError


def test_compiled_object_has_proper_names():
    field = dcsf(Integer(), lambda _: True)
    res = res_({"a": field})
    file = File(Path("/root.restrict"), [], [res])
    res.file = file
    visitor = vis(file)

    visitor.visit_file(file)

    assert res.compiled is not None
    assert res.compiled.__name__ == "Thing"
    assert res.compiled.__module__ == "/root.restrict"
    assert res.compiled.__qualname__ == "/root.restrict/Thing"


def test_compiles_simple_data_constrained_field_to_property():
    field = dcsf(Integer(), lambda _: True)
    other = res_(
        {"a": field},
        {
            "create": SecurityConstraintField(PEL(), lambda _: True),
            "modify": SecurityConstraintField(PEL(), lambda _: True),
            "details": SecurityConstraintField(PEL(), lambda _: True),
        },
        name="Other",
    )
    res = res_(
        {"a": field},
        {
            "create": SecurityConstraintField(PEL(), lambda _: True),
            "modify": SecurityConstraintField(PEL(), lambda _: True),
            "details": SecurityConstraintField(PEL(), lambda _: True),
        },
    )
    file = File(Path("/root.restrict"), [], [res, other])
    res.file = file
    other.file = file
    visitor = vis(file)

    visitor.visit_file(file)

    for r in [res, other]:
        assert r.compiled is not None
        assert issubclass(r.compiled, AppResource)
        assert r.compiled.__name__ == r.name
        assert r.compiled.__qualname__ == f"/root.restrict/{r.name}"

        instance = r.compiled()
        assert not instance._get_underlying_spec("a").bound  # type: ignore
        with pytest.raises(UnboundPropertyError) as exc_info:
            instance.a  # type: ignore

        assert exc_info.value.to_json() == {
            "type": "UnboundPropertyError",
            "is_fatal": True,
            "res": r.compiled.__qualname__,
            "name": "a",
        }

        instance._set_property_values("create", {"a": 1}, {}, [], [])  # type: ignore
        assert instance._get_underlying_spec("a").bound  # type: ignore
        assert instance.a == 1  # type: ignore

        instance = r.compiled._create({"a": 0})
        instance.a = 2  # type: ignore
        assert instance._get_underlying_spec("a").bound  # type: ignore


@pytest.mark.parametrize("value", [1.0, "abc", [], {}])
def test_set_field_to_bad_value_raises_value_error(value):
    field = dcsf(Integer(), lambda _: True)
    res = res_(
        {"a": field},
        {
            "modify": SecurityConstraintField(PEL(), lambda _: True),
        },
    )
    file = File(Path("/root.restrict"), [], [res])
    res.file = file
    visitor = vis(file)
    visitor.visit_file(file)

    assert res.compiled is not None
    instance = res.compiled()

    with pytest.raises(InvalidPropertyValueError) as exc_info:
        instance.a = value  # type: ignore

    assert exc_info.value.to_json() == {
        "type": "InvalidPropertyValueError",
        "msg": exc_info.value.msg,
        "is_fatal": True,
        "name": "a",
        "res": res.compiled.__qualname__,
        "value": value,
    }


def test_compiles_data_constrained_field_with_its_constraint():
    field = dcsf(Integer(), lambda d: d["value"] > 3)
    res = res_(
        {"a": field},
        {
            "create": SecurityConstraintField(PEL(), lambda _: True),
            "modify": SecurityConstraintField(PEL(), lambda _: True),
        },
    )
    file = File(Path("/root.restrict"), [], [res])
    res.file = file
    visitor = vis(file)

    visitor.visit_file(file)

    assert res.compiled is not None
    instance = res.compiled._create({"a": 10})
    with pytest.raises(PropertyValueFailsContraintError) as exc_info:
        instance.a = 0  # type: ignore

    assert exc_info.value.name == "a"
    assert exc_info.value.value == 0
    assert exc_info.value.res == res.bridge.compiled_name()


def test_raises_error_when_required_field_is_not_set():
    field = dcsf(Integer(), lambda d: d["value"] > 3)
    res = res_(
        {"a": field},
        {
            "create": SecurityConstraintField(PEL(), lambda _: True),
            "modify": SecurityConstraintField(PEL(), lambda _: True),
        },
    )
    file = File(Path("/root.restrict"), [], [res])
    res.file = file
    visitor = vis(file)

    visitor.visit_file(file)

    assert res.compiled is not None
    with pytest.raises(RestrictRuntimeErrorGroup) as exc_info:
        res.compiled._create({})

    assert exc_info.value.to_json() == {
        "type": "RestrictRuntimeErrorGroup",
        "is_fatal": True,
        "errors": [
            {
                "type": "UnboundPropertyError",
                "is_fatal": True,
                "name": "a",
                "res": "/root.restrict/Thing",
            }
        ],
    }


def test_compiles_data_constrained_field_optional_check():
    field = dcsf(Integer(), lambda _: True, True)
    res = res_(
        {"a": field},
        {
            "modify": SecurityConstraintField(PEL(), lambda _: True),
        },
    )
    file = File(Path("/root.restrict"), [], [res])
    res.file = file
    visitor = vis(file)

    visitor.visit_file(file)

    assert res.compiled is not None
    instance = res.compiled()

    assert instance.a is None  # type: ignore
    instance.a = None  # type: ignore


def test_compiles_data_computed_field():
    a = dcsf(Integer(), lambda _: True)
    b = dcpf(Decimal_(), lambda d: d["self"].a + 3.2)
    res = res_(
        {"a": a, "b": b},
        {
            "create": SecurityConstraintField(PEL(), lambda _: True),
            "modify": SecurityConstraintField(PEL(), lambda _: True),
        },
    )
    file = File(Path("/root.restrict"), [], [res])
    res.file = file
    visitor = vis(file)

    visitor.visit_file(file)

    assert res.compiled is not None
    instance = res.compiled()

    with pytest.raises(UnboundPropertyError):
        instance.b  # type: ignore

    with pytest.raises(ComputedFieldAssignmentError) as exc_info:
        instance.b = 2  # type: ignore

    assert exc_info.value.name == "b"
    assert exc_info.value.res == res.bridge.compiled_name()

    instance.a = 1  # type: ignore
    assert instance.b == 4.2  # type: ignore


def test_compiles_data_returns_none_if_underlying_field_is_optional():
    def expr(d):
        def op(a, b):
            if a is not None and b is not None:
                return a + b

        return op(d["self"].a, 3)

    a = dcsf(Integer(), lambda _: True, True)
    b = dcpf(Integer(), expr, True)
    res = res_(
        {"a": a, "b": b},
        {
            "modify": SecurityConstraintField(PEL(), lambda _: True),
        },
    )
    file = File(Path("/root.restrict"), [], [res])
    res.file = file
    visitor = vis(file)

    visitor.visit_file(file)

    assert res.compiled is not None
    instance = res.compiled()
    instance.a = None  # type: ignore
    assert instance.b is None  # type: ignore


def actual_sum(d):
    def _do(a0, a1):
        if a0 is not None and a1 is not None:
            return a0 + a1

    return _do(d["self"].a, d["self"].c)


def actual_cx(d):
    def _do(a0):
        if a0 is not None:
            return a0 >= 0
        else:
            return True

    return _do(d["value"])


def test_app_resource_works_for_validator_on_optional_field():
    t = Integer()
    a = dcsf(t, lambda d: d["value"] >= 0)
    b = dcpf(t, actual_sum, True)
    c = dcsf(t, actual_cx, True)
    res = res_(
        {"a": a, "b": b, "c": c},
        {
            "create": SecurityConstraintField(PEL(), lambda _: True),
            "details": SecurityConstraintField(PEL(), lambda _: True),
        },
        ["a", "c", "b"],
    )
    file = File(Path("/root.restrict"), [], [res])
    res.file = file
    visitor = vis(file)
    visitor.visit_file(file)
    assert res.compiled is not None
    instance = res.compiled._create({"a": 1, "c": None})

    assert instance._to_json("details") == {"a": 1}


def test_app_resource_turns_into_json_when_all_fields_are_set():
    t = Integer()
    a = dcsf(t, lambda d: d["value"] >= 0)
    b = dcpf(t, lambda d: d["self"].a + d["self"].c, True)
    c = dcsf(t, lambda _: True, True)
    res = res_(
        {"a": a, "b": b, "c": c},
        {
            "create": SecurityConstraintField(PEL(), lambda _: True),
            "details": SecurityConstraintField(PEL(), lambda _: True),
        },
        ["a", "c", "b"],
    )
    file = File(Path("/root.restrict"), [], [res])
    res.file = file
    visitor = vis(file)
    visitor.visit_file(file)
    assert res.compiled is not None
    instance = res.compiled()

    instance._set_property_values("create", {"a": 1, "c": 3}, {}, [], [])  # type: ignore

    assert instance._to_json("details") == {"a": 1, "b": 4, "c": 3}


def test_app_resource_turns_into_json_elides_optional_fields_with_value_none():
    t = Integer()
    a = dcsf(t, lambda _: True)
    b = dcpf(t, actual_sum, True)
    c = dcsf(t, lambda _: True, True)
    res = res_(
        {"a": a, "b": b, "c": c},
        {
            "create": SecurityConstraintField(PEL(), lambda _: True),
            "details": SecurityConstraintField(PEL(), lambda _: True),
        },
        ["a", "c", "b"],
    )
    file = File(Path("/root.restrict"), [], [res])
    res.file = file
    visitor = vis(file)
    visitor.visit_file(file)
    assert res.compiled is not None
    instance = res.compiled()

    instance._set_property_values("create", {"a": 1}, {}, [], [])

    assert instance._to_json("details") == {"a": 1}


def test_setting_computed_field_results_in_warning():
    t = Integer()
    a = dcsf(t, lambda _: True)
    b = dcpf(t, actual_sum, True)
    c = dcsf(t, lambda _: True, True)
    res = res_(
        {"a": a, "b": b, "c": c},
        field_sort=["a", "c", "b"],
    )
    file = File(Path("/root.restrict"), [], [res])
    res.file = file
    visitor = vis(file)
    visitor.visit_file(file)
    assert res.compiled is not None
    instance = res.compiled()
    errors = []

    instance._set_property_values("create", {"a": 1, "b": 10}, {}, errors, [])

    assert len(errors) == 1
    e = errors[0]
    assert type(e) is ComputedFieldAssignmentError
    assert e.is_fatal is False
    assert e.name == "b"
    assert e.res == res.compiled.__qualname__


def d_greater_than_b(d):
    return d["value"] > d["self"].b


def test_setting_fields_runs_constraint_validation_on_other_fields():
    sec = SecurityConstraintField(PipedExprList(), lambda _: True)
    t = Integer()
    a = dcsf(t, lambda _: True)
    b = dcpf(t, actual_sum)
    c = dcsf(t, lambda _: True)
    d = dcsf(t, d_greater_than_b)
    fields = {"a": a, "b": b, "c": c, "d": d}
    res = res_(
        fields,
        {"modify": sec, "details": sec},
        ["a", "c", "d", "b"],
    )
    file = File(Path("/root.restrict"), [], [res])
    res.file = file
    visitor = vis(file)
    visitor.visit_file(file)

    assert res.compiled is not None

    with pytest.raises(RestrictRuntimeErrorGroup) as exc_info:
        res.compiled._modify({"a": 1, "c": 3, "d": 5}, {"a": 1, "c": 8})

    assert exc_info.value.to_json() == {
        "type": "RestrictRuntimeErrorGroup",
        "is_fatal": True,
        "errors": [
            {
                "type": "PropertyValueFailsContraintError",
                "is_fatal": True,
                "name": "d",
                "res": res.compiled.__qualname__,
                "value": 5,
            }
        ],
    }


def test_effects_run_for_unspecified_values_in_create():
    not_run_func = Mock(return_value=1)
    run_func = Mock(return_value=1)
    effects = {
        "a": EffectsComputedField(PEL(), None, not_run_func),
        "c": EffectsComputedField(PEL(), None, run_func),
    }
    t = Integer()
    a = dcsf(t, lambda _: True)
    b = dcpf(t, actual_sum, True)
    c = dcsf(t, lambda _: True, True)
    res = Res(
        "thing",
        "Thing",
        "",
        False,
        {"a": a, "b": b, "c": c},
        {},
        {"create": effects},
        {
            "create": SecurityConstraintField(PEL(), lambda _: True),
            "details": SecurityConstraintField(PEL(), lambda _: True),
        },
        {},
        used=True,
        field_sort=["a", "c", "b"],
    )
    file = File(Path("/root.restrict"), [], [res])
    res.file = file
    visitor = vis(file)
    visitor.visit_file(file)
    assert res.compiled is not None

    instance = res.compiled._create({"a": 1})

    assert instance._to_json("details") == {"a": 1, "c": 1, "b": 2}
    run_func.assert_called()
    not_run_func.assert_not_called()


def test_effects_run_for_unspecified_values_in_modify():
    not_run_func = Mock(return_value=1)
    run_func = Mock(return_value=1)
    effects = {
        "a": EffectsComputedField(PEL(), None, not_run_func),
        "c": EffectsComputedField(PEL(), None, run_func),
    }
    t = Integer()
    a = dcsf(t, lambda _: True)
    b = dcpf(t, actual_sum, True)
    c = dcsf(t, lambda _: True, True)
    res = Res(
        "thing",
        "Thing",
        "",
        False,
        {"a": a, "b": b, "c": c},
        {},
        {"modify": effects},
        {
            "modify": SecurityConstraintField(PEL(), lambda _: True),
            "details": SecurityConstraintField(PEL(), lambda _: True),
        },
        {},
        used=True,
        field_sort=["a", "c", "b"],
    )
    file = File(Path("/root.restrict"), [], [res])
    res.file = file
    visitor = vis(file)
    visitor.visit_file(file)
    assert res.compiled is not None

    instance = res.compiled._modify({}, {"a": 1})

    assert instance._to_json("details") == {"a": 1, "c": 1, "b": 2}
    run_func.assert_called()
    not_run_func.assert_not_called()


def test_security_strips_property_value_from_create_submission():
    func = Mock(return_value=False)
    rule = SecurityConstraintField(PEL(), func)
    t = Integer()
    a = dcsf(t, lambda _: True)
    b = dcpf(t, actual_sum, True)
    c = dcsf(t, lambda _: True, True)
    res = res_(
        {"a": a, "b": b, "c": c},
        {"create": rule},
        ["a", "c", "b"],
    )
    file = File(Path("/root.restrict"), [], [res])
    res.file = file
    visitor = vis(file)
    visitor.visit_file(file)
    assert res.compiled is not None

    with pytest.raises(RestrictRuntimeErrorGroup) as exc_info:
        res.compiled._create({"a": 1, "c": 1})

    assert exc_info.value.to_json() == {
        "type": "RestrictRuntimeErrorGroup",
        "is_fatal": True,
        "errors": [
            {
                "type": "SecurityPreventedFieldAssignmentError",
                "is_fatal": False,
                "name": "a",
                "value": 1,
                "res": "/root.restrict/Thing",
            },
            {
                "type": "SecurityPreventedFieldAssignmentError",
                "is_fatal": False,
                "name": "c",
                "value": 1,
                "res": "/root.restrict/Thing",
            },
            {
                "type": "UnboundPropertyError",
                "is_fatal": True,
                "name": "a",
                "res": "/root.restrict/Thing",
            },
        ],
    }
    func.assert_called()


def test_security_strips_property_value_from_create_submission_for_single_property():
    func = Mock(return_value=True)
    rule = SecurityConstraintField(PEL(), func)
    t = Integer()
    a = dcsf(t, lambda _: True)
    b = dcpf(t, actual_sum, True)
    c = dcsf(t, lambda _: True, True)
    res = res_(
        {"a": a, "b": b, "c": c},
        {"create": {"c": rule}},
        ["a", "c", "b"],
    )
    file = File(Path("/root.restrict"), [], [res])
    res.file = file
    visitor = vis(file)
    visitor.visit_file(file)
    assert res.compiled is not None

    with pytest.raises(RestrictRuntimeErrorGroup) as exc_info:
        res.compiled._create({"a": 1, "c": 1})

    assert exc_info.value.to_json() == {
        "type": "RestrictRuntimeErrorGroup",
        "is_fatal": True,
        "errors": [
            {
                "type": "SecurityPreventedFieldAssignmentError",
                "is_fatal": False,
                "name": "a",
                "value": 1,
                "res": "/root.restrict/Thing",
            },
            {
                "type": "UnboundPropertyError",
                "is_fatal": True,
                "name": "a",
                "res": "/root.restrict/Thing",
            },
        ],
    }
    func.assert_called()


def test_security_strips_property_value_from_modify_submission_for_single_property():
    func = Mock(return_value=True)
    rule = SecurityConstraintField(PEL(), func)
    t = Integer()
    a = dcsf(t, lambda _: True)
    b = dcpf(t, actual_sum, True)
    c = dcsf(t, lambda _: True, True)
    res = res_(
        {"a": a, "b": b, "c": c},
        {"modify": {"c": rule}},
        ["a", "c", "b"],
    )
    file = File(Path("/root.restrict"), [], [res])
    res.file = file
    visitor = vis(file)
    visitor.visit_file(file)
    assert res.compiled is not None

    with pytest.raises(RestrictRuntimeErrorGroup) as exc_info:
        res.compiled._modify({"a": 10}, {"a": 1, "c": 1})

    assert exc_info.value.to_json() == {
        "is_fatal": False,
        "type": "RestrictRuntimeErrorGroup",
        "errors": [
            {
                "is_fatal": False,
                "name": "a",
                "res": "/root.restrict/Thing",
                "type": "SecurityPreventedFieldAssignmentError",
                "value": 1,
            },
        ],
    }
    func.assert_called()


def test_security_strips_property_from_json_details():
    tm = Mock(return_value=True)
    fm = Mock(return_value=False)
    frule = SecurityConstraintField(PEL(), fm)
    trule = SecurityConstraintField(PEL(), tm)
    t = Integer()
    a = dcsf(t, lambda _: True)
    b = dcpf(t, actual_sum, True)
    c = dcsf(t, lambda _: True, True)
    res = res_(
        {"a": a, "b": b, "c": c},
        {"details": {"a": frule, "b": trule, "c": trule}, "modify": trule},
        ["a", "c", "b"],
    )
    file = File(Path("/root.restrict"), [], [res])
    res.file = file
    visitor = vis(file)
    visitor.visit_file(file)
    assert res.compiled is not None

    assert res.compiled._modify({}, {"a": 1, "c": 1})._to_json("details") == {
        "b": 2,
        "c": 1,
    }
