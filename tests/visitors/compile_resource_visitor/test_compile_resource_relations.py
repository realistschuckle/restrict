from decimal import Decimal
from pathlib import Path

import pytest

from restrict.compiler.ast import (
    DataComputedField,
    DataConstrainedField,
    File,
    Rel,
    Res,
    Rules,
    SecurityConstraintField,
)
from restrict.compiler.ast import (
    PipedExprList as PEL,
)
from restrict.compiler.compiled import RelSpec
from restrict.compiler.exceptions.runtime import (
    RestrictRuntimeErrorGroup,
    UnboundPropertyError,
)
from restrict.compiler.types import (
    ModuleDictionary,
    OptionalDatum,
    OptionalResource,
)
from restrict.compiler.visitors import CompileResourceVisitor
from restrict.types.numeric import Decimal_, Integer


def vis(file: File, *other_files: File):
    for r in file.resources:
        r.file = file
    for f in other_files:
        for r in f.resources:
            r.file = f
    asts = {file.path: file} | {f.path: f for f in other_files}
    mods = ModuleDictionary()
    return CompileResourceVisitor(asts, mods, {}, file.path)


def res(name, dnc, fields={}):
    rules: Rules = {
        "create": SecurityConstraintField(PEL(), lambda _: True),
        "modify": SecurityConstraintField(PEL(), lambda _: True),
        "details": SecurityConstraintField(PEL(), lambda _: True),
    }
    field_sort = [x for x in dnc] + [x for x in fields]
    return Res(
        "thing",
        name,
        "",
        False,
        fields,
        dnc,
        {},
        rules,
        {},
        used=True,
        field_sort=field_sort,
    )


def rel(bridge, opt=False):
    if opt:
        bridge = OptionalResource(bridge)
    return Rel("", "", False, False, None, None, "", bridge)


def test_rel_points_to_other_resource():
    other = res(
        "Other",
        {},
        {"a": DataComputedField(PEL(), func_res=Integer(), compiled=lambda _: 1)},
    )
    thing = res("Thing", {"a": rel(other.bridge)})
    file = File(Path("/root.restrict"), [], [thing, other])
    other.file = file
    thing.file = file
    visitor = vis(file)

    visitor.visit_file(file)

    assert visitor.errors == []

    assert other.compiled is not None
    assert thing.compiled is not None
    assert "_spec_a" in other.compiled().__dict__.keys()
    assert "_spec_a" in thing.compiled().__dict__.keys()
    thingy = thing.compiled()
    a = thingy._get_underlying_spec("a")
    assert type(a) is RelSpec
    assert a.bound is False
    assert a.res("_create") == other.compiled._create
    assert a.res("_modify") == other.compiled._modify


def test_rel_sets_to_nested_value():
    other = res(
        "Other",
        {},
        {
            "a": DataComputedField(
                PEL(), func_res=Decimal_(), compiled=lambda d: d["self"].b + 1
            ),
            "b": DataConstrainedField(
                "", "", False, False, None, PEL(), Decimal_(), lambda _: True
            ),
        },
    )
    thing = res(
        "Thing",
        {"a": rel(other.bridge)},
        {"b": DataComputedField(PEL(), Decimal_(), lambda d: d["self"].a.a + 100)},
    )
    file = File(Path("/root.restrict"), [], [thing, other])
    other.file = file
    thing.file = file
    visitor = vis(file)

    visitor.visit_file(file)

    assert visitor.errors == []

    assert thing.compiled is not None
    thingy = thing.compiled._create({"a": {"b": Decimal("3.14")}})

    assert thingy.a.b == Decimal("3.14")  # type: ignore
    assert thingy.a.a == Decimal("4.14")  # type: ignore
    assert thingy.b == Decimal("104.14")  # type: ignore


def test_assign_to_rel_works():
    other = res(
        "Other",
        {},
        {
            "a": DataComputedField(
                PEL(), func_res=Decimal_(), compiled=lambda d: d["self"].b + 1
            ),
            "b": DataConstrainedField(
                "", "", False, False, None, PEL(), Decimal_(), lambda _: True
            ),
        },
    )
    thing = res(
        "Thing",
        {"a": rel(other.bridge)},
        {"b": DataComputedField(PEL(), Decimal_(), lambda d: d["self"].a.a + 100)},
    )
    file = File(Path("/root.restrict"), [], [thing, other])
    other.file = file
    thing.file = file
    visitor = vis(file)

    visitor.visit_file(file)

    assert visitor.errors == []

    assert thing.compiled is not None
    thingy = thing.compiled._create({"a": {"b": Decimal("3.14")}})
    thingy.a = {"b": Decimal("9.9999")}  # type: ignore
    assert thingy.a.b == Decimal("9.9999")  # type: ignore


def test_rel_raises_unbound_property_error_when_getting_it_before_set():
    other = res(
        "Other",
        {},
        {
            "a": DataComputedField(
                PEL(), func_res=Decimal_(), compiled=lambda d: d["self"].b + 1
            ),
            "b": DataConstrainedField(
                "", "", False, False, None, PEL(), Decimal_(), lambda _: True
            ),
        },
    )
    thing = res(
        "Thing",
        {"a": rel(other.bridge)},
        {"b": DataComputedField(PEL(), Decimal_(), lambda d: d["self"].a.a + 100)},
    )
    file = File(Path("/root.restrict"), [], [thing, other])
    other.file = file
    thing.file = file
    visitor = vis(file)

    visitor.visit_file(file)

    assert visitor.errors == []

    assert thing.compiled is not None
    thingy = thing.compiled()

    with pytest.raises(UnboundPropertyError) as exc_info:
        thingy.a  # type: ignore

    e = exc_info.value
    assert e.is_fatal
    assert e.name == "a"
    assert e.res == "/root.restrict/Thing"


def test_rel_returns_none_for_unset_optional_rel():
    other = res(
        "Other",
        {},
        {
            "a": DataComputedField(
                PEL(), func_res=Decimal_(), compiled=lambda d: d["self"].b + 1
            ),
            "b": DataConstrainedField(
                "", "", False, False, None, PEL(), Decimal_(), lambda _: True
            ),
        },
    )
    thing = res(
        "Thing",
        {"a": rel(other.bridge, True)},
        {"b": DataComputedField(PEL(), Decimal_(), lambda d: d["self"].a.a + 100)},
    )
    file = File(Path("/root.restrict"), [], [thing, other])
    other.file = file
    thing.file = file
    visitor = vis(file)

    visitor.visit_file(file)

    assert visitor.errors == []

    assert thing.compiled is not None
    thingy = thing.compiled()

    assert thingy.a is None  # type: ignore


def test_json_on_rels_and_fields():
    other = res(
        "Other",
        {},
        {
            "a": DataComputedField(
                PEL(), func_res=Decimal_(), compiled=lambda d: d["self"].b + 1
            ),
            "b": DataConstrainedField(
                "", "", False, False, None, PEL(), Decimal_(), lambda _: True
            ),
        },
    )
    other.field_sort = ["b", "a"]
    thing = res(
        "Thing",
        {"a": rel(other.bridge)},
        {"b": DataComputedField(PEL(), Decimal_(), lambda d: d["self"].a.a + 100)},
    )
    thing.field_sort = ["a", "b"]
    file = File(Path("/root.restrict"), [], [thing, other])
    other.file = file
    thing.file = file
    visitor = vis(file)

    visitor.visit_file(file)

    assert visitor.errors == []

    assert thing.compiled is not None
    thingy = thing.compiled._modify({}, {"a": {"b": Decimal("3.14")}})

    assert thingy._to_json("details") == {
        "a": {
            "a": 4.14,
            "b": 3.14,
        },
        "b": 104.14,
    }


def sub_add(d):
    def _do(a0, a1):
        if a0 is not None and a1 is not None:
            return a0 + a1

    try:
        return _do(d["self"].a.a, 100)
    except AttributeError:
        return None


def test_json_on_optional_rels():
    other = res(
        "Other",
        {},
        {
            "a": DataComputedField(
                PEL(), func_res=Decimal_(), compiled=lambda d: d["self"].b + 1
            ),
            "b": DataConstrainedField(
                "", "", False, False, None, PEL(), Decimal_(), lambda _: True
            ),
        },
    )
    other.field_sort = ["b", "a"]
    thing = res(
        "Thing",
        {"a": rel(OptionalResource(other.bridge))},
        {"b": DataComputedField(PEL(), OptionalDatum(Decimal_()), sub_add)},
    )
    thing.field_sort = ["a", "b"]
    file = File(Path("/root.restrict"), [], [thing, other])
    other.file = file
    thing.file = file
    visitor = vis(file)

    visitor.visit_file(file)

    assert visitor.errors == []

    assert thing.compiled is not None
    thingy = thing.compiled()
    thingy._set_property_values("create", {}, {}, [], [])  # type: ignore

    assert thingy._to_json("details") == {}


def test_sub_resource_constraint_errors_are_reported():
    other = res(
        "Other",
        {},
        fields={
            "c": DataConstrainedField(
                "", "", False, False, None, PEL(), Decimal_(), lambda _: False
            ),
            "b": DataConstrainedField(
                "", "", False, False, None, PEL(), Decimal_(), lambda _: False
            ),
        },
    )
    other.field_sort = ["b", "c"]
    thing = res(
        "Thing",
        {"a": rel(other.bridge)},
    )
    thing.field_sort = ["a"]
    file = File(Path("/root.restrict"), [], [thing, other])
    other.file = file
    thing.file = file
    visitor = vis(file)

    visitor.visit_file(file)

    assert visitor.errors == []

    assert thing.compiled is not None

    with pytest.raises(RestrictRuntimeErrorGroup) as exc_info:
        thing.compiled._modify({}, {"a": {"c": 1, "b": 2}})

    errors = exc_info.value.errors
    assert len(errors) == 2
    assert exc_info.value.to_json() == {
        "type": "RestrictRuntimeErrorGroup",
        "is_fatal": True,
        "errors": [
            {
                "type": "PropertyValueFailsContraintError",
                "is_fatal": True,
                "name": "b",
                "value": Decimal("2"),
                "res": "/root.restrict/Other",
            },
            {
                "type": "PropertyValueFailsContraintError",
                "is_fatal": True,
                "name": "c",
                "value": Decimal("1"),
                "res": "/root.restrict/Other",
            },
        ],
    }
