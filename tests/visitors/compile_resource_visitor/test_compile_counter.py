from pathlib import Path
from restrict.compiler.ast import (
    DataConstrainedField,
    EffectsComputedField,
    File,
    PipedExprList,
    Res,
    SecurityConstraintField,
)
from restrict.compiler.types import ModuleDictionary
from restrict.compiler.visitors import CompileResourceVisitor
from restrict.types.numeric import Integer


def vis(res: Res):
    file = File(Path("/counter.restrict"), [], [res])
    res.file = file
    mods = ModuleDictionary()
    return file, CompileResourceVisitor({file.path: file}, mods, {}, file.path)


def make_counter():
    return Res(
        "thing",
        "Counter",
        "",
        False,
        {
            "value_": DataConstrainedField(
                "int",
                "",
                False,
                False,
                None,
                PipedExprList(),
                Integer(),
                compiled=lambda _: True,
            )
        },
        {},
        {
            "create": {
                "value_": EffectsComputedField(PipedExprList(), None, lambda _: 1)
            },
            "modify": {
                "value_": EffectsComputedField(
                    PipedExprList(), None, lambda d: d["self"].value_ + 1
                )
            },
        },
        {"details": SecurityConstraintField(PipedExprList(), lambda _: True)},
        {},
        used=True,
        field_sort=["value_"],
    )


def test_counter_starts_at_one():
    res = make_counter()
    file, visitor = vis(res)
    visitor.visit_file(file)
    assert res.compiled is not None

    instance = res.compiled._create({})

    assert instance.value_ == 1  # type: ignore
    assert instance._to_json("details") == {"value_": 1}


def test_counter_modify_increments_value():
    res = make_counter()
    file, visitor = vis(res)
    visitor.visit_file(file)
    assert res.compiled is not None

    instance = res.compiled._modify({"value_": 9}, {})

    assert instance.value_ == 10  # type: ignore
    assert instance._to_json("details") == {"value_": 10}
