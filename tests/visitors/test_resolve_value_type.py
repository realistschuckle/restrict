from pathlib import Path

import pytest

from restrict.compiler.ast import (
    DataConstrainedField,
    File,
    PipedExprList,
    Res,
    Value,
)
from restrict.compiler.types import ModuleDictionary
from restrict.compiler.visitors import ResolveValueTypeVisitor
from restrict.types.builtins import Boolean
from restrict.types.numeric import Integer
from restrict.types.text import Text
from restrict.types.time import Timestamp


def dcf(t):
    value = Value()
    field = DataConstrainedField("", "", False, False, None, PipedExprList([value]), t)
    return field, value


def vis(file):
    return ResolveValueTypeVisitor({file.path: file}, ModuleDictionary(), {}, file.path)


@pytest.mark.parametrize("t", [Boolean(), Integer(), Timestamp(), Text()])
def test_value_takes_on_field_type(t):
    field, value = dcf(t)
    res = Res("", "", "", False, {"a": field}, {}, {}, {}, {}, used=True)
    file = File(Path(""), [], [res])
    visitor = vis(file)

    visitor.visit_file(file)

    assert value.res == t
