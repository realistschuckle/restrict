from pathlib import Path

from restrict.compiler.ast import (
    DataComputedField,
    DataConstrainedField,
    File,
    Res,
)
from restrict.compiler.ast import PipedExprList as PEL
from restrict.compiler.types import ModuleDictionary
from restrict.compiler.visitors import ResolveInheritedFieldsVisitor
from restrict.types.builtins import Boolean

from .fake_module import ResourceWithFields


def vis(file: File, mods: ModuleDictionary | None = None):
    if mods is None:
        mods = ModuleDictionary()
    return ResolveInheritedFieldsVisitor({file.path: file}, mods, {}, file.path)


def test_copies_fields_from_overridden_resource():
    a = DataConstrainedField("integer", "x", False, False, list, PEL(), res=Boolean())
    b = DataComputedField(PEL())
    base = Res("", "Base", "", False, {"a": a, "b": b}, {}, {}, {}, {})
    res = Res("", "Res", "", False, {}, {}, {}, {}, {}, base.bridge)
    root = File(Path(""), [], [res, base])
    visitor = vis(root)

    visitor.visit_file(root)

    assert res.fields_updated
    assert len(res.fields) == 2
    assert res.fields["a"] == a
    assert res.fields["b"] == b


def test_copies_fields_from_nested_overridden_resources():
    a = DataConstrainedField("integer", "x", False, False, list, PEL(), res=Boolean())
    b = DataComputedField(PEL())
    base2 = Res("", "Base2", "", False, {"b": b}, {}, {}, {}, {})
    base = Res("", "Base", "", False, {"a": a}, {}, {}, {}, {}, base2.bridge)
    res = Res("", "Res", "", False, {}, {}, {}, {}, {}, base.bridge)
    root = File(Path(""), [], [res, base])
    visitor = vis(root)

    visitor.visit_file(root)

    assert res.fields_updated
    assert len(res.fields) == 2
    assert res.fields["a"] == a
    assert res.fields["b"] == b


def test_copies_fields_from_programmatic_resources():
    base3 = ResourceWithFields()

    a = DataConstrainedField("integer", "x", False, False, list, PEL(), res=Boolean())
    b = DataComputedField(PEL())
    base2 = Res("", "Base2", "", False, {"b": b}, {}, {}, {}, {}, base3)
    base = Res("", "Base", "", False, {"a": a}, {}, {}, {}, {}, base2.bridge)
    res = Res("", "Res", "", False, {}, {}, {}, {}, {}, base.bridge)
    root = File(Path(""), [], [res, base])
    visitor = vis(root)

    visitor.visit_file(root)

    assert res.fields_updated
    assert len(res.fields) == 3

    assert res.fields["a"] == a
    assert res.fields["b"] == b
