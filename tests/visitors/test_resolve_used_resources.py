from pathlib import Path

from restrict.compiler.ast import (
    File,
    Rel,
    Rels,
    Res,
)
from restrict.compiler.types import (
    ModuleDictionary,
    Resource,
)
from restrict.compiler.visitors import ResolveUsedResourcesVisitor

from .fake_module import restrict_module


def vis(file: File, *files: File):
    asts = {f.path: f for f in files} | {file.path: file}
    return ResolveUsedResourcesVisitor(asts, ModuleDictionary(), {}, file.path)


def res(name: str, rels: Rels, base: Resource | None):
    return Res("", name, "", False, {}, rels, {}, {}, {}, base)


def test_marks_resources_in_rel_as_used():
    i_res = res("I", {}, None)
    h_res = res("H", {}, None)
    g_res = res("G", {}, None)
    f_res = res("F", {}, None)
    e_res = res("E", {}, None)
    d_res = res("D", {}, None)
    d = Rel("", "", False, False, None, None, "", d_res.bridge)

    a_res = res("A", {}, None)
    b_res = res("B", {}, None)

    ca = Rel("", "", False, False, None, None, "", a_res.bridge)
    c_res = res("C", {"a": ca, "d": d}, None)

    ac = Rel("", "", False, False, None, None, "", c_res.bridge)
    a_res.dnc = dict(a_res.dnc) | {"c": ac}

    a = Rel("", "", False, False, None, None, "", a_res.bridge)
    b = Rel("", "", False, False, None, None, "", b_res.bridge)
    c = Rel("", "", False, False, None, None, "", c_res.bridge)

    fi_file = File(Path("f"), [], [f_res, i_res])
    deh_file = File(Path("de"), [], [d_res, e_res, h_res])
    abcg_file = File(Path("abc"), [], [a_res, b_res, c_res, g_res])

    root_res = [
        res("R1", {"a": a, "b": b}, None),
        res("R2", {"b": b, "c": c}, None),
    ]
    root = File(Path("root"), [], root_res)
    visitor = vis(root, abcg_file, deh_file, fi_file)

    visitor.visit_file(root)

    assert root_res[0].used
    assert root_res[1].used
    assert a_res.used
    assert b_res.used
    assert c_res.used
    assert d_res.used
    assert not e_res.used
    assert not f_res.used
    assert not g_res.used
    assert not h_res.used
    assert not i_res.used


def test_marks_resources_in_base_as_used():
    i_res = res("I", {}, None)
    h_res = res("H", {}, None)
    g_res = res("G", {}, None)
    f_res = res("F", {}, None)
    e_res = res("E", {}, None)
    d_res = res("D", {}, i_res.bridge)
    d = Rel("", "", False, False, None, None, "", d_res.bridge)

    a_res = res("A", {}, h_res.bridge)
    b_res = res("B", {}, i_res.bridge)

    ca = Rel("", "", False, False, None, None, "", i_res.bridge)
    c_res = res("C", {"a": ca, "d": d}, None)

    ac = Rel("", "", False, False, None, None, "", c_res.bridge)
    a_res.dnc = dict(a_res.dnc) | {"c": ac}

    a = Rel("", "", False, False, None, None, "", a_res.bridge)
    b = Rel("", "", False, False, None, None, "", b_res.bridge)
    c = Rel("", "", False, False, None, None, "", c_res.bridge)

    fi_file = File(Path("f"), [], [f_res, i_res])
    deh_file = File(Path("de"), [], [d_res, e_res, h_res])
    abcg_file = File(Path("abc"), [], [a_res, b_res, c_res, g_res])

    root_res = [
        res("R1", {"a": a, "b": b}, g_res.bridge),
        res("R2", {"b": b, "c": c}, h_res.bridge),
    ]
    root = File(Path("root"), [], root_res)
    visitor = vis(root, abcg_file, deh_file, fi_file)

    visitor.visit_file(root)

    assert root_res[0].used
    assert root_res[1].used
    assert a_res.used
    assert b_res.used
    assert c_res.used
    assert d_res.used
    assert not e_res.used
    assert not f_res.used
    assert not g_res.used
    assert not h_res.used
    assert i_res.used


def test_marks_internal_resources_as_base_as_not_used():
    r = res("R1", {}, restrict_module.resources["Thing"])
    root = File(Path("root"), [], [r])
    visitor = vis(root)

    visitor.visit_file(root)

    assert r.used
    assert not restrict_module.resources["Thing"].used


def test_marks_internal_resources_as_rel_as_used():
    thing = restrict_module.resources["Thing"]
    a = Rel("", "", False, False, None, None, "", thing)
    r = res("R1", {"a": a}, None)
    root = File(Path("root"), [], [r])
    visitor = vis(root)

    visitor.visit_file(root)

    assert r.used
    assert restrict_module.resources["Thing"].used
