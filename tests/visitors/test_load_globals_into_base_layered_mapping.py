from pathlib import Path
from restrict.compiler.exceptions.compile import DuplicateGlobalNameError
from restrict.compiler.types import LayeredMapping, ModuleDictionary
from restrict.compiler.visitors import (
    LoadGlobalsIntoBaseLayeredMapping,
    CompileResourceVisitor,
)


def test_duplicate_global_names_are_bad():
    CompileResourceVisitor.context = LayeredMapping({"fargo": True})
    root = Path(".")

    visitor = LoadGlobalsIntoBaseLayeredMapping(
        {}, ModuleDictionary(), {"fargo": False}, root
    )

    assert len(visitor.errors) == 1
    e = visitor.errors[0]
    assert isinstance(e, DuplicateGlobalNameError)
    assert e.name == "fargo"
