from pathlib import Path

import pytest

from restrict.compiler.ast import (
    File,
    Lit,
    Res,
    SecurityConstraintField,
)
from restrict.compiler.ast import PipedExprList as PEL
from restrict.compiler.exceptions.compile import InvalidConstraintError
from restrict.compiler.types import ModuleDictionary
from restrict.types.builtins import Boolean
from restrict.compiler.visitors import CheckSecurityConstraintTypeVisitor
from restrict.types.numeric import Integer
from restrict.types.text import Text


def vis(file: File):
    mods = ModuleDictionary()
    return CheckSecurityConstraintTypeVisitor({file.path: file}, mods, {}, file.path)


def test_boolean_is_good():
    field = SecurityConstraintField(PEL([Lit(True, Boolean())]))
    res = Res("", "", "", False, {}, {}, {}, {"list": field}, {}, used=True)
    file = File(Path(""), [], [res])
    visitor = vis(file)

    visitor.visit_file(file)

    assert len(visitor.errors) == 0


@pytest.mark.parametrize("v,t", [(1, Integer()), ("", Text())])
def test_does_not_visit_unused_resources(v, t):
    field = SecurityConstraintField(PEL([Lit(v, t)]))
    res = Res("", "R", "", False, {}, {}, {}, {"list": field}, {})
    file = File(Path(""), [], [res])
    visitor = vis(file)

    visitor.visit_file(file)

    assert len(visitor.errors) == 0


@pytest.mark.parametrize("v,t", [(1, Integer()), ("", Text())])
def test_scalar_not_boolean_is_bad_for_action(v, t):
    field = SecurityConstraintField(PEL([Lit(v, t)]))
    res = Res("", "R", "", False, {}, {}, {}, {"list": field}, {}, used=True)
    file = File(Path(""), [], [res])
    visitor = vis(file)

    visitor.visit_file(file)

    assert len(visitor.errors) == 1
    e = visitor.errors[0]
    assert type(e) is InvalidConstraintError
    assert e.path == "R.list"
    assert e.type == t.name
    assert e.source == file.path


@pytest.mark.parametrize("v,t", [(1, Integer()), ("", Text())])
def test_scalar_not_boolean_is_bad_for_action_field(v, t):
    field = SecurityConstraintField(PEL([Lit(v, t)]))
    res = Res("", "R", "", False, {}, {}, {}, {"list": {"a": field}}, {}, used=True)
    file = File(Path(""), [], [res])
    visitor = vis(file)

    visitor.visit_file(file)

    assert len(visitor.errors) == 1
    e = visitor.errors[0]
    assert type(e) is InvalidConstraintError
    assert e.path == "R.list.a"
    assert e.type == t.name
    assert e.source == file.path
