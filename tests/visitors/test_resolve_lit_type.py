from decimal import Decimal
from itertools import product
from pathlib import Path

import pytest

from restrict.compiler.ast import (
    DataComputedField,
    DataConstrainedField,
    Effects,
    EffectsComputedField,
    File,
    Lit,
    PipedExprList,
    Res,
    Rules,
    SecurityConstraintField,
    Transition,
    TransitionComputedField,
    Transitions,
)
from restrict.compiler.types import Data, ModuleDictionary
from restrict.compiler.visitors import ResolveLitTypeVisitor
from restrict.types.builtins import Boolean
from restrict.types.numeric import Decimal_, Integer
from restrict.types.text import Text


def vis(file: File):
    return ResolveLitTypeVisitor({file.path: file}, ModuleDictionary(), {}, file.path)


def dcsf(s):
    pel = PipedExprList([s])
    a = DataConstrainedField("", "", False, False, None, pel)
    res = Res("", "", "", False, {"a": a}, {}, {}, {}, {}, used=True)
    file = File(Path(""), [], [res])
    return file


def dcpf(s):
    pel = PipedExprList([s])
    a = DataComputedField(pel)
    res = Res("", "", "", False, {"a": a}, {}, {}, {}, {}, used=True)
    file = File(Path(""), [], [res])
    return file


def eff(s):
    pel = PipedExprList([s])
    a = EffectsComputedField(pel)
    effects: Effects = {"create": {"a": a}}
    res = Res("", "", "", False, {}, {}, effects, {}, {}, used=True)
    file = File(Path(""), [], [res])
    return file


def sec(s):
    pel = PipedExprList([s])
    a = SecurityConstraintField(pel)
    rules: Rules = {"list": a}
    res = Res("", "", "", False, {}, {}, {}, rules, {}, used=True)
    file = File(Path(""), [], [res])
    return file


def tx(s):
    pel = PipedExprList([s])
    a = TransitionComputedField(pel)
    tx = Transition("", "", "", "", {"a": a})
    txs: Transitions = {"list": tx}
    res = Res("", "", "", False, {}, {}, {}, {}, txs, used=True)
    file = File(Path(""), [], [res])
    return file


@pytest.mark.parametrize(
    "value,expected,factory",
    [
        (a[0], a[1], b)
        for a, b in product(
            [
                (True, Boolean()),
                (Decimal("1.0"), Decimal_()),
                (3, Integer()),
                ("Hello", Text()),
            ],
            [dcsf, dcpf, eff, sec, tx],
        )
    ],
)
def test_scalar_lit_value(value, expected, factory):
    lit = Lit(value)
    file = factory(lit)
    visitor = vis(file)

    visitor.visit_file(file)

    assert lit.res == expected


@pytest.mark.parametrize(
    "value,expected,factory,coll_type",
    [
        (a[0], a[1], b, c)
        for a, b, c in product(
            [
                (True, Boolean()),
                (Decimal("1.0"), Decimal_()),
                (3, Integer()),
                ("Hello", Text()),
            ],
            [dcsf, dcpf, eff, sec, tx],
            [set, list],
        )
    ],
)
def test_collection_lit_value(value, expected, factory, coll_type):
    lit = Lit(coll_type([value]))
    file = factory(lit)
    visitor = vis(file)

    visitor.visit_file(file)

    assert lit.res == Data(coll_type, expected)
