from pathlib import Path

import pytest

from restrict.compiler.ast import File, Import, Res
from restrict.compiler.exceptions.compile import (
    AmbiguousTypeError,
    UnknownTypeError,
)
from restrict.compiler.types import ModuleDictionary
from restrict.compiler.visitors import ResolveBaseResourceTypeVisitor

from .fake_module import restrict_module


def vis(
    root: File,
    mods: ModuleDictionary | None = None,
    asts: dict[Path, File] | None = None,
):
    if mods is None:
        mods = ModuleDictionary()
    if asts is None:
        asts = {}
    return ResolveBaseResourceTypeVisitor(asts, mods, {}, root.path)


def test_resolve_leaves_resources_alone_that_are_not_overridden():
    res = Res("", "Thing", "", False, {}, {}, {}, {}, {})
    fake_module_path = Path("fake_module")
    file = File(Path(""), [Import(fake_module_path, "")], [res])
    mods = ModuleDictionary()
    mods[fake_module_path] = restrict_module
    visitor = vis(file, mods)

    visitor.visit_file(file)

    assert res.base is None


@pytest.mark.parametrize("prefix", ["x", ""])
def test_resolves_resource_from_compiled_import(prefix: str):
    res = Res("", "Thing", prefix, True, {}, {}, {}, {}, {})
    fake_module_path = Path("fake_module")
    file = File(Path(""), [Import(fake_module_path, prefix)], [res])
    mods = ModuleDictionary()
    mods[fake_module_path] = restrict_module
    visitor = vis(file, mods)

    visitor.visit_file(file)

    assert res.base == restrict_module.resources["Thing"]


@pytest.mark.parametrize("prefix", ["x", ""])
def test_resolves_resource_from_imported_files(prefix: str):
    thing = Res("", "Thing", prefix, False, {}, {}, {}, {}, {})
    other_file = File(Path("/thing.restrict"), [], [thing])

    res = Res("", "Thing", prefix, True, {}, {}, {}, {}, {})
    file = File(Path(""), [Import(other_file.path, prefix)], [res])
    asts = {file.path: file, other_file.path: other_file}
    visitor = vis(file, asts=asts)

    visitor.visit_file(file)

    assert res.base == thing.bridge


@pytest.mark.parametrize("prefix", ["x", ""])
def test_adds_unknown_type_when_cannot_find_type(prefix: str):
    thing = Res("", "Thing", prefix, False, {}, {}, {}, {}, {})
    other_file = File(Path("/thing.restrict"), [], [thing])
    asts = {other_file.path: other_file}
    fake_module_path = Path("fake_module")
    mods = ModuleDictionary()
    mods[fake_module_path] = restrict_module
    res = Res("", "Unknown", prefix, True, {}, {}, {}, {}, {})
    file = File(
        Path(""),
        [Import(fake_module_path, prefix), Import(other_file.path, prefix)],
        [res],
    )
    visitor = vis(file, mods, asts)

    visitor.visit_file(file)

    assert res.base is None

    e = visitor.errors[0]
    assert type(e) is UnknownTypeError
    assert e.prefix == prefix
    assert e.source == file.path
    assert e.type == "Unknown"


def test_adds_ambiguous_type_when_finds_more_than_one_type_with_name():
    thing = Res("", "Thing", "", False, {}, {}, {}, {}, {})
    other_file = File(Path("/thing.restrict"), [], [thing])
    asts = {other_file.path: other_file}
    fake_module_path = Path("fake_module")
    mods = ModuleDictionary()
    mods[fake_module_path] = restrict_module
    res = Res("", "Thing", "", True, {}, {}, {}, {}, {})
    file = File(
        Path(""),
        [Import(fake_module_path, ""), Import(other_file.path, "")],
        [res],
    )
    visitor = vis(file, mods, asts)

    visitor.visit_file(file)

    assert res.base is None

    e = visitor.errors[0]
    assert type(e) is AmbiguousTypeError
    assert e.source == file.path
    assert e.name == "Thing"
    assert e.sources == [fake_module_path, other_file.path]
