from pathlib import Path

from restrict.compiler.ast import File, Rel, Res
from restrict.compiler.types import ModuleDictionary
from restrict.compiler.visitors import ResolveInheritedRelsVisitor

from .fake_module import ResourceWithRels


def vis(file: File, mods: ModuleDictionary | None = None):
    if mods is None:
        mods = ModuleDictionary()
    return ResolveInheritedRelsVisitor({file.path: file}, mods, {}, file.path)


def test_copies_rels_from_overridden_resource():
    a_res = Res("", "A", "", False, {}, {}, {}, {}, {})
    b_res = Res("", "B", "", False, {}, {}, {}, {}, {})

    a = Rel("", "", False, False, None, None, "", a_res.bridge)
    b = Rel("", "", False, False, None, None, "", b_res.bridge)
    base = Res("", "Base", "", False, {}, {"a": a, "b": b}, {}, {}, {})
    res = Res("", "Res", "", False, {}, {}, {}, {}, {}, base.bridge)
    root = File(Path(""), [], [res, base])
    visitor = vis(root)

    visitor.visit_file(root)

    assert res.dnc_updated
    assert len(res.dnc) == 2
    assert res.dnc["a"] == a
    assert res.dnc["b"] == b


def test_copies_rels_from_nested_overridden_resources():
    a_res = Res("", "A", "", False, {}, {}, {}, {}, {})
    b_res = Res("", "B", "", False, {}, {}, {}, {}, {})

    a = Rel("", "", False, False, None, None, "", a_res.bridge)
    b = Rel("", "", False, False, None, None, "", b_res.bridge)
    base2 = Res("", "Base2", "", False, {}, {"b": b}, {}, {}, {})
    base = Res("", "Base", "", False, {}, {"a": a}, {}, {}, {}, base2.bridge)
    res = Res("", "Res", "", False, {}, {}, {}, {}, {}, base.bridge)
    root = File(Path(""), [], [res, base])
    visitor = vis(root)

    visitor.visit_file(root)

    assert res.dnc_updated
    assert len(res.dnc) == 2
    assert res.dnc["a"] == a
    assert res.dnc["b"] == b


def test_copies_rels_from_programmatic_resources():
    base3 = ResourceWithRels()
    a_res = Res("", "A", "", False, {}, {}, {}, {}, {})
    b_res = Res("", "B", "", False, {}, {}, {}, {}, {})

    a = Rel("", "", False, False, None, None, "", a_res.bridge)
    b = Rel("", "", False, False, None, None, "", b_res.bridge)
    base2 = Res("", "Base2", "", False, {}, {"b": b}, {}, {}, {}, base3)
    base = Res("", "Base", "", False, {}, {"a": a}, {}, {}, {}, base2.bridge)
    res = Res("", "Res", "", False, {}, {}, {}, {}, {}, base.bridge)
    root = File(Path(""), [], [res, base])
    visitor = vis(root)

    visitor.visit_file(root)

    assert res.dnc_updated
    assert len(res.dnc) == 3
    assert res.dnc["c"].res is not None
    assert res.dnc["a"] == a
    assert res.dnc["b"] == b
