from pathlib import Path

import pytest

from restrict.compiler.ast import (
    DataConstrainedField,
    File,
    Import,
    PipedExprList,
    Res,
)
from restrict.compiler.exceptions.compile import (
    AmbiguousTypeError,
    UnknownTypeError,
)
from restrict.compiler.types import (
    Data,
    ModuleDictionary,
    OptionalDatum,
)
from restrict.types.builtins import Boolean
from restrict.compiler.visitors import ResolveDataConstrainedFieldTypeVisitor

from .fake_module import TreasuryService, restrict_module


def vis(root: File, mods: ModuleDictionary | None = None):
    if mods is None:
        mods = ModuleDictionary()
    return ResolveDataConstrainedFieldTypeVisitor({}, mods, {}, root.path)


def dcf(type: str, prefix: str, coll_type=None, opt=False):
    return DataConstrainedField(
        type,
        prefix,
        opt,
        False,
        coll_type,
        PipedExprList(),
    )


@pytest.mark.parametrize(
    "opt,expected",
    [(False, Boolean()), (True, OptionalDatum(Boolean()))],
)
def test_resolves_built_in_types(opt, expected):
    field = dcf("boolean", "", opt=opt)
    res = Res("", "", "", False, {"a": field}, {}, {}, {}, {})
    file = File(Path(""), [], [res])
    visitor = vis(file)

    visitor.visit_file(file)

    assert field.res == expected


@pytest.mark.parametrize("coll_type", [list, set])
def test_resolves_collection_types(coll_type: type[list] | type[set]):
    field = dcf("boolean", "", coll_type)
    res = Res("", "", "", False, {"a": field}, {}, {}, {}, {})
    file = File(Path(""), [], [res])
    visitor = vis(file)

    visitor.visit_file(file)

    assert field.res == Data(coll_type, Boolean())


@pytest.mark.parametrize("coll_type", [list, set])
def test_resolves_optional_collection_types(coll_type: type[list] | type[set]):
    field = dcf("boolean", "", coll_type, True)
    res = Res("", "", "", False, {"a": field}, {}, {}, {}, {})
    file = File(Path(""), [], [res])
    visitor = vis(file)

    visitor.visit_file(file)

    assert field.res == OptionalDatum(Data(coll_type, Boolean()))


@pytest.mark.parametrize("prefix", ["x", ""])
def test_resolves_type_from_import(prefix: str):
    field = dcf("treasury_service", prefix)
    res = Res("", "", "", False, {"a": field}, {}, {}, {}, {})
    fake_module_path = Path("fake_module")
    file = File(Path(""), [Import(fake_module_path, prefix)], [res])
    mods = ModuleDictionary()
    mods[fake_module_path] = restrict_module
    visitor = vis(file, mods)

    visitor.visit_file(file)

    assert field.res == TreasuryService()


@pytest.mark.parametrize("prefix", ["", "a"])
def test_adds_unknown_type_when_cannot_find_type(prefix: str):
    field = dcf("unknown_type", prefix)
    res = Res("", "", "", False, {"a": field}, {}, {}, {}, {})
    fake_module_path = Path("fake_module")
    file = File(Path(""), [Import(fake_module_path, prefix)], [res])
    mods = ModuleDictionary()
    mods[fake_module_path] = restrict_module
    visitor = vis(file, mods)

    visitor.visit_file(file)

    assert field.res is None

    e = visitor.errors[0]
    assert type(e) is UnknownTypeError
    assert e.prefix == prefix
    assert e.source == Path("")
    assert e.type == "unknown_type"


def test_adds_ambiguous_type_when_finds_more_than_one_type_with_name():
    field = dcf("treasury_service", "")
    res = Res("", "", "", False, {"a": field}, {}, {}, {}, {})
    fake_module_path1 = Path("fake_module_1")
    fake_module_path2 = Path("fake_module_2")
    file = File(
        Path(""),
        [Import(fake_module_path1, ""), Import(fake_module_path2, "")],
        [res],
    )
    mods = ModuleDictionary()
    mods[fake_module_path1] = restrict_module
    mods[fake_module_path2] = restrict_module
    visitor = vis(file, mods)

    visitor.visit_file(file)

    assert field.res is None

    e = visitor.errors[0]
    assert type(e) is AmbiguousTypeError
    assert e.name == "treasury_service"
    assert e.source == Path("")
    assert e.sources == [fake_module_path1, fake_module_path2]
