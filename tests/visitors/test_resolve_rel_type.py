from pathlib import Path

import pytest

from restrict.compiler.ast import (
    File,
    Import,
    Rel,
    Res,
    ResToResourceBridge,
)
from restrict.compiler.exceptions.compile import (
    AmbiguousTypeError,
    UnknownTypeError,
)
from restrict.compiler.types import (
    ModuleDictionary,
    ResourceCollection,
)
from restrict.compiler.visitors import ResolveRelTypeVisitor

from .fake_module import Thing, restrict_module


def vis(
    root: File,
    mods: ModuleDictionary | None = None,
    asts: dict[Path, File] | None = None,
):
    if mods is None:
        mods = ModuleDictionary()
    if asts is None:
        asts = {root.path: root}
    return ResolveRelTypeVisitor(asts, mods, {}, root.path)


def rel(type: str, prefix: str, coll_type=None):
    return Rel(type, prefix, False, False, coll_type, None, "")


def test_resolves_resource_in_same_file():
    thing = Res("", "Thing", "", False, {}, {}, {}, {}, {})
    field = rel("Thing", "")
    res = Res("", "", "", False, {}, {"a": field}, {}, {}, {})
    file = File(Path(""), [], [res, thing])
    visitor = vis(file)

    visitor.visit_file(file)

    assert field.res == thing.bridge


@pytest.mark.parametrize("prefix", ["x", ""])
def test_resolves_resource_from_compiled_import(prefix: str):
    field = rel("Thing", prefix)
    res = Res("", "", "", False, {}, {"a": field}, {}, {}, {})
    fake_module_path = Path("fake_module")
    file = File(Path(""), [Import(fake_module_path, prefix)], [res])
    mods = ModuleDictionary()
    mods[fake_module_path] = restrict_module
    visitor = vis(file, mods)

    visitor.visit_file(file)

    assert type(field.res) is Thing


@pytest.mark.parametrize("coll_type", [list, set])
def test_resolves_collection_resource_from_compiled_import(
    coll_type: type[list] | type[set],
):
    field = rel("Thing", "", coll_type)
    res = Res("", "", "", False, {}, {"a": field}, {}, {}, {})
    fake_module_path = Path("fake_module")
    file = File(Path(""), [Import(fake_module_path, "")], [res])
    mods = ModuleDictionary()
    mods[fake_module_path] = restrict_module
    visitor = vis(file, mods)

    visitor.visit_file(file)

    assert type(field.res) is ResourceCollection
    assert type(field.res.internal_type) is Thing
    assert field.res.collection_type is coll_type


@pytest.mark.parametrize("prefix", ["x", ""])
def test_resolves_resource_from_imported_files(prefix: str):
    thing = Res("", "Thing", prefix, False, {}, {}, {}, {}, {})
    other_file = File(Path("/thing.restrict"), [], [thing])
    asts = {other_file.path: other_file}

    field = rel("Thing", prefix)
    res = Res("", "", "", False, {}, {"a": field}, {}, {}, {})
    file = File(Path(""), [Import(other_file.path, prefix)], [res])
    visitor = vis(file, asts=asts)

    visitor.visit_file(file)

    assert type(field.res) is ResToResourceBridge
    assert field.res.name == "Thing"
    assert field.res._resource == thing


@pytest.mark.parametrize("prefix", ["x", ""])
def test_adds_unknown_type_when_cannot_find_type(prefix: str):
    thing = Res("", "Thing", prefix, False, {}, {}, {}, {}, {})
    other_file = File(Path("/thing.restrict"), [], [thing])
    asts = {other_file.path: other_file}
    fake_module_path = Path("fake_module")
    mods = ModuleDictionary()
    mods[fake_module_path] = restrict_module
    field = rel("Unknown", prefix)
    res = Res("", "", "", False, {}, {"a": field}, {}, {}, {})
    file = File(
        Path(""),
        [Import(fake_module_path, prefix), Import(other_file.path, prefix)],
        [res],
    )
    visitor = vis(file, mods, asts)

    visitor.visit_file(file)

    assert field.res is None

    e = visitor.errors[0]
    assert type(e) is UnknownTypeError
    assert e.prefix == prefix
    assert e.source == file.path
    assert e.type == "Unknown"


def test_adds_ambiguous_type_when_finds_more_than_one_type_with_name():
    thing = Res("", "Thing", "", False, {}, {}, {}, {}, {})
    other_file = File(Path("/thing.restrict"), [], [thing])
    asts = {other_file.path: other_file}
    fake_module_path = Path("fake_module")
    mods = ModuleDictionary()
    mods[fake_module_path] = restrict_module
    field = rel("Thing", "")
    res = Res("", "", "", False, {}, {"a": field}, {}, {}, {})
    file = File(
        Path(""),
        [Import(fake_module_path, ""), Import(other_file.path, "")],
        [res],
    )
    visitor = vis(file, mods, asts)

    visitor.visit_file(file)

    assert field.res is None

    e = visitor.errors[0]
    assert type(e) is AmbiguousTypeError
    assert e.source == file.path
    assert e.name == "Thing"
    assert e.sources == [fake_module_path, other_file.path, file.path]
