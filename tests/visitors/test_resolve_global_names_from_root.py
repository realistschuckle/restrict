from pathlib import Path

from restrict.compiler.ast import File, Rel, Res
from restrict.compiler.exceptions.compile import UnknownPropertyError
from restrict.compiler.types import ModuleDictionary, Relation
from restrict.compiler.visitors import ResolveGlobalNamesFromRootVisitor

from .fake_module import restrict_module


def vis(
    file: File,
    mods: ModuleDictionary,
    globals_: dict[str, Relation],
    other: File | None = None,
):
    asts = {file.path: file}
    if other is not None:
        asts[other.path] = other
    return ResolveGlobalNamesFromRootVisitor(asts, mods, globals_, file.path)


def test_adds_rel_from_overridden_resource():
    for r in restrict_module.resources.values():
        r.used = False

    mods = ModuleDictionary()
    mods[Path("/fake")] = restrict_module

    other = Res("", "", "", False, {}, {}, {}, {}, {})
    a = Rel("", "", False, False, None, None, "", other.bridge)

    ome = restrict_module.resources["OverrideMe"]
    ome.used = True
    res = Res("", "", "", False, {}, {"a": a}, {}, {}, {}, ome, None, True)
    root = File(Path(""), [], [res])

    visitor = vis(root, mods, {})

    assert visitor._globals["a"] == other.bridge


def test_adds_rel_from_nested_overridden_resource():
    for r in restrict_module.resources.values():
        r.used = False

    mods = ModuleDictionary()
    mods[Path("/fake")] = restrict_module

    other = Res("", "", "", False, {}, {}, {}, {}, {})
    a = Rel("", "", False, False, None, None, "", other.bridge)

    ome = restrict_module.resources["OverrideMe"]
    ome.used = True
    nested = Res("", "", "", False, {}, {"a": a}, {}, {}, {}, ome, None, True)
    res = Res("", "", "", False, {}, {"a": a}, {}, {}, {}, nested.bridge, None, True)
    root = File(Path(""), [], [res])

    visitor = vis(root, mods, {})

    assert visitor._globals["a"] == other.bridge


def test_adds_rel_through_intermediary_overridden_resource():
    for r in restrict_module.resources.values():
        r.used = False

    mods = ModuleDictionary()
    mods[Path("/fake")] = restrict_module

    other = Res("", "", "", False, {}, {}, {}, {}, {})
    a = Rel("", "", False, False, None, None, "", other.bridge)

    ome = restrict_module.resources["OverrideMe"]
    ome.used = True
    nested = Res("", "", "", False, {}, {}, {}, {}, {}, ome, None, True)
    res = Res("", "", "", False, {}, {"a": a}, {}, {}, {}, nested.bridge, None, True)
    root = File(Path(""), [], [res])

    visitor = vis(root, mods, {})

    assert visitor._globals["a"] == other.bridge


def test_errs_when_overriden_resource_does_not_have_rel():
    for r in restrict_module.resources.values():
        r.used = False

    mods = ModuleDictionary()
    mods[Path("/fake")] = restrict_module

    ome = restrict_module.resources["OverrideMe"]
    ome.used = True
    res = Res("", "R", "", False, {}, {}, {}, {}, {}, ome, None, True)
    root = File(Path(""), [], [res])

    visitor = vis(root, mods, {})

    assert visitor._globals == {}
    assert len(visitor.errors) == 1
    e = visitor.errors[0]

    assert type(e) is UnknownPropertyError
    assert e.prop_name == "a"
    assert e.ref_res_name == "R"
    assert e.section == "dnc"
    assert e.source == root.path
    assert e.src_res_name == "OverrideMe"


def test_errs_when_nested_overriden_resource_does_not_have_rel():
    for r in restrict_module.resources.values():
        r.used = False

    mods = ModuleDictionary()
    mods[Path("/fake")] = restrict_module

    ome = restrict_module.resources["OverrideMe"]
    ome.used = True

    base = Res("", "B", "", False, {}, {}, {}, {}, {}, ome, None, True)
    base_file = File(Path("base"), [], [base])
    res = Res("", "R", "", False, {}, {}, {}, {}, {}, base.bridge, None, True)
    root = File(Path(""), [], [res])

    visitor = vis(root, mods, {}, base_file)

    assert visitor._globals == {}
    assert len(visitor.errors) == 1
    e = visitor.errors[0]

    assert type(e) is UnknownPropertyError
    assert e.prop_name == "a"
    assert e.ref_res_name == "R"
    assert e.section == "dnc"
    assert e.source == root.path
    assert e.src_res_name == "B"
