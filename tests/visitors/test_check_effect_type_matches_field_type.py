from pathlib import Path

import pytest

from restrict.compiler.ast import (
    EffectsComputedField,
    File,
    Lit,
    Res,
)
from restrict.compiler.ast import PipedExprList as PEL
from restrict.compiler.exceptions.compile import InvalidEffectError
from restrict.compiler.types import ModuleDictionary
from restrict.types.builtins import Boolean
from restrict.compiler.visitors import CheckEffectTypeMatchesFieldType
from restrict.types.numeric import Integer
from restrict.types.text import Text


def vis(file: File):
    mods = ModuleDictionary()
    return CheckEffectTypeMatchesFieldType({file.path: file}, mods, {}, file.path)


def test_does_not_visit_unused_resource():
    field = EffectsComputedField(PEL([Lit(1, Integer())]), Boolean())
    res = Res("", "", "", False, {}, {}, {"create": {"a": field}}, {}, {})
    file = File(Path(""), [], [res])
    visitor = vis(file)

    visitor.visit_file(file)

    assert len(visitor.errors) == 0


def test_matching_is_good():
    field = EffectsComputedField(PEL([Lit(True, Boolean())]), Boolean())
    res = Res("", "", "", False, {}, {}, {"create": {"a": field}}, {}, {}, used=True)
    file = File(Path(""), [], [res])
    visitor = vis(file)

    visitor.visit_file(file)

    assert len(visitor.errors) == 0


@pytest.mark.parametrize("v,t", [(1, Integer()), ("", Text())])
def test_not_matching_is_bad(v, t):
    field = EffectsComputedField(PEL([Lit(v, t)]), Boolean())
    res = Res("", "R", "", False, {}, {}, {"create": {"a": field}}, {}, {}, used=True)
    file = File(Path(""), [], [res])
    visitor = vis(file)

    visitor.visit_file(file)

    assert len(visitor.errors) == 1
    e = visitor.errors[0]
    assert type(e) is InvalidEffectError
    assert e.path == "R.create.a"
    assert e.expected_type == "boolean"
    assert e.received_type == t.name
    assert e.source == file.path
