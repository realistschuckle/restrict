from pathlib import Path

from restrict.compiler.ast import (
    DataComputedField,
    DataConstrainedField,
    Effects,
    EffectsComputedField,
    File,
    PipedExprList,
    Res,
    Selves,
    Transition,
    TransitionComputedField,
)
from restrict.compiler.types import (
    ModuleDictionary,
    ResourceCollection,
)
from restrict.compiler.visitors import (
    ResolveSelvesResourceCollectionTypeVisitor,
)


def vis(file):
    return ResolveSelvesResourceCollectionTypeVisitor(
        {file.path: file},
        ModuleDictionary(),
        {},
        file.path,
    )


def test_sets_selves_in_data_computed_fields():
    selves = Selves()
    field = DataComputedField(PipedExprList([selves]))
    res = Res("", "", "", False, {"a": field}, {}, {}, {}, {}, used=True)
    file = File(Path(""), [], [res])
    visitor = vis(file)

    visitor.visit_file(file)

    assert isinstance(selves.res, ResourceCollection)
    assert selves.res.collection_type is list
    assert selves.res.internal_type == res.bridge


def test_sets_selves_in_data_constrained_fields():
    selves = Selves()
    field = DataConstrainedField(
        "",
        "",
        False,
        False,
        None,
        PipedExprList([selves]),
    )
    res = Res("", "", "", False, {"a": field}, {}, {}, {}, {}, used=True)
    file = File(Path(""), [], [res])
    visitor = vis(file)

    visitor.visit_file(file)

    assert isinstance(selves.res, ResourceCollection)
    assert selves.res.collection_type is list
    assert selves.res.internal_type == res.bridge


def test_sets_selves_in_effects_computed_fields():
    selves = Selves()
    field = EffectsComputedField(PipedExprList([selves]))
    effects: Effects = {"create": {"a": field}}
    res = Res("", "", "", False, {}, {}, effects, {}, {}, used=True)
    file = File(Path(""), [], [res])
    visitor = vis(file)

    visitor.visit_file(file)

    assert isinstance(selves.res, ResourceCollection)
    assert selves.res.collection_type is list
    assert selves.res.internal_type == res.bridge


def test_sets_selves_in_transitions():
    selves = Selves()
    field = Transition(
        "", "", "", "", {"a": TransitionComputedField(PipedExprList([selves]))}
    )
    res = Res("", "", "", False, {}, {}, {}, {}, {"list": field}, used=True)
    file = File(Path(""), [], [res])
    visitor = vis(file)

    visitor.visit_file(file)

    assert isinstance(selves.res, ResourceCollection)
    assert selves.res.collection_type is list
    assert selves.res.internal_type == res.bridge
