from pathlib import Path

import pytest

from restrict.compiler.ast import (
    DataComputedField,
    File,
    Func,
    Import,
    PipedExprList,
    Res,
)
from restrict.compiler.exceptions.compile import (
    AmbiguousFunctionError,
    InvalidPrefixError,
    UnknownFunctionError,
)
from restrict.compiler.types import ModuleDictionary
from restrict.compiler.visitors import ResolveFuncFunctionVisitor
from restrict.types.builtins import Map

from .fake_module import Boop, restrict_module


def vis(root: File, mods: ModuleDictionary | None = None):
    if mods is None:
        mods = ModuleDictionary()
    return ResolveFuncFunctionVisitor({}, mods, {}, root.path)


def dcf(name: str, prefix: str):
    func = Func(name, [prefix], [], [])
    return DataComputedField(PipedExprList([func])), func


def test_resolves_built_in_types():
    field, func = dcf("map", "")
    res = Res("", "", "", False, {"a": field}, {}, {}, {}, {}, used=True)
    file = File(Path(""), [], [res])
    visitor = vis(file)

    visitor.visit_file(file)

    assert type(func.res) is Map


@pytest.mark.parametrize("prefix", ["x", ""])
def test_resolves_type_from_import(prefix: str):
    field, func = dcf("boop", prefix)
    res = Res("", "", "", False, {"a": field}, {}, {}, {}, {}, used=True)
    fake_module_path = Path("fake_module")
    file = File(Path(""), [Import(fake_module_path, prefix)], [res])
    mods = ModuleDictionary()
    mods[fake_module_path] = restrict_module
    visitor = vis(file, mods)

    visitor.visit_file(file)

    assert type(func.res) is Boop


@pytest.mark.parametrize("prefix", ["", "a"])
def test_adds_unknown_type_when_cannot_find_type(prefix: str):
    field, func = dcf("unknown_function", prefix)
    res = Res("", "", "", False, {"a": field}, {}, {}, {}, {}, used=True)
    fake_module_path = Path("fake_module")
    file = File(Path(""), [Import(fake_module_path, prefix)], [res])
    mods = ModuleDictionary()
    mods[fake_module_path] = restrict_module
    visitor = vis(file, mods)

    visitor.visit_file(file)

    assert func.res is None

    e = visitor.errors[0]
    assert type(e) is UnknownFunctionError
    assert e.name == "unknown_function"
    assert e.source == Path("")


def test_adds_ambiguous_type_when_finds_more_than_one_type_with_name():
    field, func = dcf("boop", "")
    res = Res("", "", "", False, {"a": field}, {}, {}, {}, {}, used=True)
    fake_module_path1 = Path("fake_module_1")
    fake_module_path2 = Path("fake_module_2")
    file = File(
        Path(""),
        [Import(fake_module_path1, ""), Import(fake_module_path2, "")],
        [res],
    )
    mods = ModuleDictionary()
    mods[fake_module_path1] = restrict_module
    mods[fake_module_path2] = restrict_module
    visitor = vis(file, mods)

    visitor.visit_file(file)

    assert func.res is None

    e = visitor.errors[0]
    assert type(e) is AmbiguousFunctionError
    assert e.name == "boop"
    assert e.source == Path("")
    assert e.sources == [fake_module_path1, fake_module_path2]


def test_funcs_with_prefixes_longer_than_one_entry_causes_error():
    field, func = dcf("boop", "hi")
    func.prefix.append("another")
    res = Res("", "", "", False, {"a": field}, {}, {}, {}, {}, used=True)
    fake_module_path1 = Path("fake_module_1")
    fake_module_path2 = Path("fake_module_2")
    file = File(
        Path(""),
        [Import(fake_module_path1, ""), Import(fake_module_path2, "")],
        [res],
    )
    mods = ModuleDictionary()
    mods[fake_module_path1] = restrict_module
    mods[fake_module_path2] = restrict_module
    visitor = vis(file, mods)

    visitor.visit_file(file)

    assert func.res is None

    e = visitor.errors[0]
    assert type(e) is InvalidPrefixError
    assert e.prefix == "hi.another"
    assert e.source == Path("")
