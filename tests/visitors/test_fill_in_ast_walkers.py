from pathlib import Path
from unittest.mock import Mock, call

import pytest

from restrict.compiler.ast import (
    Create,
    DataComputedField,
    EffectsComputedField,
    Lit,
    Modify,
    PipedExprList,
    Ref,
    Rel,
    Self,
    Selves,
    TaggedLit,
    Transition,
    TransitionComputedField,
    Value,
)
from restrict.compiler.types import ModuleDictionary
from restrict.compiler.visitors import AstVisitor
from restrict.compiler.transformers import AstTransformer


class Visitor(AstVisitor):
    pass


class Transformer(AstTransformer):
    pass


@pytest.mark.parametrize(
    "value,method",
    [
        (Lit(3), "visit_lit"),
        (Ref(["a"]), "visit_ref"),
        (Rel("", "", False, False, None, None, ""), "visit_rel"),
        (Self([]), "visit_self"),
        (Selves(), "visit_selves"),
        (TaggedLit("ast", [], "foo"), "visit_tagged_lit"),
        (Value(), "visit_value"),
    ],
)
def test_visitor_leaves(value, method):
    mock = Mock()
    visitor = Visitor({}, ModuleDictionary(), {}, Path(""))
    setattr(visitor, method, mock)

    visitor.visit(value)

    mock.assert_called_once_with("", value)


@pytest.mark.parametrize(
    "value,method",
    [
        (Create("", "", {}), "visit_create"),
        (DataComputedField(PipedExprList()), "visit_data_computed_field"),
        (EffectsComputedField(PipedExprList()), "visit_effects_computed_field"),
        (Lit(3), "visit_lit"),
        (Modify(Self([]), {}), "visit_modify"),
        (Ref(["a"]), "visit_ref"),
        (Rel("", "", False, False, None, None, ""), "visit_rel"),
        (Self([]), "visit_self"),
        (Selves(), "visit_selves"),
        (TaggedLit("ast", [], "foo"), "visit_tagged_lit"),
        (Transition("", "", "", "", {}), "visit_transition"),
        (
            TransitionComputedField(PipedExprList()),
            "visit_transition_computed_field",
        ),
        (Value(), "visit_value"),
    ],
)
def test_transformer_methods(value, method):
    mock = Mock(return_value=value)
    transformer = Transformer({}, ModuleDictionary(), {}, Path(""))
    setattr(transformer, method, mock)

    result = transformer.visit(value)

    assert result == value
    if value.is_leaf:
        mock.assert_has_calls([call(value), call(value)])
    else:
        mock.assert_called_once_with(value)
