from pathlib import Path

import pytest

from restrict.compiler.ast import (
    DataConstrainedField,
    File,
    Lit,
    Res,
)
from restrict.compiler.ast import PipedExprList as PEL
from restrict.compiler.exceptions.compile import InvalidConstraintError
from restrict.compiler.types import ModuleDictionary
from restrict.types.builtins import Boolean
from restrict.compiler.visitors import CheckDataConstraintTypeVisitor
from restrict.types.numeric import Integer
from restrict.types.text import Text


def vis(file: File):
    mods = ModuleDictionary()
    return CheckDataConstraintTypeVisitor({file.path: file}, mods, {}, file.path)


def test_does_not_visit_unused_resource():
    field = DataConstrainedField(
        "",
        "",
        False,
        False,
        None,
        PEL([Lit(1, Integer())]),
        Integer(),  # type: ignore
    )
    res = Res("", "", "", False, {"a": field}, {}, {}, {}, {})
    file = File(Path(""), [], [res])
    visitor = vis(file)

    visitor.visit_file(file)

    assert len(visitor.errors) == 0


def test_boolean_is_good():
    field = DataConstrainedField(
        "",
        "",
        False,
        False,
        None,
        PEL([Lit(True, Boolean())]),
        Integer(),  # type: ignore
    )
    res = Res("", "", "", False, {"a": field}, {}, {}, {}, {}, used=True)
    file = File(Path(""), [], [res])
    visitor = vis(file)

    visitor.visit_file(file)

    assert len(visitor.errors) == 0


@pytest.mark.parametrize("v,t", [(1, Integer()), ("", Text())])
def test_scalar_not_boolean_is_bad(v, t):
    field = DataConstrainedField(
        "",
        "",
        False,
        False,
        None,
        PEL([Lit(v, t)]),
        Integer(),  # type: ignore
    )
    res = Res("", "R", "", False, {"a": field}, {}, {}, {}, {}, used=True)
    file = File(Path(""), [], [res])
    visitor = vis(file)

    visitor.visit_file(file)

    assert len(visitor.errors) == 1
    e = visitor.errors[0]
    assert type(e) is InvalidConstraintError
    assert e.path == "R.a"
    assert e.type == t.name
    assert e.source == file.path
