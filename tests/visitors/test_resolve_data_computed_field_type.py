from pathlib import Path
from unittest.mock import Mock

from restrict.compiler.ast import (
    DataComputedField,
    DataConstrainedField,
    DataFields,
    File,
    Func,
    Lit,
    Rel,
    Res,
    Self,
)
from restrict.compiler.ast import (
    PipedExprList as PEL,
)
from restrict.compiler.types import (
    ResourceCollection,
    FuncParam,
    Data,
    ModuleDictionary,
    OptionalDatum,
)
from restrict.types.builtins import (
    Boolean,
    Equals,
    Map,
    Zip,
)
from restrict.compiler.visitors import ResolveDataComputedFieldTypeVisitor
from restrict.types.numeric import Add, Integer


def vis(file: File):
    return ResolveDataComputedFieldTypeVisitor(
        {file.path: file}, ModuleDictionary(), {}, file.path
    )


def test_ignores_unused_resource():
    res = Res("", "", "", False, {}, {}, {}, {}, {})
    file = File(Path("/root.restrict"), [], [res])
    visitor = vis(file)
    visitor.visit_data_computed_field = Mock()

    visitor.visit_file(file)

    visitor.visit_data_computed_field.assert_not_called()


def test_builds_adjacency_list():
    bfunc = Func("", [], [], [PEL([Lit(1, Integer())])], Add())
    a = DataComputedField(PEL([Self(["b"])]))
    b = DataComputedField(PEL([Self(["d"]), bfunc]))  # type: ignore
    c = DataComputedField(PEL([Self(["d"])]))
    d = DataComputedField(PEL([Self(["e"], [Integer(), Integer()])]))
    e = DataConstrainedField("", "", False, False, None, PEL(), Integer())

    mfunc = Func("", [], [], [PEL([Lit(1, Integer())])], Add())
    m = DataComputedField(PEL([Lit(1, Integer()), mfunc]))  # type: ignore

    nfunc = Func("", [], [], [PEL([Self(["e"], [Integer(), Integer()])])], Add())
    n = DataComputedField(PEL([Self(["y"], [Integer(), Integer()]), nfunc]))  # type: ignore

    xfunc = Func("", [], [], [PEL([Self(["c"])])], Add())
    y = DataConstrainedField("", "", False, False, None, PEL(), Integer())
    x = DataComputedField(PEL([Self(["y"], [Integer(), Integer()]), xfunc]))  # type: ignore
    z = DataComputedField(PEL([Self(["y"], [Integer(), Integer()])]))  # type: ignore

    fields = {
        "a": a,
        "b": b,
        "c": c,
        "d": d,
        "e": e,
        "m": m,
        "n": n,
        "x": x,
        "y": y,
        "z": z,
    }
    res = Res("", "", "", False, fields, {}, {}, {}, {}, used=True)
    file = File(Path(""), [], [res])
    visitor = vis(file)

    visitor.visit_file(file)

    assert visitor._adj_list == {
        "a": [("b", False)],
        "b": [("d", False)],
        "c": [("d", False)],
        "d": [("e", True)],
        "m": [],
        "n": [("y", True), ("e", True)],
        "x": [("y", True), ("c", False)],
        "z": [("y", True)],
    }


def test_topo_sorts_adjacency_list():
    bfunc = Func("", [], [], [PEL([Lit(1, Integer())])], Add())
    a = DataComputedField(PEL([Self(["b"])]))
    b = DataComputedField(PEL([Self(["d"]), bfunc]))  # type: ignore
    c = DataComputedField(PEL([Self(["d"])]))
    d = DataComputedField(PEL([Self(["e"], [Integer(), Integer()])]))
    e = DataConstrainedField("", "", False, False, None, PEL(), Integer())

    mfunc = Func("", [], [], [PEL([Lit(1, Integer())])], Add())
    m = DataComputedField(PEL([Lit(1, Integer()), mfunc]))  # type: ignore

    nfunc = Func("", [], [], [PEL([Self(["e"], [Integer(), Integer()])])], Add())
    n = DataComputedField(PEL([Self(["y"], [Integer(), Integer()]), nfunc]))  # type: ignore

    xfunc = Func("", [], [], [PEL([Self(["c"])])], Add())
    y = DataConstrainedField("", "", False, False, None, PEL(), Integer())
    x = DataComputedField(PEL([Self(["y"], [Integer(), Integer()]), xfunc]))  # type: ignore
    z = DataComputedField(PEL([Self(["y"], [Integer(), Integer()])]))  # type: ignore

    r = Rel("", "", False, False, None, None, "")

    fields = {
        "a": a,
        "b": b,
        "c": c,
        "d": d,
        "e": e,
        "m": m,
        "n": n,
        "x": x,
        "y": y,
        "z": z,
    }
    res = Res("", "", "", False, fields, {"r": r}, {}, {}, {}, used=True)
    file = File(Path(""), [], [res])
    visitor = vis(file)

    visitor.visit_file(file)

    assert res.field_sort is not None
    assert len(fields) + 1 == len(res.field_sort)
    assert res.field_sort == ["e", "y", "r", "d", "m", "n", "z", "b", "c", "x", "a"]


def test_data_computed_field_gets_type_from_pel():
    lit = Lit(3, Integer())
    a = DataComputedField(PEL([lit]))
    res = Res("", "", "", False, {"a": a}, {}, {}, {}, {}, used=True)
    file = File(Path(""), [], [res])
    visitor = vis(file)

    visitor.visit_file(file)

    assert a.func_res == Integer()
    assert a.res == Integer()


def test_data_computed_field_gets_type_from_pel_again():
    add = Add()
    func = Func("", [], [], [PEL([Lit(1, Integer())])], add)
    lit = Lit(3, Integer())
    a = DataComputedField(PEL([lit, func]))  # type: ignore
    res = Res("", "", "", False, {"a": a}, {}, {}, {}, {}, used=True)
    file = File(Path(""), [], [res])
    visitor = vis(file)

    visitor.visit_file(file)

    assert a.func_res == Integer()
    assert a.res == Integer()


def test_data_computed_fields_in_order():
    bfunc = Func("", [], [], [PEL([Lit(1, Integer())])], Add())
    a = DataComputedField(PEL([Self(["b"])]))
    b = DataComputedField(PEL([Self(["d"]), bfunc]))  # type: ignore
    c = DataComputedField(PEL([Self(["d"])]))
    d = DataComputedField(PEL([Self(["e"], [Integer(), Integer()])]))
    e = DataConstrainedField("", "", False, False, None, PEL(), Integer())

    mfunc = Func("", [], [], [PEL([Lit(1, Integer())])], Add())
    m = DataComputedField(PEL([Lit(1, Integer()), mfunc]))  # type: ignore

    nfunc = Func("", [], [], [PEL([Self(["e"], [Integer(), Integer()])])], Add())
    n = DataComputedField(PEL([Self(["y"], [Integer(), Integer()]), nfunc]))  # type: ignore

    xfunc = Func("", [], [], [PEL([Self(["c"])])], Add())
    y = DataConstrainedField("", "", False, False, None, PEL(), Integer())
    x = DataComputedField(PEL([Self(["y"], [Integer(), Integer()]), xfunc]))  # type: ignore
    z = DataComputedField(PEL([Self(["y"], [Integer(), Integer()])]))  # type: ignore

    fields = {
        "a": a,
        "b": b,
        "c": c,
        "d": d,
        "e": e,
        "m": m,
        "n": n,
        "x": x,
        "y": y,
        "z": z,
    }
    res = Res("", "", "", False, fields, {}, {}, {}, {}, used=True)
    file = File(Path(""), [], [res])
    visitor = vis(file)

    visitor.visit_file(file)

    assert a.func[0].res == Integer()
    assert a.func_res == Integer()
    assert b.func[0].res == Integer()
    assert b.func_res == Integer()
    assert c.func[0].res == Integer()
    assert c.func_res == Integer()
    assert d.func_res == Integer()
    assert m.func_res == Integer()
    assert n.func_res == Integer()
    assert x.func_res == Integer()
    assert z.func_res == Integer()
    assert xfunc.args[0][0].res == Integer()


def test_data_computed_field_computes_to_optional_value():
    req_src = DataConstrainedField("", "", False, False, None, PEL(), Integer())
    req_cmp = DataComputedField(PEL([Self(["a"], [Integer()])]))
    opt_src = DataConstrainedField(
        "", "", True, False, None, PEL(), OptionalDatum(Integer())
    )
    opt_cmp = DataComputedField(PEL([Self(["b"], [OptionalDatum(Integer())])]))
    fields = {"a": req_src, "c_a": req_cmp, "b": opt_src, "c_b": opt_cmp}
    res = Res("", "", "", False, fields, {}, {}, {}, {}, used=True)
    file = File(Path(""), [], [res])
    visitor = vis(file)

    visitor.visit_file(file)

    assert req_cmp.func[0].res == Integer()
    assert req_cmp.func_res == Integer()
    assert opt_cmp.func[0].res == OptionalDatum(Integer())
    assert opt_cmp.func_res == OptionalDatum(Integer())


def test_data_computed_fields_also_does_list_reduction():
    ca = DataConstrainedField("", "", False, False, None, PEL(), Integer())
    fields: DataFields = {"correct_answer": ca}
    question = Res("", "Question", "", False, fields, {}, {}, {}, {}, used=True)

    questions = ResourceCollection(list, question.bridge)
    q = Rel("", "", False, False, None, None, "", questions)
    dnc = {"questions": q}
    quiz = Res("", "Quiz", "", False, {}, dnc, {}, {}, {}, used=True)

    choice = DataConstrainedField("", "", False, False, None, PEL(), Integer())
    response = Res(
        "", "Response", "", False, {"choice": choice}, {}, {}, {}, {}, used=True
    )

    x = FuncParam("x")
    y = FuncParam("y")
    responses = ResourceCollection(list, response.bridge)
    score = DataComputedField(
        PEL(
            [
                Self(["quiz", "questions"], [Integer(), quiz.bridge, questions]),
                Func(
                    "",
                    [],
                    [],
                    [PEL([Self(["responses"], [Integer(), responses])])],  # type: ignore
                    Zip(),
                ),
                Func(
                    "",
                    [],
                    [x, y],
                    [
                        PEL(
                            [
                                x.create_ref(["correct_answer"]),
                                Func(
                                    "",
                                    [],
                                    [],
                                    [PEL([y.create_ref(["choice"])])],
                                    Equals(),
                                ),
                            ]
                        )
                    ],
                    Map(),
                ),
            ]
        )
    )
    quiz_rel = Rel("", "", False, False, None, None, "", quiz.bridge)
    responses_rel = Rel("", "", False, False, list, None, "", responses)
    attempt = Res(
        "",
        "Attempt",
        "",
        False,
        {"score": score},
        {"quiz": quiz_rel, "responses": responses_rel},
        {},
        {},
        {},
        used=True,
    )
    file = File(Path(""), [], [question, quiz, response, attempt])
    visitor = vis(file)

    visitor.visit_file(file)

    assert score.func_res == Data(list, Boolean())
