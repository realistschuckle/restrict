from datetime import datetime, timezone
from decimal import Decimal
from itertools import product
from pathlib import Path
from unittest.mock import Mock
import os

import pytest

from restrict.compiler.ast import (
    Create,
    DataComputedField,
    DataConstrainedField,
    EffectsComputedField,
    File,
    Func,
    Lit,
    Modify,
    PipedExprList,
    Ref,
    Res,
    SecurityConstraintField,
    Self,
    Selves,
    TaggedLit,
    Value,
)
from restrict.compiler.ast import (
    PipedExprList as PEL,
)
from restrict.compiler.compiled import Datum, LayeredMapping
from restrict.compiler.exceptions.compile import (
    InvalidExpressionError,
    InvalidFuncCallError,
)
from restrict.compiler.types import (
    FuncParam,
    OptionalDatum,
    Data,
    Function,
    ModuleDictionary,
    Resource,
)
from restrict.types.builtins import (
    All,
    And,
    Any,
    Boolean,
    Equals,
    Filter,
    GreaterThan,
    GreaterThanOrEqual,
    LessThan,
    LessThanOrEqual,
    Map,
    Not,
    Or,
    Zip,
)
from restrict.compiler.types import (
    LayeredMapping as LM,
)
from restrict.compiler.visitors import (
    CompilePipedExpressionListsToPythonFunctionsVisitor,
    ResolveLitTypeVisitor,
)
from restrict.types.numeric import Add, Integer, Negate, Subtract
from restrict.types.text import RandomString, Text, TextLen
from restrict.types.time import Interval, Now, TimeAdd, Timestamp
from restrict.types.system import EnvVar, getenv


class Placeholder:
    value: str

    def __init__(self, value):
        self.value = value


def vis(file):
    return CompilePipedExpressionListsToPythonFunctionsVisitor(
        {file.path: file}, ModuleDictionary(), {}, file.path
    )


def pel(*args):
    args_ = [Lit(x) if type(x) in [int, str, list, bool] else x for x in args]
    mods = ModuleDictionary()
    vis = ResolveLitTypeVisitor({}, mods, {}, Path(""))
    for a in args_:
        if isinstance(a, Lit):
            vis.visit_lit("", a)
    return PEL(args_)


def dcsf(pel):
    return DataConstrainedField("", "", False, False, None, pel)


def file_dcf(field: DataComputedField | DataConstrainedField, used: bool = True):
    res = Res("", "", "", False, {"field": field}, {}, {}, {}, {}, used=used)
    file = File(Path("/root.restrict"), [], [res])
    return file


def file_sec(field: SecurityConstraintField):
    res = Res("", "", "", False, {}, {}, {}, {"list": field}, {}, used=True)
    file = File(Path("/root.restrict"), [], [res])
    return file


def file_eff(field: EffectsComputedField):
    res = Res(
        "",
        "",
        "",
        False,
        {},
        {},
        {"create": {"field": field}},
        {},
        {},
        used=True,
    )
    file = File(Path("/root.restrict"), [], [res])
    return file


def func(res, *args):
    pels = [pel(*a) for a in args]
    return Func("", [], [], pels, res=res)


def test_constrained_field():
    field = dcsf(PEL([Lit(True, Boolean())]))
    file = file_dcf(field)
    visitor = vis(file)

    visitor.visit_file(file)

    assert field.compiled is not None
    assert field.compiled(LM()) is True


def test_constrained_field_with_no_constraint():
    field = dcsf(PEL())
    file = file_dcf(field)
    visitor = vis(file)

    visitor.visit_file(file)

    assert field.compiled is not None
    assert field.compiled(LM()) is True


@pytest.mark.parametrize(
    "field, fn",
    [
        (DataComputedField, file_dcf),
        (dcsf, file_dcf),
        (EffectsComputedField, file_eff),
        (SecurityConstraintField, file_sec),
    ],
)
def test_does_not_compile_fields_with_already_compiled_func(field, fn):
    def thing(_):
        return 1

    field = field(PEL([]))
    field.compiled = thing
    file = fn(field)
    visitor = vis(file)

    visitor.visit_file(file)

    assert field.compiled == thing


@pytest.mark.parametrize("v", ["3", True, 3, Decimal("3.0")])
def test_computed_field(v):
    field = DataComputedField(PEL([Lit(v)]))
    file = file_dcf(field)
    visitor = vis(file)

    visitor.visit_file(file)

    assert field.compiled is not None
    assert field.compiled(LM()) == v


@pytest.mark.parametrize("v", ["3", True, 3, Decimal("3.0"), {}, []])
def test_value_lookup(v):
    field = DataConstrainedField("", "", False, False, None, PEL([Value()]))
    file = file_dcf(field)
    visitor = vis(file)

    visitor.visit_file(file)

    assert field.compiled is not None
    assert field.compiled(LM({"value": v})) == v


@pytest.mark.parametrize("k", [[], ["a"], ["b", "c"], ["d", "e", "f"]])
def test_self_lookup(k):
    attr = {}
    for i, name in enumerate(reversed(k)):
        if i == 0:
            attr[name] = 10  # type: ignore
        else:
            attr = {name: type(name.upper(), (), attr)()}
    s = type("S", (), attr)()
    expected = s if len(k) == 0 else 10
    field = DataComputedField(PEL([Self(k)]))
    file = file_dcf(field)
    visitor = vis(file)

    visitor.visit_file(file)

    assert field.compiled is not None
    assert field.compiled(LM({"self": s})) == expected


@pytest.mark.parametrize("v", ["3", True, 3, Decimal("3.0"), {}, []])
def test_selves_lookup(v):
    field = DataComputedField(PEL([Selves()]))
    file = file_dcf(field)
    visitor = vis(file)

    visitor.visit_file(file)

    assert field.compiled is not None
    assert field.compiled(LM({"selves": lambda: [v]})) == [v]


@pytest.mark.parametrize("k", [["a"], ["b", "c"], ["d", "e", "f"]])
def test_ref_lookup(k):
    context = {k[-1]: 10}
    for name in reversed(k[:-1]):
        context = {name: type(name.upper(), (), context)()}
    field = SecurityConstraintField(PEL([Ref(k)]))
    file = file_sec(field)
    visitor = vis(file)

    visitor.visit_file(file)

    assert field.compiled is not None
    assert field.compiled(context) == 10  # type: ignore


@pytest.mark.parametrize(
    "pel,expected",
    [
        (PEL([func(Add(), [10], [5])]), 15),
        (PEL([func(Negate(), [10])]), -10),
        (PEL([func(Subtract(), [10], [5])]), 5),
        (PEL([func(TextLen(), ["hello"])]), 5),
        (PEL([func(RandomString(), [8])]), None),
        (PEL([func(EnvVar(), ["PATH"])]), os.environ["PATH"]),
        (PEL([func(EnvVar(), ["PATH"], ["my_value"])]), os.environ["PATH"]),
        (PEL([func(EnvVar(), ["DOES NOT EXIST"], ["my_value"])]), "my_value"),
        (
            PEL(
                [
                    func(
                        TimeAdd(),
                        [TaggedLit("ts", [], "2020-01-01T00:00:00Z", Timestamp())],
                        [TaggedLit("ti", [], "P1D", Interval())],
                    )
                ]
            ),
            datetime(2020, 1, 2, 0, 0, 0, 0, timezone.utc),
        ),
        (PEL([func(Now())]), None),
        (PEL([func(All(), [[True, True, True]])]), True),
        (PEL([func(All(), [[True, True, False]])]), False),
        (PEL([func(Any(), [[False, False, True]])]), True),
        (PEL([func(Any(), [[False, False, False]])]), False),
        (PEL([func(Not(), [True])]), False),
        (PEL([func(Not(), [False])]), True),
        (PEL([func(And(), [False], [True])]), False),
        (PEL([func(And(), [True], [True])]), True),
        (PEL([func(Or(), [False], [False])]), False),
        (PEL([func(Or(), [True], [True])]), True),
        (PEL([func(GreaterThan(), [3], [1])]), True),
        (PEL([func(GreaterThan(), [1], [1])]), False),
        (PEL([func(GreaterThan(), [-3], [1])]), False),
        (PEL([func(GreaterThanOrEqual(), [3], [1])]), True),
        (PEL([func(GreaterThanOrEqual(), [1], [1])]), True),
        (PEL([func(GreaterThanOrEqual(), [-3], [1])]), False),
        (PEL([func(LessThan(), [3], [1])]), False),
        (PEL([func(LessThan(), [1], [1])]), False),
        (PEL([func(LessThan(), [-3], [1])]), True),
        (PEL([func(LessThanOrEqual(), [3], [1])]), False),
        (PEL([func(LessThanOrEqual(), [1], [1])]), True),
        (PEL([func(LessThanOrEqual(), [-3], [1])]), True),
        (PEL([func(Equals(), [-3], [1])]), False),
        (PEL([func(Equals(), [-3], [-3])]), True),
        (PEL([func(Zip(), [[1, 2]], [[2, 3]])]), [(1, 2), (2, 3)]),
    ],
)
def test_functions(pel: PEL, expected):
    field = DataComputedField(pel)
    file = file_dcf(field)
    visitor = vis(file)

    visitor.visit_file(file)

    assert field.compiled is not None
    if expected is not None:
        result = field.compiled(LM())
        if type(result) is zip:
            result = list(result)
        assert result == expected
    else:
        assert field.compiled(LM()) is not None


def test_env_raises_key_error_when_key_does_not_exist_and_no_default():
    with pytest.raises(KeyError):
        getenv("THIS SHOULD NOT EXIST. IT HAS SPACES FOR GOODNESS SAKE.")


def _p(fn, t, arg=None):
    if arg is None:
        return (
            PEL([func(fn, [Self(["a"], [t, OptionalDatum(t)])])]),
            t,
        )
    return (
        PEL([func(fn, [Self(["a"], [t, OptionalDatum(t)])], pel(*arg))]),
        t,
    )


def ct(t):
    return Data(list, t)


@pytest.mark.parametrize(
    "pel,t",
    [
        _p(Add(), Integer(), [5]),
        _p(Negate(), Integer()),
        _p(Subtract(), Integer(), [5]),
        _p(TextLen(), Text()),
        _p(All(), ct(Boolean())),
        _p(Any(), ct(Boolean())),
        _p(Not(), Boolean()),
        _p(And(), Boolean(), [True]),
        _p(Or(), Boolean(), [True]),
        _p(GreaterThan(), Integer(), [3]),
        _p(GreaterThanOrEqual(), Integer(), [3]),
        _p(LessThan(), Integer(), [3]),
        _p(LessThanOrEqual(), Integer(), [3]),
        _p(Equals(), Integer(), [3]),
        _p(Zip(), ct(Integer()), [[3, 4]]),
    ],
)
def test_functions_with_optional_arguments(pel: PEL, t: Datum):
    dcsf = DataConstrainedField("", "", True, False, None, PEL(), OptionalDatum(t))
    field = DataComputedField(pel)
    file = file_dcf(field)
    file.resources[0].fields["a"] = dcsf
    selfo = type("Selfo", (), {"a": None})
    visitor = vis(file)

    visitor.visit_file(file)

    assert field.compiled is not None
    assert field.compiled(LM({"self": selfo})) is None


def test_security_constraint_field_cx_returns_true_for_none_value():
    layer = LayeredMapping()
    pel_ = pel(Value(OptionalDatum(Integer())), func(GreaterThan(), [3]))
    field = SecurityConstraintField(pel_)
    file = file_sec(field)
    visitor = vis(file)

    visitor.visit_file(file)

    assert field.compiled is not None
    assert field.compiled(layer.layer({"value": None})) is True
    assert field.compiled(layer.layer({"value": 0})) is False
    assert field.compiled(layer.layer({"value": 4})) is True


def test_data_constrained_field_cx_returns_true_for_none_value():
    pel_ = pel(Value(OptionalDatum(Integer())), func(GreaterThan(), [3]))
    field = DataConstrainedField("", "", True, False, None, pel_)
    file = file_dcf(field)
    visitor = vis(file)

    visitor.visit_file(file)

    assert field.compiled is not None
    assert field.compiled(LM({"value": None})) is True
    assert field.compiled(LM({"value": 0})) is False
    assert field.compiled(LM({"value": 4})) is True


@pytest.mark.parametrize(
    "op,expected,coll",
    [
        (x[0], x[1], a)
        for x, a in product([(Map(), [0, 1, 2]), (Filter(), [2, 3])], [list, set])
    ],
)
def test_parameterized_functions(
    op: Function,
    expected,
    coll: type[list] | type[set],
):
    x = FuncParam("x")
    add = Func("add", [], [], [PipedExprList([Lit(-1, Integer())])], res=Add())
    func = Func("map", [], [x], [PipedExprList([x.create_ref([]), add])], res=op)
    coll_type = Data(coll, Integer())
    pel = PipedExprList([Lit(coll([1, 2, 3]), res=coll_type), func])

    field = DataComputedField(pel)  # type: ignore
    file = file_dcf(field)
    visitor = vis(file)

    visitor.visit_file(file)

    assert field.compiled is not None
    assert list(field.compiled(LM())) == expected


@pytest.mark.parametrize(
    "coll,op,expected",
    [
        (x, a[0], a[1])
        for x, a in product([list, set], [(Map(), [0, 1, 2]), (Filter(), [2, 3])])
    ],
)
def test_parameterized_functions_returns_collection_type_handed_to_it(
    coll: type[list] | type[set],
    op: Function,
    expected: list[int],
):
    x = FuncParam("x")
    add = Func("add", [], [], [pel(-1)], res=Add())
    func = Func("map", [], [x], [PipedExprList([x.create_ref([]), add])], res=op)
    coll_type = Data(coll, Integer())
    pel_ = PipedExprList([Lit([1, 2, 3], res=coll_type), func])

    field = DataComputedField(pel_)  # type: ignore
    file = file_dcf(field)
    visitor = vis(file)

    visitor.visit_file(file)

    assert field.compiled is not None
    assert field.compiled(LM()) == coll(expected)


@pytest.mark.parametrize("op", [Map(), Filter()])
def test_does_not_visit_unused_resources(op):
    x = FuncParam("x")
    add = Func("add", [], [], [PipedExprList([Lit(1)])], res=Add())
    another = Func("", [], [], [], res=Now())
    func = Func(
        "map",
        [],
        [x],
        [PipedExprList([x.create_ref([]), add]), PipedExprList([another])],
        res=op,
    )
    coll_type = Data(set, Integer())
    pel = PipedExprList([Lit([1, 2, 3], res=coll_type), func])

    field = DataComputedField(pel)  # type: ignore
    file = file_dcf(field, False)
    visitor = vis(file)

    visitor.visit_file(file)


@pytest.mark.parametrize(
    "op,field,fn",
    [
        (op, f[0], f[1])
        for op, f in product(
            [Map(), Filter()],
            [
                (DataComputedField, file_dcf),
                (dcsf, file_dcf),
                (EffectsComputedField, file_eff),
                (SecurityConstraintField, file_sec),
            ],
        )
    ],
)
def test_parameterized_functions_raises_error_when_args_are_not_length_two(
    op, field, fn
):
    x = FuncParam("x")
    add = Func("add", [], [], [PipedExprList([Lit(1, Integer())])], res=Add())
    another = Func("", [], [], [], res=Now())
    func = Func(
        "map",
        [],
        [x],
        [PipedExprList([x.create_ref([]), add]), PipedExprList([another])],
        res=op,
    )
    coll_type = Data(set, Integer())
    pel = PipedExprList([Lit([1, 2, 3], res=coll_type), func])

    field = field(pel)  # type: ignore
    file = fn(field)
    visitor = vis(file)

    visitor.visit_file(file)

    assert len(visitor.errors) == 1
    e = visitor.errors[0]
    assert type(e) is InvalidFuncCallError
    assert e.name == op.name
    assert e.resource == file.resources[0].name
    assert e.source == file.path
    assert e.expected_num_args == 2
    assert e.received_num_args == 3


def test_function_pipeline():
    pel_ = pel(10, func(Add(), pel(5)))
    field = EffectsComputedField(pel_)
    file = file_eff(field)
    visitor = vis(file)

    visitor.visit_file(file)

    assert field.compiled is not None
    assert field.compiled(LayeredMapping()) == 15


@pytest.mark.parametrize(
    "field,fn",
    [
        (dcsf, file_dcf),
        (DataComputedField, file_dcf),
        (SecurityConstraintField, file_sec),
        (EffectsComputedField, file_eff),
    ],
)
def test_pipeline_fails_when_not_function_after_first_entry(field, fn):
    field = field(PEL([Lit(10), Lit(20)]))
    file = fn(field)
    visitor = vis(file)

    visitor.visit_file(file)

    assert len(visitor.errors) == 1
    e = visitor.errors[0]
    assert type(e) is InvalidExpressionError


def test_function_pipeline_with_nested_function():
    # -10 - (0 + (9 - value)), value = 1
    # 10 |> neg |> subtract(add(0, 9|> subtract(value)))
    inner_sub = func(Subtract(), [Value(res=Integer())])
    inner_add = func(Add(), [0], [9, inner_sub])
    sub = func(Subtract(), [inner_add])
    neg = func(Negate())
    fn = pel(Lit(10), neg, sub)
    field = DataComputedField(fn)
    file = file_dcf(field)
    visitor = vis(file)

    visitor.visit_file(file)

    assert field.compiled is not None
    assert field.compiled(LM({"value": 1})) == -18


def test_create_calls_constructor_then_create_on_bridge_class():
    file = File(Path("/this/is/a/thing"), [], [])

    thing_factory = Mock(return_value=1)
    thing_factory._create = Mock(return_value={})
    thing = Res("", "Thing", "", False, {}, {}, {}, {}, {}, file=file)

    other_factory = Mock(return_value=2)
    other_factory._create = Mock(return_value="I AM OTHER_INST")
    other = Res("", "OtherThing", "", False, {}, {}, {}, {}, {}, file=file)

    create = Create(
        "",
        "",
        {
            "a": EffectsComputedField(PipedExprList([Lit(True)])),
            "b": EffectsComputedField(PipedExprList([Lit(1)])),
            "c": EffectsComputedField(
                PipedExprList(
                    [
                        Create("", "", {}, other.bridge),
                    ]
                )
            ),
        },
        thing.bridge,
    )
    field = EffectsComputedField(PipedExprList([create]), thing.bridge)
    file = file_eff(field)
    visitor = vis(file)

    visitor.visit_file(file)

    assert field.compiled is not None

    mapping = LayeredMapping()
    layer = mapping.layer()
    mapping[thing.bridge.compiled_name()] = thing_factory
    mapping[other.bridge.compiled_name()] = other_factory
    assert field.compiled(layer) == {}
    thing_factory.assert_not_called()
    thing_factory._create.assert_called_once()


def test_modify_calls_update_on_existing_resource():
    t_value = Mock(name="t_value", return_value=1)
    t_value._alter = Mock(name="t_value.modify", return_value=2)
    o_value = Mock(name="o_value", return_value=3)
    o_value._alter = Mock(name="o_value.modify", return_value=4)

    def t(self):
        self.t_called = self.t_called + 1 if hasattr(self, "t_called") else 1
        return t_value

    def o(self):
        self.o_called = self.o_called + 1 if hasattr(self, "o_called") else 1
        return o_value

    selfo = type(
        "Selfo",
        (Resource,),
        {
            "t": property(t),
            "o": property(o),
            "name": lambda _: "Selfo",
            "compiled_name": lambda _: "",
            "resolve_path": lambda _: [],
            "get_global_names": lambda _: {},
            "get_effects": lambda _: {},
            "get_rules": lambda _: {},
            "get_singular_relations": lambda _: {},
            "get_fields": lambda _: {},
            "get_relations": lambda _: {},
            "to_schema": lambda _: {},
            "field_order": lambda _: [],
        },
    )()
    file = File(Path("/this/is/a/thing"), [], [])
    thing = Res("", "Thing", "", False, {}, {}, {}, {}, {}, file=file)
    other = Res("", "Other", "", False, {}, {}, {}, {}, {}, file=file)
    create = Modify(
        Self(["t"]),
        {
            "a": EffectsComputedField(PipedExprList([Lit(True)])),
            "b": EffectsComputedField(PipedExprList([Lit(1)])),
            "c": EffectsComputedField(
                PipedExprList(
                    [
                        Modify(Self(["o"]), {}, other.bridge),
                    ]
                )
            ),
        },
        thing.bridge,
    )
    field = EffectsComputedField(PipedExprList([create]), thing.bridge)
    file = file_eff(field)
    visitor = vis(file)

    visitor.visit_file(file)

    assert field.compiled is not None

    assert field.compiled({"self": selfo}) == 2  # type: ignore
    t_value.assert_not_called()
    t_value._alter.assert_called_once_with({"self": selfo, "a": True, "b": 1, "c": 4})
    o_value.assert_not_called()
    o_value._alter.assert_called_once_with({"self": selfo})
