from pathlib import Path

import pytest

from restrict.compiler.ast import (
    File,
    Res,
    SecurityConstraintField,
)
from restrict.compiler.ast import (
    PipedExprList as PEL,
)
from restrict.compiler.types import (
    EffName,
    ModuleDictionary,
    Rule,
)
from restrict.compiler.visitors import ResolveInheritedSecurityVisitor

from .fake_module import OverrideMe


def vis(file: File):
    mods = ModuleDictionary()
    return ResolveInheritedSecurityVisitor({file.path: file}, mods, {}, file.path)


@pytest.mark.parametrize(
    "sec,oth",
    [("create", "modify"), ("modify", "delete"), ("delete", "create")],
)
def test_effects_copied_from_declared_resource(sec: EffName, oth: EffName):
    a = SecurityConstraintField(PEL())
    b = SecurityConstraintField(PEL())
    base = Res("", "B", "", False, {}, {}, {}, {sec: {"a": a, "b": b}}, {})

    b = SecurityConstraintField(PEL())
    c = SecurityConstraintField(PEL())
    d = SecurityConstraintField(PEL())
    res = Res(
        "",
        "R",
        "",
        True,
        {},
        {},
        {},
        {oth: {"d": d}, sec: {"b": b, "c": c}},
        {},
        base.bridge,
    )
    file = File(Path(""), [], [res])
    visitor = vis(file)

    visitor.visit_file(file)

    assert res.rules[sec]["a"] == a  # type: ignore
    assert res.rules[sec]["b"] == b  # type: ignore
    assert res.rules[sec]["c"] == c  # type: ignore
    assert res.rules[oth]["d"] == d  # type: ignore
    assert res.security_updated


def test_effects_copied_from_compiled_resource():
    sec = "create"
    oth = "modify"
    b = SecurityConstraintField(PEL())
    c = SecurityConstraintField(PEL())
    d = SecurityConstraintField(PEL())
    res = Res(
        "",
        "R",
        "",
        True,
        {},
        {},
        {},
        {
            oth: {"d": d},
            sec: {"b": b, "c": c},
            "details": SecurityConstraintField(PEL()),
        },
        {},
        OverrideMe(),
    )
    file = File(Path(""), [], [res])
    visitor = vis(file)

    visitor.visit_file(file)

    assert res.rules[sec]["a"] is not None  # type: ignore
    assert res.rules[sec]["b"] == b  # type: ignore
    assert res.rules[sec]["c"] == c  # type: ignore
    assert res.rules[oth]["d"] == d  # type: ignore
    assert type(res.rules["list"]) is Rule
    assert type(res.rules["delete"]) is Rule
    assert res.security_updated
