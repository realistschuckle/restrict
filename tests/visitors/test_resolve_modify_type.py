from pathlib import Path

from restrict.compiler.ast import (
    Effects,
    EffectsComputedField,
    File,
    Modify,
    Rel,
    Res,
    Self,
)
from restrict.compiler.ast import (
    PipedExprList as PEL,
)
from restrict.compiler.exceptions.compile import (
    InvalidModifyCollectionError,
    InvalidModifyValueError,
)
from restrict.compiler.types import (
    ModuleDictionary,
    OptionalResource,
    ResourceCollection,
)
from restrict.compiler.visitors import ResolveModifyTypeVisitor
from restrict.types.builtins import Boolean


def vis(file: File):
    return ResolveModifyTypeVisitor(
        {file.path: file}, ModuleDictionary(), {}, file.path
    )


def test_modify_resolves_to_bridge_of_rel():
    other = Res("", "", "", False, {}, {}, {}, {}, {}, used=True)
    modify = Modify(Self(["a"]), {})
    field = EffectsComputedField(PEL([modify]))
    effects: Effects = {"modify": {"a": field}}
    a = Rel("", "", False, False, None, None, "", other.bridge)
    res = Res("", "", "", False, {}, {"a": a}, effects, {}, {}, used=True)
    file = File(Path(""), [], [res])
    res.file = file
    visitor = vis(file)

    visitor.visit_file(file)

    assert modify.res == other.bridge


def test_modify_resolves_to_optional_bridge_of_rel():
    other = Res("", "", "", False, {}, {}, {}, {}, {}, used=True)
    modify = Modify(Self(["a"]), {})
    field = EffectsComputedField(PEL([modify]))
    effects: Effects = {"modify": {"a": field}}
    t = OptionalResource(other.bridge)
    a = Rel("", "", False, False, None, None, "", t)
    res = Res("", "", "", False, {}, {"a": a}, effects, {}, {}, used=True)
    file = File(Path(""), [], [res])
    res.file = file
    visitor = vis(file)

    visitor.visit_file(file)

    assert modify.res == OptionalResource(other.bridge)


def test_modify_cannot_work_with_collection_types():
    other = Res("", "", "", False, {}, {}, {}, {}, {}, used=True)
    modify = Modify(Self(["a"]), {})
    field = EffectsComputedField(PEL([modify]))
    effects: Effects = {"modify": {"a": field}}
    coll = ResourceCollection(list, other.bridge)
    a = Rel("", "", False, False, None, None, "", coll)
    res = Res("", "R", "", False, {}, {"a": a}, effects, {}, {}, used=True)
    file = File(Path(""), [], [res])
    res.file = file
    visitor = vis(file)

    visitor.visit_file(file)

    assert modify.res is None

    assert len(visitor.errors) == 1
    e = visitor.errors[0]
    assert type(e) is InvalidModifyCollectionError
    assert e.prop == "a"
    assert e.resource == "R"
    assert e.source == file.path


def test_modify_cannot_work_with_data():
    modify = Modify(Self(["a"]), {})
    field = EffectsComputedField(PEL([modify]))
    effects: Effects = {"modify": {"a": field}}
    a = Rel("", "", False, False, None, None, "", Boolean())  # type: ignore
    res = Res("", "R", "", False, {}, {"a": a}, effects, {}, {}, used=True)
    file = File(Path(""), [], [res])
    res.file = file
    visitor = vis(file)

    visitor.visit_file(file)

    assert modify.res is None

    assert len(visitor.errors) == 1
    e = visitor.errors[0]
    assert type(e) is InvalidModifyValueError
    assert e.prop == "a"
    assert e.resource == "R"
    assert e.source == file.path


def test_nested_modify_resolves_to_bridge_of_rel():
    """
    thing Carruther {}
    thing Another {
        dnc { c: Carruther; }
    }
    thing Other {
        dnc { b: OtherOther; z: Carruther; }
    }
    thing Thing {
        dnc { a: Other; }
        effects {
            modify {
                a = modify a {
                    b = modify b { c = modify c{} };
                    z = modify z {};
                };
            }
        }
    }
    """
    carruther = Res("", "Carruther", "", False, {}, {}, {}, {}, {}, used=True)

    c = Rel("", "", False, False, None, None, "", carruther.bridge)
    another = Res("", "Another", "", False, {}, {"c": c}, {}, {}, {}, used=True)

    b = Rel("", "", False, False, None, None, "", another.bridge)
    z = Rel("", "", False, False, None, None, "", carruther.bridge)
    other = Res("", "Other", "", False, {}, {"b": b, "z": z}, {}, {}, {}, used=True)

    z_outer = Modify(Self(["z"]), {})
    z_field = EffectsComputedField(PEL([z_outer]))

    inner_inner = Modify(Self(["c"]), {})
    inner_inner_eff = EffectsComputedField(PEL([inner_inner]))
    inner = Modify(Self(["b"]), {"c": inner_inner_eff})
    inner_eff = EffectsComputedField(PEL([inner]))
    outer = Modify(Self(["a"]), {"b": inner_eff, "z": z_field})
    a_field = EffectsComputedField(PEL([outer]))

    effects: Effects = {"modify": {"a": a_field}}
    a = Rel("", "", False, False, None, None, "", other.bridge)
    res = Res("", "Thing", "", False, {}, {"a": a}, effects, {}, {}, used=True)
    file = File(Path(""), [], [res, other, another, carruther])
    for resource in file.resources:
        resource.file = file
    visitor = vis(file)

    visitor.visit_file(file)

    assert outer.res == other.bridge
    assert inner.res == another.bridge
    assert inner_inner.res == carruther.bridge
    assert z_outer.res == carruther.bridge
