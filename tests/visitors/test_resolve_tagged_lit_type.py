from itertools import product
from pathlib import Path

import pytest

from restrict.compiler.ast import (
    DataComputedField,
    DataConstrainedField,
    Effects,
    EffectsComputedField,
    File,
    Import,
    PipedExprList,
    Res,
    Rules,
    SecurityConstraintField,
    TaggedLit,
    Transition,
    TransitionComputedField,
    Transitions,
)
from restrict.compiler.exceptions.compile import (
    AmbiguousTagError,
    InvalidPrefixError,
    InvalidTaggedValue,
    UnknownTagError,
)
from restrict.compiler.types import (
    ModuleDictionary,
)
from restrict.compiler.visitors import ResolveTaggedLitTypeVisitor

from .fake_module import TreasuryService, restrict_module


def vis(file: File):
    mods = ModuleDictionary()
    mods[Path("/fake_module")] = restrict_module
    return ResolveTaggedLitTypeVisitor({file.path: file}, mods, {}, file.path)


def dcsf(s):
    pel = PipedExprList([s])
    a = DataConstrainedField("", "", False, False, None, pel)
    res = Res("", "", "", False, {"a": a}, {}, {}, {}, {}, used=True)
    file = File(Path(""), [], [res])
    return file


def dcpf(s):
    pel = PipedExprList([s])
    a = DataComputedField(pel)
    res = Res("", "", "", False, {"a": a}, {}, {}, {}, {}, used=True)
    file = File(Path(""), [], [res])
    return file


def eff(s):
    pel = PipedExprList([s])
    a = EffectsComputedField(pel)
    effects: Effects = {"create": {"a": a}}
    res = Res("", "", "", False, {}, {}, effects, {}, {}, used=True)
    file = File(Path(""), [], [res])
    return file


def sec(s):
    pel = PipedExprList([s])
    a = SecurityConstraintField(pel)
    rules: Rules = {"list": a}
    res = Res("", "", "", False, {}, {}, {}, rules, {}, used=True)
    file = File(Path(""), [], [res])
    return file


def tx(s):
    pel = PipedExprList([s])
    a = TransitionComputedField(pel)
    tx = Transition("", "", "", "", {"a": a})
    txs: Transitions = {"list": tx}
    res = Res("", "", "", False, {}, {}, {}, {}, txs, used=True)
    file = File(Path(""), [], [res])
    return file


@pytest.mark.parametrize(
    "factory,prefix",
    product([dcsf, dcpf, eff, sec, tx], ["x", ""]),
)
def test_finds_tagged_value(factory, prefix):
    p = [prefix] if prefix else []
    lit = TaggedLit("tss", p, "2020-01-01T00:00:00Z")
    file = factory(lit)
    file.imports.append(Import(Path("/fake_module"), prefix))
    visitor = vis(file)

    visitor.visit_file(file)

    assert lit.res == TreasuryService()


@pytest.mark.parametrize(
    "factory",
    [dcsf, dcpf, eff, sec, tx],
)
def test_ambiguous_tag_errs(factory):
    lit = TaggedLit("tss", [], "2020-01-01T00:00:00Z")
    file = factory(lit)
    file.imports.append(Import(Path("/fake_module"), ""))
    file.imports.append(Import(Path("/fake_module_2"), ""))
    visitor = vis(file)
    visitor._mods[Path("/fake_module_2")] = restrict_module

    visitor.visit_file(file)

    assert lit.res is None

    assert len(visitor.errors) == 1
    e = visitor.errors[0]
    assert type(e) is AmbiguousTagError
    assert e.tag == "tss"
    assert e.prefix == ""
    assert e.source == file.path


@pytest.mark.parametrize(
    "factory,prefix",
    product([dcsf, dcpf, eff, sec, tx], ["x", ""]),
)
def test_unknown_tag_errs(factory, prefix):
    p = [prefix] if prefix else []
    lit = TaggedLit("unknown", p, "2020-01-01T00:00:00Z")
    file = factory(lit)
    file.imports.append(Import(Path("/fake_module"), prefix))
    visitor = vis(file)

    visitor.visit_file(file)

    assert lit.res is None

    assert len(visitor.errors) == 1
    e = visitor.errors[0]
    assert type(e) is UnknownTagError
    assert e.tag == "unknown"
    assert e.prefix == prefix
    assert e.source == file.path


@pytest.mark.parametrize(
    "factory",
    [dcsf, dcpf, eff, sec, tx],
)
def test_bad_prefix_errs(factory):
    p = ["a", "b", "c"]
    lit = TaggedLit("unknown", p, "2020-01-01T00:00:00Z")
    file = factory(lit)
    visitor = vis(file)

    visitor.visit_file(file)

    assert lit.res is None

    assert len(visitor.errors) == 1
    e = visitor.errors[0]
    assert type(e) is InvalidPrefixError
    assert e.prefix == "a.b.c"
    assert e.source == file.path


@pytest.mark.parametrize(
    "factory",
    [dcsf, dcpf, eff, sec, tx],
)
def test_bad_value_errs(factory):
    lit = TaggedLit("ssl", [], "bad")
    file = factory(lit)
    file.imports.append(Import(Path("/fake_module"), ""))
    visitor = vis(file)

    visitor.visit_file(file)

    assert lit.res is None

    assert len(visitor.errors) == 1
    e = visitor.errors[0]
    assert type(e) is InvalidTaggedValue
    assert e.source == file.path
    assert e.tag == "ssl"
    assert e.value == "bad"
    assert e.type == "slip_slap"
