from collections.abc import Callable
from pathlib import Path

import pytest

from restrict.compiler.ast import (
    DataComputedField,
    DataConstrainedField,
    Effects,
    EffectsComputedField,
    File,
    Rel,
    Res,
    Rules,
    SecurityConstraintField,
    Self,
    Transition,
    TransitionComputedField,
)
from restrict.compiler.ast import (
    PipedExprList as PEL,
)
from restrict.compiler.types import OptionalDatum, OptionalResource
from restrict.compiler.exceptions.compile import InvalidPathError
from restrict.compiler.types import ModuleDictionary
from restrict.compiler.visitors import ResolveSelfPathTypesVisitor
from restrict.types.builtins import Boolean
from restrict.types.numeric import Decimal_, Integer


def vis(file: File):
    return ResolveSelfPathTypesVisitor(
        {file.path: file}, ModuleDictionary(), {}, file.path
    )


def bases():
    very = Res("", "V", "", False, {}, {}, {}, {}, {}, used=True)
    v = Rel("", "", False, False, None, None, "", res_=very.bridge)

    x = DataConstrainedField("", "", False, False, None, PEL(), res=Integer())
    inner = Res("", "I", "", False, {"x": x}, {"v": v}, {}, {}, {}, used=True)

    w = Rel("", "", False, False, None, None, "", res_=inner.bridge)
    d = DataConstrainedField("", "", False, False, None, PEL(), res=Integer())
    other = Res("", "O", "", False, {"d": d}, {"w": w}, {}, {}, {}, used=True)

    return other, inner, very


def set_file(file, *args):
    for arg in args:
        arg.file = file
    return tuple([file] + list(args))


def dcsf(s):
    pel = PEL([s])
    other, inner, very = bases()

    a = DataConstrainedField("", "", False, False, None, pel)
    b = DataConstrainedField("", "", False, False, None, PEL(), res=Boolean())
    c = Rel("", "", False, False, None, None, "", res_=other.bridge)
    res = Res("", "R", "", False, {"a": a, "b": b}, {"c": c}, {}, {}, {}, used=True)
    file = File(Path("/root.restrict"), [], [res, other])

    return set_file(file, res, other, inner, very)


def dcpf(s):
    pel = PEL([s])
    other, inner, very = bases()

    a = DataComputedField(pel)
    b = DataConstrainedField("", "", False, False, None, PEL(), res=Boolean())
    c = Rel("", "", False, False, None, None, "", res_=other.bridge)
    res = Res("", "R", "", False, {"a": a, "b": b}, {"c": c}, {}, {}, {}, used=True)
    file = File(Path("/root.restrict"), [], [res, other])

    return set_file(file, res, other, inner, very)


def eff(s):
    pel = PEL([s])
    other, inner, very = bases()

    b = DataConstrainedField("", "", False, False, None, PEL(), res=Boolean())
    c = Rel("", "", False, False, None, None, "", res_=other.bridge)

    field = EffectsComputedField(pel)
    effects: Effects = {"delete": {"a": field}}
    res = Res("", "R", "", False, {"b": b}, {"c": c}, effects, {}, {}, used=True)
    file = File(Path("/root.restrict"), [], [res, other])

    return set_file(file, res, other, inner, very)


def sec(s):
    pel = PEL([s])
    other, inner, very = bases()

    b = DataConstrainedField("", "", False, False, None, PEL(), res=Boolean())
    c = Rel("", "", False, False, None, None, "", res_=other.bridge)

    pel = PEL([s])
    field = SecurityConstraintField(pel)
    rules: Rules = {"list": field}
    res = Res("", "R", "", False, {"b": b}, {"c": c}, {}, rules, {}, used=True)
    file = File(Path("/root.restrict"), [], [res, other])

    return set_file(file, res, other, inner, very)


def tx(s):
    pel = PEL([s])
    other, inner, very = bases()

    b = DataConstrainedField("", "", False, False, None, PEL(), res=Boolean())
    c = Rel("", "", False, False, None, None, "", res_=other.bridge)

    field = TransitionComputedField(pel)
    tx = Transition("", "", "", "", {"a": field})
    res = Res("", "R", "", False, {"b": b}, {"c": c}, {}, {}, {"list": tx}, used=True)
    file = File(Path("/root.restrict"), [], [res, other])

    return set_file(file, res, other, inner, very)


@pytest.mark.parametrize("fn", [dcsf, dcpf, eff, sec])
def test_resolves_self_with_no_path_as_resource(
    fn: Callable[[Self], tuple[File, Res, Res, Res, Res]],
):
    s = Self([])
    file, res, *_ = fn(s)
    visitor = vis(file)

    visitor.visit_file(file)

    assert s.path_res == [res.bridge]
    assert s.res == res.bridge


@pytest.mark.parametrize("fn", [dcsf, dcpf, eff, sec])
def test_resolves_self_with_path_to_data_constrained_field(
    fn: Callable[[Self], tuple[File, Res, Res, Res, Res]],
):
    s = Self(["b"])
    file, res, *_ = fn(s)
    visitor = vis(file)

    visitor.visit_file(file)

    assert s.path_res == [res.bridge, Boolean()]
    assert s.res == Boolean()


@pytest.mark.parametrize("fn", [dcsf, dcpf, eff, sec])
def test_resolves_self_with_path_to_rel(
    fn: Callable[[Self], tuple[File, Res, Res, Res, Res]],
):
    s = Self(["c"])
    file, res, other, *_ = fn(s)
    visitor = vis(file)

    visitor.visit_file(file)

    assert s.path_res == [res.bridge, other.bridge]
    assert s.res == other.bridge


@pytest.mark.parametrize("fn", [dcsf, dcpf, eff, sec])
def test_resolves_self_with_path_to_rel_field(
    fn: Callable[[Self], tuple[File, Res, Res, Res, Res]],
):
    s = Self(["c", "d"])
    file, res, other, *_ = fn(s)
    visitor = vis(file)

    visitor.visit_file(file)

    assert s.path_res == [res.bridge, other.bridge, Integer()]
    assert s.res == Integer()


@pytest.mark.parametrize("fn", [dcsf, dcpf, eff, sec])
def test_resolves_self_with_path_to_optional_field(
    fn: Callable[[Self], tuple[File, Res, Res, Res, Res]],
):
    s = Self(["c", "d"])
    file, res, other, *_ = fn(s)
    other.fields["d"].res = OptionalDatum(Integer())  # type: ignore
    visitor = vis(file)

    visitor.visit_file(file)

    assert s.path_res == [res.bridge, other.bridge, OptionalDatum(Integer())]
    assert s.res == OptionalDatum(Integer())


@pytest.mark.parametrize("fn", [dcsf, dcpf, eff, sec])
def test_resolves_self_with_path_through_optional_rel_field_that_ends_with_rel(
    fn: Callable[[Self], tuple[File, Res, Res, Res, Res]],
):
    s = Self(["c", "w", "v"])
    file, res, other, inner, very = fn(s)
    res.dnc["c"].res = OptionalResource(res.dnc["c"].res)
    visitor = vis(file)

    visitor.visit_file(file)

    assert s.path_res == [
        res.bridge,
        OptionalResource(other.bridge),
        OptionalResource(inner.bridge),
        OptionalResource(very.bridge),
    ]
    assert s.res == OptionalResource(very.bridge)


@pytest.mark.parametrize("fn", [dcsf, dcpf, eff, sec])
def test_resolves_self_with_path_through_optional_rel_field_that_ends_with_field(
    fn: Callable[[Self], tuple[File, Res, Res, Res, Res]],
):
    s = Self(["c", "w", "x"])
    file, res, other, inner, *_ = fn(s)
    res.dnc["c"].res = OptionalResource(res.dnc["c"].res)
    visitor = vis(file)

    visitor.visit_file(file)

    assert s.path_res == [
        res.bridge,
        OptionalResource(other.bridge),
        OptionalResource(inner.bridge),
        OptionalDatum(Integer()),
    ]
    assert s.res == OptionalDatum(Integer())


@pytest.mark.parametrize("fn", [dcsf, dcpf, eff, sec])
def test_skips_self_with_res_already_set(
    fn: Callable[[Self], tuple[File, Res, Res, Res, Res]],
):
    s = Self(["c", "d"], path_res=[Decimal_()])
    file, *_ = fn(s)
    visitor = vis(file)

    visitor.visit_file(file)

    assert s.path_res == [Decimal_()]
    assert s.res == Decimal_()


def test_path_going_nowhere_errors():
    s = Self(["c", "d", "e"])
    file, *_ = dcpf(s)
    visitor = vis(file)

    visitor.visit_file(file)

    assert len(visitor.errors) == 1
    e = visitor.errors[0]
    assert isinstance(e, InvalidPathError)
    assert e.reference == "self.c.d.e"
    assert e.source == file.path
