from pathlib import Path

from restrict.compiler.ast import (
    DataComputedField,
    DataConstrainedField,
    EffectsComputedField,
    File,
    Rel,
    Res,
)
from restrict.compiler.ast import PipedExprList as PEL
from restrict.compiler.exceptions.compile import (
    InvalidEffectError,
    UnknownPropertyError,
)
from restrict.compiler.types import ModuleDictionary
from restrict.compiler.visitors import ResolveEffectFieldTypeVisitor
from restrict.types.numeric import Integer


def vis(file: File):
    mods = ModuleDictionary()
    return ResolveEffectFieldTypeVisitor({file.path: file}, mods, {}, file.path)


def test_gets_type_from_data_constrained_field():
    field = DataConstrainedField("", "", False, False, None, PEL([]), Integer())
    effect = EffectsComputedField(PEL())
    res = Res(
        "",
        "R",
        "",
        False,
        {"a": field},
        {},
        {"create": {"a": effect}},
        {},
        {},
        used=True,
    )
    file = File(Path(""), [], [res])
    visitor = vis(file)

    visitor.visit_file(file)

    assert effect.res == Integer()


def test_gets_type_from_rel():
    other = Res("", "O", "", False, {}, {}, {}, {}, {})
    rel = Rel("", "", False, False, None, None, "", other.bridge)
    effect = EffectsComputedField(PEL())
    res = Res(
        "",
        "R",
        "",
        False,
        {},
        {"a": rel},
        {"create": {"a": effect}},
        {},
        {},
        used=True,
    )
    file = File(Path(""), [], [res])
    visitor = vis(file)

    visitor.visit_file(file)

    assert effect.res == other.bridge


def test_errs_when_effect_refers_to_computed_field():
    field = DataComputedField(PEL())
    effect = EffectsComputedField(PEL())
    res = Res(
        "",
        "R",
        "",
        False,
        {"a": field},
        {},
        {"create": {"a": effect}},
        {},
        {},
        used=True,
    )
    file = File(Path(""), [], [res])
    visitor = vis(file)

    visitor.visit_file(file)

    assert effect.res is None
    assert len(visitor.errors) == 1
    e = visitor.errors[0]
    assert type(e) is InvalidEffectError
    assert e.path == "R.effects.create.a"
    assert e.expected_type == "static"
    assert e.received_type == "computed"
    assert e.source == file.path


def test_errs_when_refers_to_unknown_property():
    effect = EffectsComputedField(PEL())
    res = Res("", "R", "", False, {}, {}, {"create": {"a": effect}}, {}, {}, used=True)
    file = File(Path(""), [], [res])
    visitor = vis(file)

    visitor.visit_file(file)

    assert effect.res is None
    assert len(visitor.errors) == 1
    e = visitor.errors[0]
    assert type(e) is UnknownPropertyError
    assert e.prop_name == "a"
    assert e.section == "R.effects.create"
    assert e.ref_res_name == "R.effects.create.a"
    assert e.src_res_name == "R"
    assert e.source == file.path
