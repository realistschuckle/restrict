from collections.abc import Mapping, Sequence
from typing import Any, overload, override

from restrict.compiler.types import (
    Datum,
    Effect,
    Field,
    Function,
    Module,
    Relation,
    Resource,
    Rule,
    RuleName,
)


class SlipSlap(Datum):
    @property
    @override
    def name(self) -> str:
        return "slip_slap"

    @override
    def to_schema(self) -> dict[str, Any]:
        return {}

    @override
    def can_handle_tag(self, tag: str) -> bool:
        return tag == "ssl"


class TreasuryService(Datum):
    @property
    @override
    def name(self) -> str:
        return "treasury_service"

    @override
    def can_handle_tag(self, tag: str) -> bool:
        return tag == "tss"

    @override
    def can_handle_value(self, value: str) -> bool:
        return value != "bad"

    @override
    def to_schema(self) -> dict[str, Any]:
        return {}


class BaseRMR(Resource):
    _name: str

    @property
    def name(self) -> str:
        return self._name

    @property
    @override
    def field_order(self) -> list[str]:
        return []

    @override
    def get_relations(self) -> dict[str, Relation]:
        raise NotImplementedError()

    @override
    def get_fields(self) -> dict[str, Field]:
        raise NotImplementedError()

    @override
    def get_global_names(self) -> Sequence[str]:
        raise NotImplementedError()

    @override
    def get_singular_relations(self) -> dict[str, Relation]:
        raise NotImplementedError()

    @override
    def compiled_name(self) -> str:
        return "!" + self._name

    @override
    def get_effects(self):
        raise NotImplementedError()

    @override
    def resolve_path(self, path: list[str]):
        raise NotImplementedError()

    @override
    def get_rules(self) -> Mapping[RuleName, Rule | dict[str, Rule]]:
        raise NotImplementedError()

    @override
    def to_schema(self) -> dict[str, Any]:
        raise NotImplementedError()


class OverrideMe(BaseRMR):
    _name = "OverrideMe"

    @override
    def get_global_names(self) -> Sequence[str]:
        return ["a"]

    @override
    def compiled_name(self) -> str:
        return "OVERRIDE ME!"

    @override
    def get_effects(self):
        return {
            "create": {"a": Effect(), "b": Effect()},
            "delete": {"x": Effect()},
        }

    @override
    def get_relations(self) -> dict[str, Relation]:
        return {}

    @override
    def get_rules(
        self,
    ) -> Mapping[RuleName, Rule | dict[str, Rule]]:
        return {
            "list": Rule(),
            "create": {
                "a": Rule(),
                "b": Rule(),
            },
            "delete": Rule(),
        }


class ResourceWithRels(BaseRMR):
    _name = "ResourceWithFields"

    @override
    def get_relations(self) -> dict[str, Relation]:
        return {"c": Relation("c", ResourceWithFields())}


class ResourceWithFields(BaseRMR):
    _name = "ResourceWithFields"

    @override
    def get_fields(self) -> dict[str, Field]:
        return {"c": Field()}


class TargetResource(BaseRMR):
    _name = "Target"

    @override
    def get_singular_relations(self) -> dict[str, Relation]:
        return {}


class Thing(BaseRMR):
    _name = "Thing"

    @override
    def get_global_names(self) -> Sequence[str]:
        return ["foobar"]

    @override
    def get_singular_relations(self) -> dict[str, Relation]:
        return {"foobar": Relation("foobar", TargetResource())}


class Boop(Function):
    @overload  # pragma: nocover
    def _calculate_return_type(self, args: list[Datum]) -> Datum | None: ...

    @overload  # pragma: nocover
    def _calculate_return_type(self, args: list[Resource]) -> Resource | None: ...

    @override
    def _calculate_return_type(
        self,
        args: list[Datum] | list[Resource],
    ) -> Datum | Resource | None:
        return super().calculate_return_type(args)  # type: ignore


restrict_module = Module(
    {"treasury_service": TreasuryService(), "slip_slap": SlipSlap()},
    {"boop": Boop()},
    {
        "Thing": Thing(),
        "TargetResource": TargetResource(),
        "OverrideMe": OverrideMe(),
        "ResourceWithRels": ResourceWithRels(),
        "ResourceWithFields": ResourceWithFields(),
    },
)
