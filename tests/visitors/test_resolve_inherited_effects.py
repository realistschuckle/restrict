from pathlib import Path

import pytest

from restrict.compiler.ast import EffectsComputedField, File, Res
from restrict.compiler.ast import PipedExprList as PEL
from restrict.compiler.types import EffName, ModuleDictionary
from restrict.compiler.visitors import ResolveInheritedEffectsVisitor

from .fake_module import OverrideMe


def vis(file: File):
    mods = ModuleDictionary()
    return ResolveInheritedEffectsVisitor({file.path: file}, mods, {}, file.path)


@pytest.mark.parametrize(
    "sec,oth",
    [("create", "modify"), ("modify", "delete"), ("delete", "create")],
)
def test_effects_copied_from_declared_resource(sec: EffName, oth: EffName):
    a = EffectsComputedField(PEL())
    b = EffectsComputedField(PEL())
    base = Res("", "B", "", False, {}, {}, {sec: {"a": a, "b": b}}, {}, {})

    b = EffectsComputedField(PEL())
    c = EffectsComputedField(PEL())
    d = EffectsComputedField(PEL())
    res = Res(
        "",
        "R",
        "",
        True,
        {},
        {},
        {oth: {"d": d}, sec: {"b": b, "c": c}},
        {},
        {},
        base.bridge,
    )
    file = File(Path(""), [], [res])
    visitor = vis(file)

    visitor.visit_file(file)

    assert res.effects[sec]["a"] == a
    assert res.effects[sec]["b"] == b
    assert res.effects[sec]["c"] == c
    assert res.effects[oth]["d"] == d
    assert res.effects_updated


def test_effects_copied_from_compiled_resource():
    sec = "create"
    oth = "modify"
    b = EffectsComputedField(PEL())
    c = EffectsComputedField(PEL())
    d = EffectsComputedField(PEL())
    res = Res(
        "",
        "R",
        "",
        True,
        {},
        {},
        {oth: {"d": d}, sec: {"b": b, "c": c}},
        {},
        {},
        OverrideMe(),
    )
    file = File(Path(""), [], [res])
    visitor = vis(file)

    visitor.visit_file(file)

    assert res.effects[sec]["a"] is not None
    assert res.effects[sec]["b"] == b
    assert res.effects[sec]["c"] == c
    assert res.effects[oth]["d"] == d
    assert res.effects["delete"]["x"] is not None
    assert res.effects_updated
