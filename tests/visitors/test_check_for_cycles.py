from pathlib import Path

from restrict.compiler.ast import (
    DataComputedField,
    DataConstrainedField,
    File,
    Res,
    Self,
)
from restrict.compiler.ast import (
    PipedExprList as PEL,
)
from restrict.compiler.exceptions.compile import ReferenceCycleError
from restrict.compiler.types import ModuleDictionary
from restrict.compiler.visitors import CheckForCyclesVisitor


def vis(root: File):
    mods = ModuleDictionary()
    return CheckForCyclesVisitor({root.path: root}, mods, {}, root.path)


def test_cannot_refer_to_self_prop_in_data_constrained_field():
    path = ["a"]
    path_res = []
    selfo = Self(path, path_res)
    a = DataConstrainedField("", "", False, False, None, PEL([selfo]))
    res = Res("", "", "", False, {"a": a}, {}, {}, {}, {})
    file = File(Path(""), [], [res])
    visitor = vis(file)

    visitor.visit_file(file)

    assert len(visitor.errors) == 1
    e = visitor.errors[0]
    assert type(e) is ReferenceCycleError
    assert e.source == file.path
    assert e.resource == ""
    assert e.path == "a"


def test_cannot_refer_to_self_prop_in_data_constrained_field_referring_to_one_another():
    bpath = ["a"]
    bpath_res = []
    bself = Self(bpath, bpath_res)
    b = DataConstrainedField("", "", False, False, None, PEL([bself]))

    apath = ["b"]
    apath_res = []
    aself = Self(apath, apath_res)
    a = DataConstrainedField("", "", False, False, None, PEL([aself]))
    res = Res("", "", "", False, {"a": a, "b": b}, {}, {}, {}, {})
    file = File(Path(""), [], [res])
    visitor = vis(file)

    visitor.visit_file(file)

    assert len(visitor.errors) == 1
    e = visitor.errors[0]
    assert type(e) is ReferenceCycleError
    assert e.source == file.path
    assert e.resource == ""
    assert e.path == "a->b"


def test_cannot_refer_to_self_prop_in_data_computed_field():
    path = ["a"]
    path_res = []
    selfo = Self(path, path_res)
    a = DataComputedField(PEL([selfo]))
    res = Res("", "", "", False, {"a": a}, {}, {}, {}, {})
    file = File(Path(""), [], [res])
    visitor = vis(file)

    visitor.visit_file(file)

    assert len(visitor.errors) == 1
    e = visitor.errors[0]
    assert type(e) is ReferenceCycleError
    assert e.source == file.path
    assert e.resource == ""
    assert e.path == "a"


def test_cannot_refer_to_self_prop_in_data_computed_field_through_other_fields():
    bpathres = []
    bself = Self(["a"], bpathres)
    b = DataComputedField(PEL([bself]))

    apathres = []
    aself = Self(["b"], apathres)
    a = DataComputedField(PEL([aself]))
    res = Res("", "", "", False, {"a": a, "b": b}, {}, {}, {}, {})
    file = File(Path(""), [], [res])
    visitor = vis(file)

    visitor.visit_file(file)

    assert len(visitor.errors) == 1
    e = visitor.errors[0]
    assert type(e) is ReferenceCycleError
    assert e.source == file.path
    assert e.resource == ""
    assert e.path == "a->b"


def test_cannot_refer_to_self_prop_in_data_computed_field_through_many_other_fields():
    cself = Self(["a"])
    c = DataComputedField(PEL([cself]))

    bself = Self(["c"])
    b = DataComputedField(PEL([bself]))

    aself = Self(["b"])
    a = DataComputedField(PEL([aself]))

    res = Res("", "", "", False, {"b": b, "a": a, "c": c}, {}, {}, {}, {})
    file = File(Path(""), [], [res])
    visitor = vis(file)

    visitor.visit_file(file)

    assert len(visitor.errors) == 1
    e = visitor.errors[0]
    assert type(e) is ReferenceCycleError
    assert e.source == file.path
    assert e.resource == ""
    assert e.path == "a->b->c"


def test_orders_errors_by_length_and_entries():
    dself = Self(["d"])
    d = DataComputedField(PEL([dself]))

    cself = Self(["a"])
    c = DataComputedField(PEL([cself]))

    bself = Self(["c"])
    b = DataComputedField(PEL([bself]))

    aself = Self(["b"])
    a = DataComputedField(PEL([aself]))

    res = Res("", "", "", False, {"b": b, "a": a, "c": c, "d": d}, {}, {}, {}, {})
    file = File(Path(""), [], [res])
    visitor = vis(file)

    visitor.visit_file(file)

    assert len(visitor.errors) == 2
    e = visitor.errors[0]
    assert type(e) is ReferenceCycleError
    assert e.source == file.path
    assert e.resource == ""
    assert e.path == "d"

    e = visitor.errors[1]
    assert type(e) is ReferenceCycleError
    assert e.source == file.path
    assert e.resource == ""
    assert e.path == "a->b->c"
