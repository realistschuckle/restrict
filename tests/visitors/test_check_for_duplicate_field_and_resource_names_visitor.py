from pathlib import Path

from restrict.compiler.ast import (
    Create,
    DataComputedField,
    DataConstrainedField,
    EffectsComputedField,
    File,
    PipedExprList,
    Rel,
    Res,
    SecurityConstraintField,
)
from restrict.compiler.exceptions.compile import (
    DuplicatePropertyError,
    DuplicateResourceError,
)
from restrict.compiler.types import ModuleDictionary
from restrict.compiler.visitors import (
    CheckForDuplicateFieldAndResourceNamesVisitor,
)


def _(*args):
    return PipedExprList(args)


def vis(asts={}):
    return CheckForDuplicateFieldAndResourceNamesVisitor(
        asts, ModuleDictionary(), {}, Path("")
    )


def test_duplicate_names_between_data_and_dnc_causes_error():
    ast = File(
        Path("/root.restrict"),
        [],
        [
            Res(
                "thing",
                "Thing",
                "",
                False,
                {
                    "a": DataComputedField(_()),
                    "b": DataConstrainedField("", "", False, False, None, _()),
                },
                {
                    "b": Rel("Thing", "", False, False, None, None, ""),
                    "a": Rel("Thing", "", False, False, None, None, ""),
                },
                {},
                {},
                {},
            ),
            Res("role", "Thing", "", False, {}, {}, {}, {}, {}),
        ],
    )
    visitor = vis()

    visitor.visit_file(ast)

    assert len(visitor.errors) == 3

    for name, error in zip(["b", "a"], visitor.errors):
        assert isinstance(error, DuplicatePropertyError)
        assert error.name == name
        assert error.source == ast.path

    for name, error in zip(["Thing"], visitor.errors[2:]):
        assert isinstance(error, DuplicateResourceError)
        assert error.name == name
        assert error.source == ast.path


def test_duplicate_names_ignores_non_data_dnc_entries():
    ast = File(
        Path("/root.restrict"),
        [],
        [
            Res(
                "thing",
                "Thing",
                "",
                False,
                {
                    "a": DataComputedField(_()),
                    "b": DataConstrainedField("", "", False, False, None, _()),
                },
                {
                    "c": Rel("Thing", "", False, False, None, None, ""),
                    "d": Rel("Thing", "", False, False, None, None, ""),
                },
                {
                    "create": {
                        "a": EffectsComputedField(
                            _(
                                Create(
                                    "",
                                    "",
                                    {
                                        "a": EffectsComputedField(_()),
                                        "b": EffectsComputedField(_()),
                                        "c": EffectsComputedField(_()),
                                        "d": EffectsComputedField(_()),
                                    },
                                )
                            )
                        ),
                        "b": EffectsComputedField(_()),
                        "c": EffectsComputedField(_()),
                        "d": EffectsComputedField(_()),
                    }
                },
                {
                    "list": {
                        "a": SecurityConstraintField(_()),
                        "b": SecurityConstraintField(_()),
                        "c": SecurityConstraintField(_()),
                        "d": SecurityConstraintField(_()),
                    }
                },
                {},
            ),
        ],
    )
    visitor = vis()

    visitor.visit_file(ast)

    assert len(visitor.errors) == 0
