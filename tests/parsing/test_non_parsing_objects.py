from pathlib import Path
from unittest.mock import Mock

import pytest

from restrict.compiler.ast import (
    File,
    Func,
    Import,
    Lit,
    PipedExprList,
    Rel,
    Res,
    ResToResourceBridge,
)
from restrict.compiler.ast import (
    PipedExprList as PEL,
)
from restrict.compiler.exceptions.compile import InvalidExpressionError
from restrict.compiler.parsers import (
    RULE,
    PlyRestrictParser,
    ShiftedYaccProduction,
)
from restrict.compiler.ply.yacc import YaccProduction, YaccSymbol
from restrict.compiler.types import ResourceCollection


def test_bridge_returns_field_sort():
    resource = Res("", "", "", False, {}, {}, {}, {}, {}, field_sort=["a", "b"])
    bridge = ResToResourceBridge(resource)

    assert bridge.field_order == ["a", "b"]


def test_get_used_bridge():
    resource = Res("", "", "", False, {}, {}, {}, {}, {})
    bridge = ResToResourceBridge(resource)

    assert bridge.used is False

    resource.used = True
    assert bridge.used is True


def test_get_schema_bridge():
    resource = Res("", "", "", False, {}, {}, {}, {}, {})
    bridge = ResToResourceBridge(resource)

    with pytest.raises(NotImplementedError):
        bridge.to_schema()


def test_file_resolved_imports():
    expected = [
        (Path("/this/is/absolute"), True),
        (Path("this/is/relative.restrict"), False),
    ]
    file = File(
        Path("/this/is/root.restrict"),
        [
            Import(Path("/this/is/absolute"), ""),
            Import(Path("this/is/relative"), ""),
        ],
        [],
    )

    assert list(file.resolved_imports()) == expected


def test_rule_decorator_requires_non_none_first_arg():
    with pytest.raises(SyntaxError):
        RULE(None)


def test_rule_decorator_takes_lines():
    def p_foo():
        pass

    f = RULE(["one", "two"])(p_foo)

    assert f.__doc__ == "foo : one\n    | two"


def test_shifted_yacc_production():
    syms = [YaccSymbol(x, x) for x in [None, "hello", "barry"]]
    yp = YaccProduction(syms)
    syp = ShiftedYaccProduction(yp)

    assert syp[-1] is None
    assert syp[0] == "hello"
    assert syp[1] == "barry"
    assert [x for x in syp[1:3]] == [x.value for x in syms[1:3]]
    assert len(syp) == 2


def test_unbuilt_parser_returns_no_tokens():
    assert PlyRestrictParser().tokens == []


def test_unbuilt_parser_raises_name_error_when_parsing_happens():
    content = "  \t\t\n\n\n\t\t\n  "
    resolver = Mock()
    resolver.resolve_rel = Mock(return_value=content)
    file_path = Path("some/place.restrict")

    with pytest.raises(NameError):
        PlyRestrictParser().parse(resolver, file_path)


def test_parser_returns_empty_file_for_whitespace_string():
    content = "  \t\t\n\n\n\t\t\n  "
    file_path = Path("some/place.restrict")

    file = PlyRestrictParser().build().parse(content, file_path)

    assert file is not None
    if file is not None:
        assert file.path == file_path
        assert len(file.imports) == 0
        assert len(file.resources) == 0


def test_piped_expr_list_treeify_returns_true_for_empty_list():
    lst = PipedExprList()

    assert lst.treeify() == Lit(True)


def test_piped_expr_list_treeify_returns_expr_for_one_item_list():
    lst = PipedExprList()
    lst += [Lit(10)]

    assert lst.treeify() == Lit(10)


def test_piped_expr_list_treeify_returns_composed_function_2():
    lst = PipedExprList()
    lst += [Lit(10)]
    lst += [Func("", [], [], [PEL([Lit(12)])])]

    assert lst.treeify() == Func("", [], [], [PEL([Lit(10)]), PEL([Lit(12)])])


def test_piped_expr_list_fails_for_non_function_after_first_entry():
    lst = PipedExprList()
    lst += [Lit(10)]
    lst += [Lit(12)]

    with pytest.raises(InvalidExpressionError):
        lst.treeify()


def test_piped_expr_list_treeify_returns_composed_function_4():
    level_1: Func[Lit] = Func("1", [], [], [PEL([Lit(0)]), PEL([Lit(1)])])
    level_2: Func[Lit | Func[Lit]] = Func("2", [], [], [PEL([level_1]), PEL([Lit(2)])])
    expected: Func[Lit | Func[Lit | Func[Lit]]] = Func(
        "3", [], [], [PEL([level_2]), PEL([Lit(3)])]
    )

    lst = PipedExprList()
    lst += [Lit(0)]
    lst += [Func("1", [], [], [PEL([Lit(1)])])]
    lst += [Func("2", [], [], [PEL([Lit(2)])])]
    lst += [Func("3", [], [], [PEL([Lit(3)])])]

    assert lst.treeify() == expected


def test_piped_expr_list_str_method():
    lst = PipedExprList()

    assert str(lst) == "PipedExprList([])"


def test_bridge_is_not_equal_to_non_bridge():
    res = Res("", "", "", False, {}, {}, {}, {}, {})

    assert res.bridge != "Hi!"


def test_get_fields_caches():
    res = Res("", "", "", False, {}, {}, {}, {}, {})
    res.fields_updated = True

    assert res.bridge.get_fields() == {}


def test_get_rels_caches():
    res = Res("", "", "", False, {}, {}, {}, {}, {})
    res.dnc_updated = True

    assert res.bridge.get_relations() == {}


def test_repr_returns_compiled_name():
    res = Res("", "R", "", False, {}, {}, {}, {}, {})
    file = File(Path("/root.restrict"), [], [res])
    res.file = file

    assert repr(res.bridge) == "Bridge</root.restrict/R>"


def test_bridge_with_no_override_returns_no_globals():
    res = Res("", "R", "", False, {}, {}, {}, {}, {})

    assert res.bridge.get_global_names() == []


def test_cloning_rel_includes_inherited_properties():
    res = Res("", "R", "", False, {}, {}, {}, {}, {})
    rel = Rel("", "", False, False, None, None, "", res.bridge, "billy")

    clone = rel.clone()

    assert clone.res == res.bridge
    assert clone.name == "billy"


def test_resource_resolve_path_raise_nie_for_dnc_with_no_res():
    rel = Rel("", "", False, False, None, None, "")
    res = Res("", "R", "", False, {}, {"rel": rel}, {}, {}, {})

    with pytest.raises(NotImplementedError):
        res.resolve_path(["rel"])


def test_resource_resolve_path_raise_nie_for_none_type():
    rel = Rel("", "", False, False, None, None, "")
    res = Res("", "R", "", False, {}, {"rel": rel}, {}, {}, {})

    with pytest.raises(NotImplementedError):
        res.resolve_path(["rel", "rel"])


def test_resource_resolve_path_raise_nie_for_collection_types():
    other = Res("", "", "", False, {}, {}, {}, {}, {})
    coll = ResourceCollection(list, other.bridge)
    rel = Rel("", "", False, False, None, None, "", coll)
    res = Res("", "R", "", False, {}, {"rel": rel}, {}, {}, {})

    with pytest.raises(NotImplementedError):
        res.resolve_path(["rel", "rel"])
