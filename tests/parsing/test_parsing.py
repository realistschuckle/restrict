import os
from decimal import Decimal
from pathlib import Path

import pytest

from restrict.compiler.ast import (
    Create,
    DataComputedField,
    DataConstrainedField,
    EffectsComputedField,
    Func,
    FuncParam,
    Lit,
    Modify,
    PipedExprList,
    Ref,
    Rel,
    Res,
    SecurityConstraintField,
    Self,
    Selves,
    TaggedLit,
    Transition,
    TransitionComputedField,
    Value,
)
from restrict.compiler.exceptions.compile import ParsingError
from restrict.compiler.parsers import PlyRestrictParser


def _(*args) -> PipedExprList:
    return PipedExprList(args)


@pytest.fixture(scope="module")
def parser():
    return PlyRestrictParser().build(debug=True)


@pytest.fixture(scope="module")
def file_path():
    return os.getcwd() / Path("test.restrict")


def test_show_me_a_parsing_error(parser, file_path):
    content = "use hello"
    with pytest.raises(SyntaxError):
        parser.parse(content, file_path)

    assert len(parser.errors) == 1
    assert isinstance(parser.errors[0], ParsingError)
    assert parser.errors[0].token is None


def test_parser_output_has_no_conflicts():
    PlyRestrictParser().build(debug=True, out_file="check_conflicts.out")
    with open("check_conflicts.out", "r") as f:
        assert "conflict" not in f.read()


def test_use_statement(parser, file_path):
    content = """
        use </thing>
        use <rel>
        """
    file = parser.parse(content, file_path)

    assert file.path == file_path
    assert len(file.resources) == 0
    assert len(file.imports) == 2

    assert file.imports[0].path == Path("/thing")
    assert file.imports[1].path == Path("rel")

    assert file.imports[0].prefix == ""
    assert file.imports[1].prefix == ""


def test_refer_to_statement(parser, file_path):
    content = """
        refer to </thing> as thing
        refer to <rel> as r
        """
    file = parser.parse(content, file_path)

    assert file.path == file_path
    assert len(file.resources) == 0
    assert len(file.imports) == 2

    assert file.imports[0].path == Path("/thing")
    assert file.imports[1].path == Path("rel")

    assert file.imports[0].prefix == "thing"
    assert file.imports[1].prefix == "r"


def test_resource(parser, file_path):
    content = """
        role Role { data { a: int; } }
        override thing Thing { data{ b: int; } }
        override party st.Part { data { c: int; } }
    """
    file = parser.parse(content, file_path)

    assert file.path == file_path
    assert len(file.imports) == 0
    assert len(file.resources) == 3

    for r, o in zip(file.resources, [False, True, True]):
        assert r.override == o

    for r, t in zip(file.resources, ["role", "thing", "party"]):
        assert r.type == t

    for r, n in zip(file.resources, ["Role", "Thing", "Part"]):
        assert r.name == n

    for r in file.resources:
        assert r.source == file_path


def test_resource_raise_value_error_when_no_file():
    r = Res("", "", "", False, {}, {}, {}, {}, {})

    with pytest.raises(ValueError):
        r.source


def test_data_section(parser, file_path):
    expected = {
        "a": DataConstrainedField("int", "x", True, False, set, _(Lit(True))),
        "b": DataConstrainedField(
            "int",
            "x",
            True,
            False,
            list,
            _(
                Value(),
                Func("gte", ["x"], [], [_(Lit(0))]),
                Func("lte", [], [], [_(Lit(Decimal("10.0")))]),
            ),
        ),
        "c": DataComputedField(
            _(Func("add", ["y"], [], [_(Ref(["a"])), _(Ref(["b"]))]))
        ),
        "d_1d2_e": DataComputedField(
            _(
                Selves(),
                Func(
                    "filter",
                    [],
                    [FuncParam("x")],
                    [
                        _(Ref(["x", "a"]), Func("gt", [], [], [_(Lit(5))])),
                    ],
                ),
                Func("len", [], [], []),
            )
        ),
        "f": DataComputedField(_(Func("add", [], [], [_(Lit(1)), _(Self(["c"]))]))),
        "g": DataComputedField(_(TaggedLit("foo", [], "gogo"))),
        "h": DataConstrainedField(
            "text", "", False, True, None, PipedExprList([Lit(True)])
        ),
    }
    content = """
        thing Thing {
            data {
                a: optional set x.int;
                b: optional list x.int: value |> x.gte(0) |> lte(10.0);
                c = y.add(a, b);
                d_1d2_e = selves |> filter[x](x.a |> gt(5)) |> len();
                f = add(1, self.c);
                g = foo`gogo`;
                h: unique text;
            }
        }
    """
    file = parser.parse(content, file_path)

    assert file.path == file_path
    assert len(file.imports) == 0
    assert len(file.resources) == 1

    assert file.resources[0].fields == expected


def test_dnc_section(parser, file_path):
    expected = {
        "a": Rel("Thing1", "", True, False, list, (0, "*"), ""),
        "b": Rel("Thing2", "x", True, False, set, (1, 10), ""),
        "c": Rel("Thing3", "", False, False, list, (1, "*"), "next"),
        "d": Rel("Thing4", "yolo", False, False, None, None, "previous"),
        "e": Rel("Thing5", "", False, True, None, None, "root"),
        "f": Rel("Thing6", "", True, False, None, None, "details"),
    }
    content = """
        thing Thing {
            dnc {
                a: optional list Thing1;
                b[1, 10]: optional set x.Thing2;
                <next> c[1, *]: list Thing3;
                <previous> d: yolo.Thing4;
                <root> e: unique Thing5;
                <details> f: optional Thing6;
            }
        }
    """
    file = parser.parse(content, file_path)

    assert file.path == file_path
    assert len(file.imports) == 0
    assert len(file.resources) == 1

    assert file.resources[0].dnc == expected


def test_effects_section(parser, file_path):
    expected = {
        "create": {
            "boop": EffectsComputedField(_(Selves(), Func("len", [], [], []))),
            "active": EffectsComputedField(_(Lit(True))),
            "count": EffectsComputedField(_(Lit(1))),
            "items": EffectsComputedField(
                _(
                    Ref(["products"]),
                    Func(
                        "map",
                        [],
                        [FuncParam("x")],
                        [
                            _(
                                Create(
                                    "Item",
                                    "",
                                    {
                                        "bag": EffectsComputedField(_(Self([]))),
                                        "assigned": EffectsComputedField(
                                            _(
                                                Create(
                                                    "ItemRelation",
                                                    "ir",
                                                    {
                                                        "item": EffectsComputedField(
                                                            _(Ref(["x"]))
                                                        ),
                                                        "count": EffectsComputedField(
                                                            _(Lit(0))
                                                        ),
                                                    },
                                                )
                                            )
                                        ),
                                    },
                                )
                            )
                        ],
                    ),
                )
            ),
            "borp": EffectsComputedField(_(Create("Borp", "", {}))),
        },
        "modify": {
            "count": EffectsComputedField(
                _(
                    Func(
                        "add",
                        [],
                        [],
                        [
                            _(Lit(1)),
                            _(Ref(["count"]), Func("sub", [], [], [_(Lit(1))])),
                        ],
                    )
                )
            ),
            "items": EffectsComputedField(
                _(
                    Ref(["items"]),
                    Func(
                        "map",
                        [],
                        [FuncParam("x")],
                        [
                            _(
                                Modify(
                                    Self(["x"]),
                                    {
                                        "count": EffectsComputedField(
                                            _(
                                                Ref(["x", "count"]),
                                                Func("add", [], [], [_(Lit(1))]),
                                            )
                                        )
                                    },
                                )
                            )
                        ],
                    ),
                )
            ),
            "borp": EffectsComputedField(_(Modify(Self(["borp"]), {}))),
            "boop": EffectsComputedField(_(TaggedLit("a", ["x"], "bcd"))),
        },
        "delete": {
            "active": EffectsComputedField(_(Lit(False))),
            "username": EffectsComputedField(_(Func("random_string", [], [], []))),
            "items": EffectsComputedField(_()),
            "when": EffectsComputedField(_(TaggedLit("dt", [], "1990-01-01"))),
        },
    }
    content = """
        thing Thing {
            effects {
                create {
                    boop = selves |> len();
                    active = true;
                    count = 1;
                    items = products 
                            |> map[x](create Item { 
                                bag = self;
                                assigned = create ir.ItemRelation { item = x; count = 0; }; 
                            });
                    borp = create Borp;
                }
                modify {
                    count = add(1, count |> sub(1));
                    items = items 
                            |> map[x](modify x { count = x.count |> add(1); });
                    borp = modify borp;
                    boop = x.a`bcd`;
                }
                delete {
                    active = false;
                    username = random_string();
                    items = delete;
                    when = dt`1990-01-01`;
                }
            }
        }
    """
    file = parser.parse(content, file_path)

    assert file.path == file_path
    assert len(file.imports) == 0
    assert len(file.resources) == 1

    assert file.resources[0].effects == expected


def test_security_section(parser, file_path):
    expected = {
        "list": {
            "id": SecurityConstraintField(_(Lit(True))),
            "name": SecurityConstraintField(_(Lit(True))),
            "when": SecurityConstraintField(
                _(
                    Ref(["when"]),
                    Func("gt", [], [], [_(TaggedLit("ts", [], "2025-01-01"))]),
                )
            ),
        },
        "details": SecurityConstraintField(
            func=_(Ref(["actor"]), Func("eq", [], [], [_(Self([]))])),
        ),
        "create": SecurityConstraintField(
            func=_(Ref(["actor", "admin"]), Func("exists", [], [], [])),
        ),
        "modify": SecurityConstraintField(
            func=_(Self(["actor"]), Func("eq", [], [], [_(Ref(["actor"]))])),
        ),
        "delete": SecurityConstraintField(_(Lit(False))),
    }
    content = """
        thing Thing {
            security {
                list {
                    id: true;
                    name: true;
                    when: when |> gt(ts`2025-01-01`);
                }
                details: actor |> eq(self);
                create: actor.admin |> exists();
                modify: self.actor |> eq(actor);
                delete: false;
            }
        }
    """
    file = parser.parse(content, file_path)

    assert file.path == file_path
    assert len(file.imports) == 0
    assert len(file.resources) == 1

    assert file.resources[0].rules == expected


def test_workflow_section(parser, file_path):
    expected = {
        "list": Transition("", "", "entrypoint", "", {}),
        "details": Transition(
            "Role",
            "",
            "",
            "create",
            {
                "thing": TransitionComputedField(_(Self([]))),
                "coll": TransitionComputedField(_(Selves())),
                "i": TransitionComputedField(
                    _(Lit(3), Func("add", [], [], [_(Ref(["x"]))]))
                ),
                "expires": TransitionComputedField(_(TaggedLit("ti", [], "P1D"))),
            },
        ),
        "create": Transition("Workflow", "x", "alias", "create", {}),
    }
    content = """
        thing Thing1 {
            workflow {
                <entrypoint> list;
                details -> Role#create { 
                    thing = self;
                    coll = selves;
                    i = 3 |> add(x);
                    expires = ti`P1D`;
                }
                <alias> create -> x.Workflow#create;
            }
        }
    """
    file = parser.parse(content, file_path)

    assert file.path == file_path
    assert len(file.imports) == 0
    assert len(file.resources) == 1

    assert file.resources[0].workflow == expected


def test_all_sections(parser, file_path):
    content = """
        use </thing>
        use <rel>
        refer to </thing> as thing
        refer to <rel> as r
        thing Thing {
            data {
                a: int;
                b: int: value |> x.gte(0) |> lte(10.0);
                c = y.add(a, b);
                d_1d2_e = selves |> filter[x](x.a |> gt(5)) |> len();
            }
            dnc {
                a: Thing1;
                b: x.Thing2;
                <next> c: Thing3;
                <previous> d: yolo.Thing4;
                <root> e: Thing5;
                <details> f: Thing6;
            }
            effects {
                create {
                    boop = selves |> len();
                    active = true;
                    count = 1;
                    items = products 
                            |> map[x](create Item { 
                                bag = self;
                                assigned = create ir.ItemRelation { item = x; count = 0; }; 
                            });
                }
                modify {
                    count = count |> add(1);
                    items = items 
                            |> map[x](modify x { count = x.count |> add(1); });
                }
                delete {
                    active = false;
                    username = random_string();
                    items = delete;
                }
            }
            security {
                list {
                    id: true;
                    name: true;
                }
                details: actor |> eq(self);
                create: actor.admin |> exists();
                modify: self.actor |> eq(actor);
                delete: false;
            }
            workflow {
                <entrypoint> list;
                details -> Role#create { thing = self; }
                <alias> create -> x.Workflow#create;
            }
        }
    """

    file = parser.parse(content, file_path)

    assert parser.errors == []
    assert len(file.imports) == 4
    assert len(file.resources) == 1
    assert len(file.resources[0].fields) == 4
    assert len(file.resources[0].dnc) == 6
    assert len(file.resources[0].effects["create"]) == 4
    assert len(file.resources[0].effects["modify"]) == 2
    assert len(file.resources[0].effects["delete"]) == 3
    assert len(file.resources[0].rules) == 5
    assert len(file.resources[0].workflow) == 3


def test_duplicate_data_names_causes_error(parser, file_path):
    content = """
        thing Thing {
            data {
                a: int;
                a: text;
                b: int;
                c: text;
                b: text;
                c = true;
            }
        }
    """

    parser.parse(content, file_path)

    assert len(parser.errors) == 3

    assert parser.errors[0].name == "a"
    assert parser.errors[0].source == file_path
    assert parser.errors[1].name == "b"
    assert parser.errors[1].source == file_path
    assert parser.errors[2].name == "c"
    assert parser.errors[2].source == file_path


def test_duplicate_dnc_names_causes_error(parser, file_path):
    content = """
        thing Thing {
            dnc {
                a: Thing1;
                a: Thing2;
                b: Thing1;
                c: Thing1;
                b: Thing2;
            }
        }
    """

    parser.parse(content, file_path)

    assert len(parser.errors) == 2

    assert parser.errors[0].name == "a"
    assert parser.errors[0].source == file_path
    assert parser.errors[1].name == "b"
    assert parser.errors[1].source == file_path


def test_data_constrained_field_constraint_terms(parser, file_path):
    content = """
        thing Thing {
            data {
                a: integer: selves |> len() |> lt(10);
                b: integer: value |> gt(ti`P1D`);
                c: integer: self.a |> gt(b);
            }
        }
    """

    parser.parse(content, file_path)

    assert len(parser.errors) == 0
