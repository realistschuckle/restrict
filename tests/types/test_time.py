from datetime import datetime, timezone, timedelta
from decimal import Decimal
from itertools import product

import pytest

from restrict.compiler.exceptions.compile import InvalidFuncCallError
from restrict.compiler.types import OptionalDatum
from restrict.types.builtins import Boolean
from restrict.types.numeric import Decimal_, Integer
from restrict.types.text import Hash, Text
from restrict.types.time import Interval, Now, TimeAdd, Timestamp, restrict_module


def test_restrict_module():
    assert len(restrict_module.types) == 2
    assert len(restrict_module.functions) == 2
    assert len(restrict_module.resources) == 0

    assert restrict_module.types["timestamp"] == Timestamp()
    assert restrict_module.types["interval"] == Interval()

    assert type(restrict_module.functions["now"]) is Now
    assert type(restrict_module.functions["time_add"]) is TimeAdd


@pytest.mark.parametrize(
    "a,b,expected,opt",
    [
        a + [b]
        for a, b in product(
            [
                [Interval(), Timestamp(), Timestamp()],
                [Timestamp(), Interval(), Timestamp()],
                [Interval(), Interval(), Interval()],
            ],
            [lambda x: x, OptionalDatum],
        )
    ],
)
def test_time_add_calculates_nice_return_types(a, b, expected, opt):
    op = TimeAdd()
    assert op.calculate_return_type([a, b]) == expected
    assert op.calculate_return_type([opt(a), b]) == opt(expected)
    assert op.calculate_return_type([a, opt(b)]) == opt(expected)
    assert op.calculate_return_type([opt(a), opt(b)]) == opt(expected)


@pytest.mark.parametrize(
    "a",
    [Boolean(), Text(), Hash(), Integer(), Decimal_()],
)
def test_biop_requires_two_numeric_types(a):
    op = TimeAdd()
    b = Timestamp()
    with pytest.raises(InvalidFuncCallError):
        op.calculate_return_type([])

    with pytest.raises(InvalidFuncCallError):
        op.calculate_return_type([a])

    with pytest.raises(ValueError):
        op.calculate_return_type([a, b])

    with pytest.raises(InvalidFuncCallError):
        op.calculate_return_type([a, a, a])


def test_now_returns_timestamp():
    assert type(Now().calculate_return_type([])) is Timestamp


def test_now_raises_error_for_arguments():
    with pytest.raises(InvalidFuncCallError):
        Now().calculate_return_type([Text()])


@pytest.mark.parametrize("tag,expected", [("ti", True), ("x", False)])
def test_interval_can_handle_tag(tag: str, expected: bool):
    interval = Interval()

    assert interval.can_handle_tag(tag) == expected


@pytest.mark.parametrize("value,expected", [("P1D", True), ("P1H", False)])
def test_interval_can_handle_parseable_value(value: str, expected: bool):
    interval = Interval()

    assert interval.can_handle_value(value) == expected


@pytest.mark.parametrize("tag,expected", [("ts", True), ("x", False)])
def test_timestamp_can_handle_tag(tag: str, expected: bool):
    interval = Timestamp()

    assert interval.can_handle_tag(tag) == expected


@pytest.mark.parametrize(
    "value,expected",
    [
        ("2020-10-01T00:00:00Z", True),
        ("2020-10-01T00:00:00", False),
        ("Hi!", False),
    ],
)
def test_timestamp_can_handle_parseable_value(value: str, expected: bool):
    interval = Timestamp()

    assert interval.can_handle_value(value) == expected


@pytest.mark.parametrize(
    "value,expected",
    [
        ("2020-01-01T00:00:00Z", datetime(2020, 1, 1, tzinfo=timezone.utc)),
        (
            "2020-01-01T00:00:00-06:00",
            datetime(2020, 1, 1, tzinfo=timezone(timedelta(hours=-6))),
        ),
    ],
)
def test_timestamp_can_parse_strings_and_lets_through_ints(value, expected):
    assert Timestamp().parse(value) == expected


@pytest.mark.parametrize("value", [True, {}, [], Decimal("3.0")])
def test_timestamp_cannot_parse_other_values(value):
    with pytest.raises(ValueError):
        Timestamp().parse(value)


@pytest.mark.parametrize(
    "value,expected",
    [("P1D", timedelta(days=1)), ("PT1H", timedelta(hours=1))],
)
def test_interval_can_parse_strings_and_lets_through_decimals_and_converts_ints(
    value, expected
):
    assert Interval().parse(value) == expected


@pytest.mark.parametrize("value", [True, {}, [], "a"])
def test_interval_cannot_parse_other_values(value):
    with pytest.raises(ValueError):
        print(Interval().parse(value))


@pytest.mark.parametrize(
    "value,expected",
    [
        (datetime(2020, 10, 1, tzinfo=timezone.utc), "2020-10-01T00:00:00+00:00"),
        (
            datetime(2020, 10, 1, 2, 3, 4, tzinfo=timezone(timedelta(hours=-6))),
            "2020-10-01T02:03:04-06:00",
        ),
    ],
)
def test_timestamp_can_json_date_times(value: datetime, expected: str):
    timestamp = Timestamp()

    assert timestamp.to_json(value) == expected


@pytest.mark.parametrize(
    "value,expected",
    [
        (timedelta(days=1), "P1D"),
        (timedelta(weeks=1), "P1W"),
        (timedelta(hours=1), "PT1H"),
        (timedelta(minutes=1), "PT1M"),
        (timedelta(seconds=1), "PT1S"),
        (timedelta(weeks=1, days=2, hours=3, minutes=4, seconds=5), "P1W2DT3H4M5S"),
    ],
)
def test_interval_can_json_date_times(value: timedelta, expected: str):
    interval = Interval()

    assert interval.to_json(value) == expected


@pytest.mark.parametrize(
    "t,f",
    [
        (Timestamp(), {"format": "date-time"}),
        (Interval(), {"format": "interval"}),
    ],
)
def test_types_to_schema(t, f):
    assert t.to_schema() == {"type": "string"} | f
