from decimal import Decimal
from itertools import product

import pytest

from restrict.types.builtins import Boolean
from restrict.compiler.types import OptionalDatum
from restrict.types.numeric import (
    Add,
    Decimal_,
    Integer,
    Negate,
    Subtract,
    restrict_module,
)
from restrict.types.text import Hash, Text, Email
from restrict.types.time import Interval, Timestamp
from tests.types.test_builtins import InvalidFuncCallError


def test_restrict_module():
    assert len(restrict_module.types) == 2
    assert len(restrict_module.functions) == 3
    assert len(restrict_module.resources) == 0

    assert restrict_module.types["integer"] == Integer()
    assert restrict_module.types["decimal"] == Decimal_()

    assert type(restrict_module.functions["add"]) is Add
    assert type(restrict_module.functions["negate"]) is Negate
    assert type(restrict_module.functions["subtract"]) is Subtract


@pytest.mark.parametrize(
    "a,b,expected,op,opt",
    [
        a + [b, c]
        for a, b, c in product(
            [
                [Integer(), Decimal_(), Decimal_()],
                [Decimal_(), Decimal_(), Decimal_()],
                [Decimal_(), Integer(), Decimal_()],
                [Integer(), Integer(), Integer()],
            ],
            [Add(), Subtract()],
            [lambda x: x, OptionalDatum],
        )
    ],
)
def test_biop_calculates_nice_return_types(a, b, op, expected, opt):
    assert op.calculate_return_type([a, b]) == expected
    assert op.calculate_return_type([opt(a), b]) == opt(expected)
    assert op.calculate_return_type([a, opt(b)]) == opt(expected)
    assert op.calculate_return_type([opt(a), opt(b)]) == opt(expected)


@pytest.mark.parametrize("t", [Integer(), Decimal_()])
def test_types_do_not_rely_on_imports(t):
    assert t.get_imports() == set()


@pytest.mark.parametrize(
    "a,op",
    [
        (a, b)
        for a, b in product(
            [Boolean(), Text(), Hash(), Timestamp(), Interval(), Email()],
            [Add(), Subtract()],
        )
    ],
)
def test_biop_requires_two_numeric_types(a, op):
    b = Integer()
    with pytest.raises(InvalidFuncCallError):
        op.calculate_return_type([])

    with pytest.raises(InvalidFuncCallError):
        op.calculate_return_type([a])

    with pytest.raises(ValueError):
        op.calculate_return_type([a, b])

    with pytest.raises(InvalidFuncCallError):
        op.calculate_return_type([a, a, a])


def test_monop_requires_one_numeric_type():
    a = Boolean()
    op = Negate()

    with pytest.raises(InvalidFuncCallError):
        op.calculate_return_type([])

    with pytest.raises(ValueError):
        op.calculate_return_type([a])

    with pytest.raises(InvalidFuncCallError):
        op.calculate_return_type([a, a])

    with pytest.raises(InvalidFuncCallError):
        op.calculate_return_type([a, a, a])


@pytest.mark.parametrize(
    "a,opt",
    product([Integer(), Decimal_()], [lambda x: x, OptionalDatum]),
)
def test_negate_calculates_type(a, opt):
    op = Negate()
    assert op.calculate_return_type([opt(a)]) == opt(a)


@pytest.mark.parametrize("value,expected", [("1", 1), (1, 1)])
def test_integer_can_parse_strings_and_lets_through_ints(value, expected):
    assert Integer().parse(value) == expected


@pytest.mark.parametrize("value", [True, {}, [], Decimal("3.0")])
def test_integer_cannot_parse_other_values(value):
    with pytest.raises(ValueError):
        Integer().parse(value)


@pytest.mark.parametrize(
    "value,expected",
    [("1", "1.0"), ("1.0", "1.0"), (Decimal("1.0"), "1.0"), (1, "1.0")],
)
def test_decimal_can_parse_strings_and_lets_through_decimals_and_converts_ints(
    value, expected
):
    assert Decimal_().parse(value) == Decimal(expected)


@pytest.mark.parametrize("value", [True, {}, [], "a"])
def test_decimal_cannot_parse_other_values(value):
    with pytest.raises(ValueError):
        print(Decimal_().parse(value))


@pytest.mark.parametrize(
    "t,s",
    [(Integer(), "integer"), (Decimal_(), "number")],
)
def test_types_to_schema(t, s):
    assert t.to_schema() == {"type": s}
