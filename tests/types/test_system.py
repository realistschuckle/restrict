from restrict.types.system import EnvVar, restrict_module
from restrict.types.text import Text


def test_restrict_module():
    assert len(restrict_module.types) == 0
    assert len(restrict_module.functions) == 1
    assert len(restrict_module.resources) == 0

    assert type(restrict_module.functions["env"]) is EnvVar


def test_env_function_works_for_one_and_two_string_args():
    op = EnvVar()
    arg = Text()

    result = op.calculate_return_type([arg])
    assert result == Text()

    result = op.calculate_return_type([arg, arg])
    assert result == Text()
