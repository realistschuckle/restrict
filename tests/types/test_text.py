from itertools import product

import pytest

from restrict.compiler.types import OptionalDatum
from restrict.types.builtins import Boolean
from restrict.types.numeric import Decimal_, Integer
from restrict.types.text import (
    Email,
    Hash,
    RandomString,
    Text,
    TextLen,
    restrict_module,
)
from restrict.types.time import Interval, Timestamp
from tests.types.test_builtins import InvalidFuncCallError


def test_restrict_module():
    assert len(restrict_module.types) == 3
    assert len(restrict_module.functions) == 2
    assert len(restrict_module.resources) == 0

    assert restrict_module.types["email"] == Email()
    assert restrict_module.types["hash"] == Hash()
    assert restrict_module.types["text"] == Text()

    assert type(restrict_module.functions["random_string"]) is RandomString
    assert type(restrict_module.functions["text_len"]) is TextLen


@pytest.mark.parametrize(
    "a,opt",
    product([Text(), Hash(), Email()], [lambda x: x, OptionalDatum]),
)
def test_text_len_works_on_text_types(a, opt):
    op = TextLen()
    assert op.calculate_return_type([opt(a)]) == opt(Integer())


def test_random_string_returns_text_for_integer_arg():
    op = RandomString()
    assert op.calculate_return_type([Integer()]) == Text()


def test_random_string_returns_text_fails_for_optional_integer_arg():
    op = RandomString()
    with pytest.raises(ValueError):
        op.calculate_return_type([OptionalDatum(Integer())])


@pytest.mark.parametrize(
    "a",
    [Boolean(), Timestamp(), Interval(), Integer(), Decimal_()],
)
def test_text_len_requires_one_text_types(a):
    op = TextLen()
    b = Text()

    with pytest.raises(InvalidFuncCallError):
        op.calculate_return_type([])

    with pytest.raises(ValueError):
        op.calculate_return_type([a])

    with pytest.raises(InvalidFuncCallError):
        op.calculate_return_type([a, b])

    with pytest.raises(InvalidFuncCallError):
        op.calculate_return_type([a, a, a])


@pytest.mark.parametrize(
    "a",
    [Text(), Hash(), Boolean(), Timestamp(), Interval(), Decimal_()],
)
def test_random_string_requires_one_integer_args(a):
    op = RandomString()

    with pytest.raises(ValueError):
        op.calculate_return_type([a])


@pytest.mark.parametrize(
    "t,value,expected",
    [
        (a, b[0], b[1])
        for a, b in product(
            [Email(), Text(), Hash()], [("true", "true"), ("false", "false")]
        )
    ],
)
def test_text_can_parse_strings(t, value, expected):
    assert t.parse(value) == expected


@pytest.mark.parametrize(
    "t,value",
    product([Email(), Text(), Hash()], [1, {}, [], 3.0]),
)
def test_text_cannot_parse_other_values(t, value):
    with pytest.raises(ValueError):
        t.parse(value)


@pytest.mark.parametrize(
    "t,f",
    [
        (Text(), {}),
        (Email(), {"format": "email"}),
        (Hash(), {"contentEncoding": "base64"}),
    ],
)
def test_types_to_schema(t, f):
    assert t.to_schema() == {"type": "string"} | f
