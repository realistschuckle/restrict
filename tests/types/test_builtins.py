from collections.abc import Callable
from dataclasses import dataclass
from itertools import pairwise, product
from typing import Protocol, override, overload

import pytest

from restrict.compiler.exceptions.compile import InvalidFuncCallError
from restrict.compiler.types import (
    Data,
    Datum,
    DatumTuple,
    FuncParam,
    Function,
    OptionalResource,
    OptionalDatum,
    Relation,
    Resource,
    ResourceCollection,
    ResourceTuple,
)
from restrict.types.builtins import (
    All,
    And,
    Any,
    Boolean,
    Equals,
    Filter,
    GreaterThan,
    GreaterThanOrEqual,
    LessThan,
    LessThanOrEqual,
    Map,
    Not,
    Or,
    Zip,
)
from restrict.types.numeric import Decimal_, Integer
from restrict.types.text import Email, Hash, Text
from restrict.types.time import Interval, Timestamp

from ..visitors.fake_module import OverrideMe


class Proto(Protocol):
    def __call__(self, *args: Any) -> str | None: ...


attrs: dict[str, Proto | str | list | bool] = {
    name: value
    for name, value in [
        ("compiled_name", lambda s, *_: f"${s.name}"),
        ("get_effects", lambda *_: {}),
        ("get_fields", lambda *_: {}),
        ("get_global_names", lambda *_: []),
        ("get_relations", lambda *_: {}),
        ("get_rules", lambda *_: {}),
        ("get_singular_relations", lambda *_: {}),
        ("resolve_path", lambda *_: ""),
        ("to_schema", lambda *_: {}),
        ("name", "STUBBY"),
        ("field_order", []),
    ]
}
StubResource = type("Foo", (Resource,), attrs)


@pytest.mark.parametrize(
    "arg,op,opt",
    product(
        [Boolean, Integer, Decimal_, Text, Hash, Timestamp, Interval, Email],
        [
            Equals(),
            GreaterThan(),
            GreaterThanOrEqual(),
            LessThan(),
            LessThanOrEqual(),
        ],
        [lambda x: x, OptionalDatum],
    ),
)
def test_calculate_return_type_for_same_types(
    arg: type[Datum],
    op: Function,
    opt: Callable[[Datum], Datum],
):
    result = op.calculate_return_type([arg(), arg()])
    assert result == Boolean()

    result = op.calculate_return_type([opt(arg()), arg()])
    assert result == opt(Boolean())

    result = op.calculate_return_type([arg(), opt(arg())])
    assert result == opt(Boolean())

    result = op.calculate_return_type([opt(arg()), opt(arg())])
    assert result == opt(Boolean())


@pytest.mark.parametrize(
    "arg,op",
    [
        (a, b)
        for a, b in product(
            [
                Boolean,
                Integer,
                Decimal_,
                Text,
                Hash,
                Timestamp,
                Interval,
                Email,
            ],
            [
                Equals,
                GreaterThan,
                GreaterThanOrEqual,
                LessThan,
                LessThanOrEqual,
            ],
        )
    ],
)
def test_raise_error_for_too_many_args_for_binary_comparable_functions(
    arg: type[Datum],
    op: type[Function],
):
    eq = op()

    with pytest.raises(InvalidFuncCallError):
        eq.calculate_return_type([arg(), arg(), arg()])


@pytest.mark.parametrize(
    "arg,op",
    [
        (a, b)
        for a, b in product(
            [
                Boolean,
                Integer,
                Decimal_,
                Text,
                Hash,
                Timestamp,
                Interval,
                Email,
            ],
            [
                Equals,
                GreaterThan,
                GreaterThanOrEqual,
                LessThan,
                LessThanOrEqual,
            ],
        )
    ],
)
def test_raise_error_for_too_few_args_for_binary_comparable_functions(
    arg: type[Datum],
    op: type[Function],
):
    eq = op()

    with pytest.raises(InvalidFuncCallError):
        eq.calculate_return_type([arg()])


@pytest.mark.parametrize(
    "left,right,op,opt",
    [
        (a[0], a[1], b, opt)
        for a, b, opt in product(
            [
                (Integer(), Decimal_()),
                (Decimal_(), Integer()),
                (Text(), Hash()),
                (Hash(), Text()),
                (Email(), Text()),
                (Email(), Hash()),
                (Text(), Email()),
                (Hash(), Email()),
            ],
            [
                Equals,
                LessThan,
                LessThanOrEqual,
                GreaterThan,
                GreaterThanOrEqual,
            ],
            [lambda x: x, OptionalDatum],
        )
    ],
)
def test_calculate_return_type_for_comparable_types(
    left: Datum,
    right: Datum,
    op: type[Function],
    opt: Callable[[Datum], Datum],
):
    eq = op()

    result = eq.calculate_return_type([left, right])
    assert result == Boolean()

    result = eq.calculate_return_type([opt(left), right])
    assert result == opt(Boolean())

    result = eq.calculate_return_type([opt(left), opt(right)])
    assert result == opt(Boolean())

    result = eq.calculate_return_type([left, opt(right)])
    assert result == opt(Boolean())


@pytest.mark.parametrize(
    "left,right,op",
    [
        (a[0], a[1], b)
        for a, b in product(
            [(Integer(), Boolean()), (Boolean(), Integer())],
            [
                Equals,
                LessThan,
                LessThanOrEqual,
                GreaterThan,
                GreaterThanOrEqual,
            ],
        )
    ],
)
def test_calculate_return_type_raises_errors_for_incompatible_comparable_types(
    left: Datum,
    right: Datum,
    op: type[Function],
):
    eq = op()

    with pytest.raises(ValueError):
        eq.calculate_return_type([left, right])


@pytest.mark.parametrize(
    "operand,op",
    [
        (a, b)
        for a, b in product(
            [Integer(), Decimal_(), Text(), Hash(), Timestamp(), Interval()],
            [And, Or, Not],
        )
    ],
)
def test_calculate_return_type_raises_errors_for_incompatible_boolean_types(
    operand: Datum,
    op: type[Function],
):
    eq = op()

    with pytest.raises(InvalidFuncCallError):
        eq.calculate_return_type([Boolean(), Boolean(), Boolean()])

    if op is Not:
        with pytest.raises(InvalidFuncCallError):
            eq.calculate_return_type([Boolean(), operand])

        with pytest.raises(InvalidFuncCallError):
            eq.calculate_return_type([operand, Boolean()])

        with pytest.raises(ValueError):
            eq.calculate_return_type([operand])
    else:
        with pytest.raises(ValueError):
            eq.calculate_return_type([Boolean(), operand])

        with pytest.raises(ValueError):
            eq.calculate_return_type([operand, Boolean()])

        with pytest.raises(InvalidFuncCallError):
            eq.calculate_return_type([operand])


@pytest.mark.parametrize(
    "op,opt",
    product([And(), Or(), Not()], [lambda x: x, OptionalDatum]),
)
def test_calculate_return_type_for_boolean_types(
    op: Function,
    opt: Callable[[Datum], Datum],
):
    ops: list[Datum] = [opt(Boolean())]
    if type(op) is not Not:
        ops.append(Boolean())
    result = op.calculate_return_type(ops)

    assert result == opt(Boolean())


@pytest.mark.parametrize("op,coll", product([All(), Any()], [list, set]))
def test_boolean_reduction_methods_returns_true_when_collection_of_boolean(
    op: Function,
    coll: type[list] | type[set],
):
    t = Data(coll, Boolean())
    assert type(op.calculate_return_type([t])) is Boolean


@pytest.mark.parametrize("op,coll", product([All(), Any()], [list, set]))
def test_boolean_reduction_methods_returns_true_when_optional_collection_of_boolean(
    op: Function,
    coll: type[list] | type[set],
):
    t = Data(coll, Boolean())
    t = OptionalDatum(t)
    assert op.calculate_return_type([t]) == OptionalDatum(Boolean())


@pytest.mark.parametrize(
    "op,t",
    product(
        [All(), Any()],
        [Boolean, Integer, Decimal_, Text, Hash, Timestamp, Interval],
    ),
)
def test_boolean_reduction_methods_raises_error_when_not_list_type(op, t):
    with pytest.raises(ValueError):
        op.calculate_return_type([t()])


@pytest.mark.parametrize(
    "op,t",
    product(
        [All(), Any()],
        [Boolean, Integer, Decimal_, Text, Hash, Timestamp, Interval],
    ),
)
def test_boolean_reduction_methods_raises_error_when_not_one_arg(op, t):
    with pytest.raises(InvalidFuncCallError):
        op.calculate_return_type([])

    with pytest.raises(InvalidFuncCallError):
        op.calculate_return_type([t(), t()])

    with pytest.raises(InvalidFuncCallError):
        x = Data(list, Boolean())
        op.calculate_return_type([x, t()])


@pytest.mark.parametrize("op", [Map(), Filter()])
def test_map_raises_error_for_not_having_collection_params(op):
    with pytest.raises(ValueError):
        op.prepare_params(Integer())


@pytest.mark.parametrize("op", [Map(), Filter()])
def test_map_raises_error_for_not_same_args_and_params(op):
    op.define_params([FuncParam("x")])
    op.prepare_params(Data(list, Integer()))
    with pytest.raises(ValueError):
        op.calculate_return_type([Data(set, Integer()), Decimal_()])


@pytest.mark.parametrize(
    "op,opt",
    product([Map(), Filter()], [lambda x: x, OptionalDatum]),
)
def test_map_calculates_return_value_for_single_datum_param(op, opt):
    x = FuncParam("x")
    coll = opt(Data(list, Integer()))
    result_coll = opt(Data(list, Decimal_()))

    op.define_params([x])
    op.prepare_params(coll)

    result = op.calculate_return_type([coll, Decimal_()])
    assert result == result_coll
    assert x.res == Integer()


@pytest.mark.parametrize(
    "op,opt",
    product([Map(), Filter()], [lambda x: x, OptionalResource]),
)
def test_map_calculates_return_value_for_single_resource_param(op, opt):
    x = FuncParam("x")
    coll = opt(ResourceCollection(list, StubResource()))
    result_coll = opt(ResourceCollection(list, StubResource()))

    op.define_params([x])
    op.prepare_params(coll)

    result = op.calculate_return_type([coll, StubResource()])
    assert result == result_coll
    assert x.res == StubResource()


@pytest.mark.parametrize(
    "op,opt",
    product([Map(), Filter()], [lambda x: x, OptionalDatum]),
)
def test_map_calculates_return_value_for_multiple_data_params(op, opt):
    x = FuncParam("x")
    y = FuncParam("y")
    tup = DatumTuple([Integer(), Text()])
    coll = opt(Data(list, tup))
    result_type = Decimal_()

    op.define_params([x, y])
    op.prepare_params(coll)

    result = op.calculate_return_type([coll, result_type])
    assert result == opt(Data(list, result_type))
    assert x.res == Integer()
    assert y.res == Text()


@pytest.mark.parametrize(
    "op,opt",
    product([Map(), Filter()], [lambda x: x, OptionalResource]),
)
def test_map_calculates_return_value_for_multiple_resource_params(op, opt):
    x = FuncParam("x")
    y = FuncParam("y")
    tup = ResourceTuple([StubResource(), StubResource()])
    coll = opt(ResourceCollection(list, tup))
    result_type = StubResource()

    op.define_params([x, y])
    op.prepare_params(coll)

    result = op.calculate_return_type([coll, result_type])
    assert result == opt(ResourceCollection(list, result_type))
    assert x.res == StubResource()
    assert y.res == StubResource()


@pytest.mark.parametrize(
    "t",
    [Boolean, Integer, Decimal_, Text, Hash, Timestamp, Interval],
)
def test_zip_raises_error_when_not_list_types(t):
    op = Zip()
    with pytest.raises(InvalidFuncCallError):
        op.calculate_return_type([t()])


@pytest.mark.parametrize(
    "t",
    [Boolean, Integer, Decimal_, Text, Hash, Timestamp, Interval],
)
def test_zip_raises_error_when_not_two_collection_args(t):
    op = Zip()
    with pytest.raises(InvalidFuncCallError):
        op.calculate_return_type([])
    with pytest.raises(InvalidFuncCallError):
        op.calculate_return_type([t()])
    with pytest.raises(InvalidFuncCallError):
        x = Data(list, t())
        op.calculate_return_type([x])
    with pytest.raises(ValueError):
        x = Data(list, t())
        op.calculate_return_type([x, t()])


@pytest.mark.parametrize(
    "coll,first,second,opt",
    [
        (x, a[0], a[1], opt)
        for x, a, opt in product(
            [list, set],
            pairwise([Boolean, Integer, Decimal_, Text, Hash, Timestamp, Interval]),
            [lambda x: x, OptionalDatum],
        )
    ],
)
def test_zip_returns_tuple_of_values_when_two_collection_of_values(
    coll: type[list] | type[set],
    first: type[Datum],
    second: type[Datum],
    opt: Callable[[Datum], Datum],
):
    left = opt(Data(coll, first()))
    right = opt(Data(coll, second()))
    op = Zip()

    result = op.calculate_return_type([left, right])

    if type(result) is OptionalDatum:
        result = result.internal_type
    assert type(result) is Data
    assert result.collection_type is list
    assert type(result.internal_type) is DatumTuple
    assert type(result.internal_type.internal_types[0]) is first
    assert type(result.internal_type.internal_types[1]) is second


def test_collection_type_name():
    assert Data(list, Boolean()).name == "list[boolean]"


def test_tuple_type_name():
    assert DatumTuple([Boolean()]).name == "tuple[boolean]"
    assert [type(x) for x in DatumTuple([Boolean()]).internal_types] == [Boolean]
    assert DatumTuple([Boolean(), Integer()]).name == "tuple[boolean, integer]"


def test_func_param_ref_with_empty_path_resolves_param_type():
    x = FuncParam("bob", Boolean())

    assert x.create_ref([]).resolve_type() == Boolean()


@dataclass(frozen=True)
class StubData(Datum):
    @property
    def name(self) -> str:
        return "Data"

    @override
    def to_schema(self) -> dict[str, Any]:
        return {}


def test_collection_type_of_scalar_can_have_used_read_and_set():
    coll = Data(list, Boolean())

    assert not coll.used
    coll.used = True
    assert coll.used


def test_collection_type_of_frozen_scalar_can_have_used_read_and_set():
    coll = Data(list, StubData())

    assert not coll.used
    coll.used = True
    assert not coll.used


def test_collection_type_does_not_equal_non_collections():
    coll = Data(list, Boolean())

    assert coll != "Hi!"


def test_tuple_type_of_scalar_can_have_used_read_and_set():
    tup = DatumTuple([Boolean()])

    assert not tup.used
    tup.used = True
    assert tup.used


def test_tuple_type_of_frozen_scalar_can_have_used_read_and_set():
    tup = DatumTuple([StubData()])

    assert not tup.used
    tup.used = True
    assert not tup.used


class FakeFunction(Function):
    num_args = (1, 2)

    @overload
    def _calculate_return_type(self, args: list[Datum]) -> Datum | None: ...

    @overload
    def _calculate_return_type(self, args: list[Resource]) -> Resource | None: ...

    @override
    def _calculate_return_type(
        self,
        args: list[Datum] | list[Resource],
    ) -> Datum | Resource | None:
        left, right = args
        return None


def test_default_function_clone_returns_new_instance():
    func = FakeFunction()

    clone = func.clone()

    assert clone != func
    assert type(clone) is type(func)


def test_clone_of_relation():
    res = OverrideMe()
    rel = Relation("bob", res)

    clone = rel.clone()

    assert clone != rel
    assert clone.name == rel.name
    assert clone.res == rel.res


@pytest.mark.parametrize(
    "value,expected",
    [("true", True), ("false", False), (True, True), (False, False)],
)
def test_boolean_can_parse_strings_and_lets_through_bools(value, expected):
    assert Boolean().parse(value) == expected


@pytest.mark.parametrize("value", [1, {}, [], 3.0])
def test_boolean_cannot_parse_other_values(value):
    with pytest.raises(ValueError):
        Boolean().parse(value)


@pytest.mark.parametrize(
    "value,expected",
    [("true", True), ("false", False), (True, True), (False, False)],
)
def test_optional_boolean_can_parse_strings_and_lets_through_bools(value, expected):
    assert OptionalDatum(Boolean()).parse(value) == expected


@pytest.mark.parametrize("value", [1, {}, [], 3.0])
def test_optional_boolean_cannot_parse_other_values(value):
    with pytest.raises(ValueError):
        OptionalDatum(Boolean()).parse(value)


def test_optional_type_repr():
    assert repr(OptionalDatum(Integer())) == "OptionalDatum(Integer())"


def test_optional_type_can_compare():
    left = OptionalDatum(Integer())
    right = OptionalDatum(Integer())

    assert left.can_compare(right)


def test_optional_type_is_not_required():
    assert not OptionalDatum(Integer()).is_required


def test_optional_type_to_schema_returns_internal_type():
    assert OptionalDatum(Integer()).to_schema() == Integer().to_schema()


def test_collection_type_repr():
    assert repr(Data(list, Boolean())) == "Data(list, Boolean())"


def test_return_type_for_tuple_num_args():
    fn = FakeFunction()

    with pytest.raises(InvalidFuncCallError):
        fn.calculate_return_type([])

    with pytest.raises(InvalidFuncCallError):
        fn.calculate_return_type([Integer(), Integer(), Integer()])


def test_filter_to_py_needs_two_ast_args():
    with pytest.raises(InvalidFuncCallError):
        Filter().to_py(False, [], None)


def test_map_to_py_needs_two_ast_args():
    with pytest.raises(InvalidFuncCallError):
        Map().to_py(False, [], None)


def test_boolean_to_schema():
    assert Boolean().to_schema() == {"type": "boolean"}


@pytest.mark.parametrize(
    "op", [Equals(), LessThan(), LessThanOrEqual(), GreaterThan(), GreaterThanOrEqual()]
)
def test_binary_comparable_functions_cannot_take_resources(op):
    with pytest.raises(ValueError):
        op.calculate_return_type([Integer(), StubResource()])


@pytest.mark.parametrize(
    "od", [OptionalDatum(Boolean()), OptionalResource(StubResource())]
)
def test_optional_wrap_passes_through_existing_optional(od):
    assert od.wrap(od) == od


def test_optional_datum_type_has_a_nice_name():
    assert OptionalDatum(Boolean()).name == "optional boolean"


def test_parsing_data_fails_for_non_list_argument():
    with pytest.raises(ValueError):
        Data(list, Boolean()).parse("Hello")


def test_datum_tuple_fails_to_parse_and_json_with_wrong_number_of_values():
    tup = DatumTuple([Boolean()])

    with pytest.raises(ValueError):
        tup.parse([1, 2])

    with pytest.raises(ValueError):
        tup.to_json([1, 2])


def test_optional_resource():
    res = OptionalResource(StubResource())

    assert repr(res) == "OptionalResource(<STUBBY>)"
    assert res.field_order == []
    assert res.name == "STUBBY"
    assert res.get_effects() == {}
    assert res.get_fields() == {}
    assert res.get_relations() == {}
    assert res.get_rules() == {}
    assert res.get_global_names() == []
    assert res.get_singular_relations() == {}
    assert res.to_schema() == {}


def test_resource_collection():
    res = ResourceCollection(list, StubResource())

    assert repr(res) == "ResourceCollection(list, <STUBBY>)"
    assert res.field_order == []
    assert res.name == "STUBBY"
    assert res.get_effects() == {}
    assert res.get_fields() == {}
    assert res.get_relations() == {}
    assert res.get_rules() == {}
    assert res.get_global_names() == []
    assert res.get_singular_relations() == {}
    assert res.to_schema() == {"items": {}, "type": "array", "uniqueItems": False}
    assert res != 3
    assert res.compiled_name() == "$STUBBY"
    res.used = False
    assert res.used is False
    res.used = True
    assert res.used is True


def test_resource_tuple():
    res = ResourceTuple([StubResource()])

    assert repr(res) == "ResourceTuple(<STUBBY>)"
    assert res.field_order == []
    assert res.name == "tuple[STUBBY]"
    assert res.get_effects() == {}
    assert res.get_fields() == {}
    assert res.get_relations() == {}
    assert res.get_rules() == {}
    assert res.get_global_names() == []
    assert res.get_singular_relations() == {}
    assert res.resolve_path([]) == ""
    assert res.to_schema() == {
        "prefixItems": [{}],
        "type": "array",
        "uniqueItems": False,
    }
    assert res != 3
    res.used = False
    assert res.used is False
    res.used = True
    assert res.used is True

    with pytest.raises(NotImplementedError):
        assert res.compiled_name() == {}

    res = ResourceTuple([])
    assert res.used is False
