import pytest

from restrict.compiler.lexers import RestrictLexer


def test_unbuilt_lexer_cannot_take_input():
    with pytest.raises(RuntimeError):
        RestrictLexer().input("Hi")


def test_unbuilt_lexer_has_unbuilt_state():
    assert RestrictLexer().current_state() == "<UNBUILT>"


def test_unbuilt_lexer_cannot_give_tokens():
    with pytest.raises(RuntimeError):
        RestrictLexer().token()


def test_built_lexer_reports_lineno():
    assert RestrictLexer().build().lineno == 1


def test_built_lexer_reports_lexpos():
    assert RestrictLexer().build().lexpos == 0


def test_unbuilt_cannot_iterate():
    with pytest.raises(RuntimeError):
        for _ in RestrictLexer():
            pass
