import pytest
from decimal import Decimal

from restrict.compiler.lexers import RestrictLexer, RestrictLexerError


def check_expectations(expected, lexer):
    for expectation in expected:
        t = lexer.token()

        print(
            "RECEIVED: ",
            t,
            "\nSTATE:    ",
            lexer._lexer.lexstatestack + [lexer.current_state()],
            "\nEXPECTED: ",
            expectation,
            "\n",
        )

        assert lexer.errors == []
        assert t.type == expectation[0], t
        assert t.value == expectation[1], t
        assert lexer.current_state() == expectation[2], t
        if len(expectation) == 4:
            assert t.colno == expectation[3], t


@pytest.fixture(scope="module")
def lexer():
    lexer = RestrictLexer()
    lexer.build(debug=True)
    return lexer


@pytest.mark.parametrize(
    "i,o",
    [
        ("  \n  use", ("USE", "use", 5, 2, 2, 5)),
        ("  \n  place", ("TYPE", "place", 5, 2, 2, 7)),
        ("  \n  thing", ("TYPE", "thing", 5, 2, 2, 7)),
        ("  \n  party", ("TYPE", "party", 5, 2, 2, 7)),
        ("  \n  role", ("TYPE", "role", 5, 2, 2, 6)),
        ("  \n  interval", ("TYPE", "interval", 5, 2, 2, 10)),
        ("  \n  moment", ("TYPE", "moment", 5, 2, 2, 8)),
        ("  \n  override", ("OVERRIDE", "override", 5, 2, 2, 10)),
    ],
)
def test_top_level_tokens(lexer, i, o):
    lexer.input(i)

    t = lexer.token()

    assert t.type == o[0], t
    assert t.value == o[1], t
    assert t.lexpos == o[2], t
    assert t.lineno == o[3], t
    assert t.colno == o[4], t
    assert lexer.colno == o[5], t


def test_use_statement_state(lexer):
    lexer.input("use <hello/use/thing>")
    expected = [
        ("USE", "use", "use"),
        ("<", "<", "path"),
        ("PATH", "hello/use/thing", "use"),
        (">", ">", "INITIAL"),
    ]
    check_expectations(expected, lexer)


def test_use_errors(lexer):
    lexer.input("a use b <hello/use/thing> c")

    for t in lexer:
        print(t)

    assert lexer.errors == [
        RestrictLexerError(
            value="a use b <hello/use/thing> c",
            state="INITIAL",
            lineno=1,
            colno=0,
            pos=0,
        ),
        RestrictLexerError(
            value="b <hello/use/thing> c",
            state="use",
            lineno=1,
            colno=6,
            pos=6,
        ),
        RestrictLexerError(
            value="c",
            state="INITIAL",
            lineno=1,
            colno=26,
            pos=26,
        ),
    ]


def test_refer_to_statement_state(lexer):
    lexer.input("refer to <hello/refer/to/thing> as hut")
    expected = [
        ("REFER", "refer", "refer"),
        ("TO", "to", "referto"),
        ("<", "<", "path"),
        ("PATH", "hello/refer/to/thing", "referto"),
        (">", ">", "referto"),
        ("AS", "as", "refertoas"),
        ("ID", "hut", "INITIAL"),
    ]

    check_expectations(expected, lexer)


def test_refer_to_errors(lexer):
    lexer.input("a refer b to c <hello/refer/to/thing> d as hut e")

    for t in lexer:
        print(t)

    assert lexer.errors == [
        RestrictLexerError(
            value="a refer b to c <hello/refer/to/thing> d as hut e",
            state="INITIAL",
            lineno=1,
            colno=0,
            pos=0,
        ),
        RestrictLexerError(
            value="b to c <hello/refer/to/thing> d as hut e",
            state="refer",
            lineno=1,
            colno=8,
            pos=8,
        ),
        RestrictLexerError(
            value="c <hello/refer/to/thing> d as hut e",
            state="referto",
            lineno=1,
            colno=13,
            pos=13,
        ),
        RestrictLexerError(
            value="d as hut e",
            state="referto",
            lineno=1,
            colno=38,
            pos=38,
        ),
        RestrictLexerError(
            value="e",
            state="INITIAL",
            lineno=1,
            colno=47,
            pos=47,
        ),
    ]


@pytest.mark.parametrize(
    "type",
    ["moment", "party", "place", "thing", "role", "interval"],
)
def test_resource_declaration_with_override_and_prefix(lexer, type):
    lexer.input(f"override {type} prefix.XoXo {{ }}")
    expected = [
        ("OVERRIDE", "override", "INITIAL", 0),
        ("TYPE", type, "resource", 9),
        ("ID", "prefix", "resource", 10 + len(type)),
        (".", ".", "resource", 16 + len(type)),
        ("NAME", "XoXo", "resource", 17 + len(type)),
        ("{", "{", "body", 22 + len(type)),
        ("}", "}", "INITIAL", 24 + len(type)),
    ]
    check_expectations(expected, lexer)


@pytest.mark.parametrize(
    "type",
    ["party", "place", "thing", "role", "moment", "interval"],
)
def test_resource_declaration(lexer, type):
    lexer.input(f"{type} XoXo {{ }}")
    expected = [
        ("TYPE", type, "resource"),
        ("NAME", "XoXo", "resource"),
        ("{", "{", "body"),
        ("}", "}", "INITIAL"),
    ]
    check_expectations(expected, lexer)


def test_data_section(lexer):
    lexer.input(
        """
        thing XoXo {
            data {
                value: list text;
                one: optional list int;
                two: x.int : value |> x.gt(0) |> and(value |> lt(10.0));
                three = one |> add(two) |> add(value);
                four: set text;
                five: optional int;
                six: unique text;
            }
        }
        """
    )
    expected = [
        ("TYPE", "thing", "resource"),
        ("NAME", "XoXo", "resource"),
        ("{", "{", "body"),
        ("DATA", "data", "data"),
        ("{", "{", "data"),
        #
        ("ID", "value", "field"),
        (":", ":", "constrainedfield"),
        ("LIST", "list", "constrainedfield"),
        ("ID", "text", "constrainedfield"),
        (";", ";", "data"),
        #
        ("ID", "one", "field"),
        (":", ":", "constrainedfield"),
        ("OPTIONAL", "optional", "constrainedfield"),
        ("LIST", "list", "constrainedfield"),
        ("ID", "int", "constrainedfield"),
        (";", ";", "data"),
        #
        ("ID", "two", "field"),
        (":", ":", "constrainedfield"),
        ("ID", "x", "constrainedfield"),
        (".", ".", "constrainedfield"),
        ("ID", "int", "constrainedfield"),
        (":", ":", "constraint"),
        ("VALUE", "value", "constraint"),
        ("PIPEOP", "|>", "constraint"),
        ("ID", "x", "constraint"),
        (".", ".", "coprop"),
        ("ID", "gt", "constraint"),
        ("(", "(", "constraint"),
        ("INT", 0, "constraint"),
        (")", ")", "constraint"),
        ("PIPEOP", "|>", "constraint"),
        ("ID", "and", "constraint"),
        ("(", "(", "constraint"),
        ("VALUE", "value", "constraint"),
        ("PIPEOP", "|>", "constraint"),
        ("ID", "lt", "constraint"),
        ("(", "(", "constraint"),
        ("DECIMAL", 10.0, "constraint"),
        (")", ")", "constraint"),
        (")", ")", "constraint"),
        (";", ";", "data"),
        #
        ("ID", "three", "field"),
        ("=", "=", "computation"),
        ("ID", "one", "computation"),
        ("PIPEOP", "|>", "computation"),
        ("ID", "add", "computation"),
        ("(", "(", "computation"),
        ("ID", "two", "computation"),
        (")", ")", "computation"),
        ("PIPEOP", "|>", "computation"),
        ("ID", "add", "computation"),
        ("(", "(", "computation"),
        ("VALUE", "value", "computation"),
        (")", ")", "computation"),
        (";", ";", "data"),
        #
        ("ID", "four", "field"),
        (":", ":", "constrainedfield"),
        ("SET", "set", "constrainedfield"),
        ("ID", "text", "constrainedfield"),
        (";", ";", "data"),
        #
        ("ID", "five", "field"),
        (":", ":", "constrainedfield"),
        ("OPTIONAL", "optional", "constrainedfield"),
        ("ID", "int", "constrainedfield"),
        (";", ";", "data"),
        #
        #
        ("ID", "six", "field"),
        (":", ":", "constrainedfield"),
        ("UNIQUE", "unique", "constrainedfield"),
        ("ID", "text", "constrainedfield"),
        (";", ";", "data"),
        #
        ("}", "}", "body"),
        ("}", "}", "INITIAL"),
    ]
    check_expectations(expected, lexer)


def test_dnc_section(lexer):
    lexer.input(
        """
        thing XoXo {
            dnc {
                value: optional Thing1;
                <next> next: list x.Thing2;
                <previous> previous[1, 10]: set Thing3;
                <root> root: Thing04Thing;
                <details> details: optional set Thing5;
                foop: unique Thing6;
            }
        }
        """
    )
    expected = [
        ("TYPE", "thing", "resource"),
        ("NAME", "XoXo", "resource"),
        ("{", "{", "body"),
        ("DNC", "dnc", "dnc"),
        ("{", "{", "dnc"),
        #
        ("ID", "value", "rel"),
        (":", ":", "rel"),
        ("OPTIONAL", "optional", "rel"),
        ("NAME", "Thing1", "rel"),
        (";", ";", "dnc"),
        #
        ("<", "<", "reldec"),
        ("REL", "next", "reldec"),
        (">", ">", "dnc"),
        ("ID", "next", "rel"),
        (":", ":", "rel"),
        ("LIST", "list", "rel"),
        ("ID", "x", "rel"),
        (".", ".", "rel"),
        ("NAME", "Thing2", "rel"),
        (";", ";", "dnc"),
        #
        ("<", "<", "reldec"),
        ("REL", "previous", "reldec"),
        (">", ">", "dnc"),
        ("ID", "previous", "rel"),
        ("[", "[", "rel"),
        ("INT", 1, "rel"),
        (",", ",", "rel"),
        ("INT", 10, "rel"),
        ("]", "]", "rel"),
        (":", ":", "rel"),
        ("SET", "set", "rel"),
        ("NAME", "Thing3", "rel"),
        (";", ";", "dnc"),
        #
        ("<", "<", "reldec"),
        ("REL", "root", "reldec"),
        (">", ">", "dnc"),
        ("ID", "root", "rel"),
        (":", ":", "rel"),
        ("NAME", "Thing04Thing", "rel"),
        (";", ";", "dnc"),
        #
        ("<", "<", "reldec"),
        ("REL", "details", "reldec"),
        (">", ">", "dnc"),
        ("ID", "details", "rel"),
        (":", ":", "rel"),
        ("OPTIONAL", "optional", "rel"),
        ("SET", "set", "rel"),
        ("NAME", "Thing5", "rel"),
        (";", ";", "dnc"),
        #
        ("ID", "foop", "rel"),
        (":", ":", "rel"),
        ("UNIQUE", "unique", "rel"),
        ("NAME", "Thing6", "rel"),
        (";", ";", "dnc"),
        #
        ("}", "}", "body"),
        ("}", "}", "INITIAL"),
    ]
    check_expectations(expected, lexer)


def test_effects_section(lexer):
    lexer.input(
        """
        thing XoXo {
            effects {
                create {
                    name = "Curtis";
                    borp = create Borp;
                    mother = modify x { thing = self; };
                    value = create x.Thing { thing = self; other = x; };
                    other = value |> time_add(ts`P1D`);
                    other2 = value |> time_add(x.ts`P1D`);
                }
                modify {
                    borp = modify borp;
                    value = 1 |> add(.1);
                }
                delete {
                    value = random_string();
                    others = selves |> filter[x, y](x.value |> gt(y.o.value));
                }
            }
        }
        """
    )
    expected = [
        ("TYPE", "thing", "resource"),
        ("NAME", "XoXo", "resource"),
        ("{", "{", "body"),
        ("EFFECTS", "effects", "effects"),
        ("{", "{", "effects"),
        ("CREATE", "create", "effectsec"),
        ("{", "{", "effectsec"),
        #
        ("ID", "name", "effectsec"),
        ("=", "=", "computation"),
        ("STRING", '"Curtis"', "computation"),
        (";", ";", "effectsec"),
        #
        ("ID", "borp", "effectsec"),
        ("=", "=", "computation"),
        ("CREATE", "create", "compmod"),
        ("NAME", "Borp", "compmod"),
        (";", ";", "effectsec"),
        #
        ("ID", "mother", "effectsec"),
        ("=", "=", "computation"),
        ("MODIFY", "modify", "compmod"),
        ("ID", "x", "compmod"),
        ("{", "{", "effectsec"),
        ("ID", "thing", "effectsec"),
        ("=", "=", "computation"),
        ("SELF", "self", "computation"),
        (";", ";", "effectsec"),
        ("}", "}", "computation"),
        (";", ";", "effectsec"),
        ("ID", "value", "effectsec"),
        ("=", "=", "computation"),
        ("CREATE", "create", "compmod"),
        ("ID", "x", "compmod"),
        (".", ".", "compmod"),
        ("NAME", "Thing", "compmod"),
        ("{", "{", "effectsec"),
        ("ID", "thing", "effectsec"),
        ("=", "=", "computation"),
        ("SELF", "self", "computation"),
        (";", ";", "effectsec"),
        ("ID", "other", "effectsec"),
        ("=", "=", "computation"),
        ("ID", "x", "computation"),
        (";", ";", "effectsec"),
        ("}", "}", "computation"),
        (";", ";", "effectsec"),
        #
        ("ID", "other", "effectsec"),
        ("=", "=", "computation"),
        ("VALUE", "value", "computation"),
        ("PIPEOP", "|>", "computation"),
        ("ID", "time_add", "computation"),
        ("(", "(", "computation"),
        ("DATATAG", "ts", "datatag"),
        ("DATAVALUE", "P1D", "computation"),
        (")", ")", "computation"),
        (";", ";", "effectsec"),
        #
        ("ID", "other2", "effectsec"),
        ("=", "=", "computation"),
        ("VALUE", "value", "computation"),
        ("PIPEOP", "|>", "computation"),
        ("ID", "time_add", "computation"),
        ("(", "(", "computation"),
        ("ID", "x", "computation"),
        (".", ".", "coprop"),
        ("DATATAG", "ts", "datatag"),
        ("DATAVALUE", "P1D", "computation"),
        (")", ")", "computation"),
        (";", ";", "effectsec"),
        #
        ("}", "}", "effects"),
        ("MODIFY", "modify", "effectsec"),
        ("{", "{", "effectsec"),
        ("ID", "borp", "effectsec"),
        ("=", "=", "computation"),
        ("MODIFY", "modify", "compmod"),
        ("ID", "borp", "compmod"),
        (";", ";", "effectsec"),
        ("ID", "value", "effectsec"),
        ("=", "=", "computation"),
        ("INT", 1, "computation"),
        ("PIPEOP", "|>", "computation"),
        ("ID", "add", "computation"),
        ("(", "(", "computation"),
        ("DECIMAL", Decimal("0.1"), "computation"),
        (")", ")", "computation"),
        (";", ";", "effectsec"),
        ("}", "}", "effects"),
        ("DELETE", "delete", "effectsec"),
        ("{", "{", "effectsec"),
        ("ID", "value", "effectsec"),
        ("=", "=", "computation"),
        ("ID", "random_string", "computation"),
        ("(", "(", "computation"),
        (")", ")", "computation"),
        (";", ";", "effectsec"),
        ("ID", "others", "effectsec"),
        ("=", "=", "computation"),
        ("SELVES", "selves", "computation"),
        ("PIPEOP", "|>", "computation"),
        ("ID", "filter", "computation"),
        ("[", "[", "computation"),
        ("ID", "x", "computation"),
        (",", ",", "computation"),
        ("ID", "y", "computation"),
        ("]", "]", "computation"),
        ("(", "(", "computation"),
        ("ID", "x", "computation"),
        (".", ".", "coprop"),
        ("ID", "value", "computation"),
        ("PIPEOP", "|>", "computation"),
        ("ID", "gt", "computation"),
        ("(", "(", "computation"),
        ("ID", "y", "computation"),
        (".", ".", "coprop"),
        ("ID", "o", "computation"),
        (".", ".", "coprop"),
        ("ID", "value", "computation"),
        (")", ")", "computation"),
        (")", ")", "computation"),
        (";", ";", "effectsec"),
        ("}", "}", "effects"),
        ("}", "}", "body"),
        ("}", "}", "INITIAL"),
    ]
    check_expectations(expected, lexer)


def test_security_section(lexer):
    lexer.input(
        """
        thing XoXo {
            security {
                list: true;
                details {
                    value: true;
                }
                create: false;
                modify {
                    value: actor |> eq(creator);
                    *: actor |> eq(creator);
                }
                delete {
                    value: false;
                }
            }
        }
        """
    )
    expected = [
        ("TYPE", "thing", "resource"),
        ("NAME", "XoXo", "resource"),
        ("{", "{", "body"),
        ("SECURITY", "security", "security"),
        ("{", "{", "security"),
        ("LIST", "list", "securitysec"),
        (":", ":", "constraint"),
        ("TRUE", True, "constraint"),
        (";", ";", "security"),
        ("DETAILS", "details", "securitysec"),
        ("{", "{", "securitysec"),
        ("ID", "value", "constrainedfield"),
        (":", ":", "constraint"),
        ("TRUE", True, "constraint"),
        (";", ";", "securitysec"),
        ("}", "}", "security"),
        ("CREATE", "create", "securitysec"),
        (":", ":", "constraint"),
        ("FALSE", False, "constraint"),
        (";", ";", "security"),
        ("MODIFY", "modify", "securitysec"),
        ("{", "{", "securitysec"),
        ("ID", "value", "constrainedfield"),
        (":", ":", "constraint"),
        ("ID", "actor", "constraint"),
        ("PIPEOP", "|>", "constraint"),
        ("ID", "eq", "constraint"),
        ("(", "(", "constraint"),
        ("ID", "creator", "constraint"),
        (")", ")", "constraint"),
        (";", ";", "securitysec"),
        ("*", "*", "constrainedfield"),
        (":", ":", "constraint"),
        ("ID", "actor", "constraint"),
        ("PIPEOP", "|>", "constraint"),
        ("ID", "eq", "constraint"),
        ("(", "(", "constraint"),
        ("ID", "creator", "constraint"),
        (")", ")", "constraint"),
        (";", ";", "securitysec"),
        ("}", "}", "security"),
        ("DELETE", "delete", "securitysec"),
        ("{", "{", "securitysec"),
        ("ID", "value", "constrainedfield"),
        (":", ":", "constraint"),
        ("FALSE", False, "constraint"),
        (";", ";", "securitysec"),
        ("}", "}", "security"),
        ("}", "}", "body"),
        ("}", "}", "INITIAL"),
    ]
    check_expectations(expected, lexer)


def test_workflow_section(lexer):
    lexer.input(
        """
        thing XoXo {
            workflow {
                <entrypoint> list;
                details -> Role#create { thing = self; id = 0; }
                <alias> create -> x.Workflow#create;
            }
        }
        """
    )
    expected = [
        ("TYPE", "thing", "resource"),
        ("NAME", "XoXo", "resource"),
        ("{", "{", "body"),
        ("WORKFLOW", "workflow", "workflow"),
        ("{", "{", "workflow"),
        #
        ("<", "<", "wfdec"),
        ("ENTRYPOINT", "entrypoint", "wfdec"),
        (">", ">", "workflow"),
        ("LIST", "list", "wfarrow"),
        (";", ";", "workflow"),
        #
        ("DETAILS", "details", "wfarrow"),
        ("TRANSITION", "->", "wfname"),
        ("NAME", "Role", "wfhash"),
        ("#", "#", "wfmeth"),
        ("CREATE", "create", "wfend"),
        ("{", "{", "wfattr"),
        ("ID", "thing", "wfattr"),
        ("=", "=", "computation"),
        ("SELF", "self", "computation"),
        (";", ";", "wfattr"),
        ("ID", "id", "wfattr"),
        ("=", "=", "computation"),
        ("INT", 0, "computation"),
        (";", ";", "wfattr"),
        ("}", "}", "workflow"),
        #
        ("<", "<", "wfdec"),
        ("ALIAS", "alias", "wfdec"),
        (">", ">", "workflow"),
        ("CREATE", "create", "wfarrow"),
        ("TRANSITION", "->", "wfname"),
        ("ID", "x", "wfname"),
        (".", ".", "wfname"),
        ("NAME", "Workflow", "wfhash"),
        ("#", "#", "wfmeth"),
        ("CREATE", "create", "wfend"),
        (";", ";", "workflow"),
        #
        ("}", "}", "body"),
        ("}", "}", "INITIAL"),
    ]
    check_expectations(expected, lexer)
