import pytest
from restrict.compiler.types import LayeredMapping


def test_empty_at_creation():
    lm = LayeredMapping()

    assert len(lm) == 0


def test_len_is_union_of_all_layers():
    lm = LayeredMapping()
    lm["one"] = 1
    lm["two"] = 2
    lm["layer0"] = True
    assert len(lm) == 3

    lm = lm.layer()
    lm["one"] = "one"
    lm["three"] = 3
    lm["layer1"] = True
    assert len(lm) == 5

    lm = lm.layer()
    lm["one"] = "uno"
    lm["two"] = "dos"
    lm["three"] = "tres"
    lm["layer2"] = True

    assert len(lm) == 6


def test_raises_key_error_when_not_in_layers():
    lm = LayeredMapping().layer().layer()

    with pytest.raises(KeyError):
        lm["foo"]


def test_delete_removes_from_current_layer():
    lm = LayeredMapping({"a": 1}).layer({"a": 2}).layer({"a": 3})

    assert lm.get("a") == 3

    del lm["a"]

    assert lm.get("a") == 2


def test_iterator_is_union_of_keys():
    lm = LayeredMapping()
    lm["one"] = 1
    lm["two"] = 2
    lm["layer0"] = True

    lm = lm.layer()
    lm["one"] = "one"
    lm["three"] = 3
    lm["layer1"] = True

    lm = lm.layer()
    lm["one"] = "uno"
    lm["two"] = "dos"
    lm["three"] = "tres"
    lm["layer2"] = True

    keys = list(lm)

    assert len(keys) == 6
    for expceted_key in ["one", "two", "three", "layer2", "layer1", "layer0"]:
        assert expceted_key in keys


def test_repr():
    lm = LayeredMapping()
    lm["one"] = 1
    lm["two"] = 2
    lm["layer0"] = True

    lm = lm.layer()
    lm["one"] = "one"
    lm["three"] = 3
    lm["layer1"] = True

    lm = lm.layer()
    lm["one"] = "uno"
    lm["two"] = "dos"
    lm["three"] = "tres"
    lm["layer2"] = True

    assert repr(lm) == (
        "LayeredMapping({'one': 1, 'two': 2, 'layer0': True})"
        ".layer({'one': 'one', 'three': 3, 'layer1': True})"
        ".layer({'one': 'uno', 'two': 'dos', 'three': 'tres', 'layer2': True})"
    )
