import os
from datetime import datetime, timedelta, timezone
from pathlib import Path
from typing import cast
from unittest.mock import Mock

import pytest
import pytest_asyncio
from starlette.middleware import Middleware
from starlette.middleware.sessions import SessionMiddleware

from restrict.compiler import RestrictCompiler
from restrict.compiler.compiled import AppResource, FieldSpec
from restrict.compiler.resolver import FileResolver
from restrict.resources.starlette import (
    Global,
    Session,
)
from restrict.resources.starlette import (
    restrict_module as starlette_module,
    MiddlewareSpec,
)
from restrict.types.time import restrict_module as time_module


@pytest_asyncio.fixture
async def bp():
    return Path(".").absolute()


@pytest_asyncio.fixture
async def p(bp):
    return bp / "root.restrict"


def mkcompiler(content):
    fields = {
        "resolve_file": Mock(return_value=content),
        "resolve_module": Mock(side_effect=[starlette_module, time_module]),
    }
    Resolver = type("MockResolver", (FileResolver,), fields)
    return RestrictCompiler(resolver=Resolver())


@pytest.mark.asyncio
async def test_can_extend_starlette_global(p, bp):
    content = """
    refer to </restrict/resources/starlette> as starlette

    override thing starlette.Global {
        data {
            created: boolean;
        }
        effects {
            create {
                created = true;
            }
        }
    }
    """
    compiler = mkcompiler(content)
    app = compiler.compile(p, bp)

    assert "/root.restrict/Global" in app.paths
    Res = app.get_resource("/root.restrict/Global")
    assert issubclass(Res, Global)  # type: ignore
    instance = Res._create({})

    key = instance._get_underlying_spec("key")
    assert key is not None
    assert isinstance(key, FieldSpec)
    assert "create" in key.effects
    assert key.effects["create"] is not None
    assert instance.key == "RESTRICT_GLOBAL"  # type: ignore

    created = instance._get_underlying_spec("created")
    assert created is not None
    assert isinstance(created, FieldSpec)
    assert "create" in created.effects
    assert created.effects["create"] is not None
    assert instance.created is True  # type: ignore

    assert instance._order == 1  # type: ignore

    repo = app.get_repository("/root.restrict/Global")(None, Res)  # type: ignore
    assert repo.get_middleware_dependencies() == []  # type: ignore

    app = Mock()
    app.state = Mock()
    del app.state.RESTRICT_GLOBAL
    scope = {"app": app}
    await repo.initialize_middleware(scope)  # type: ignore
    assert isinstance(app.state.RESTRICT_GLOBAL, Res)

    assert await repo.get_object(scope, None) == app.state.RESTRICT_GLOBAL
    await repo.save_object(scope, None, instance)
    assert app.state.RESTRICT_GLOBAL == instance


@pytest.mark.asyncio
async def test_can_extend_starlette_session(p, bp):
    os.environ["RESTRICT_SESSION_SECRET"] = "fairchild"
    content = """
    refer to </restrict/resources/starlette> as starlette
    use </restrict/types/time>

    override thing starlette.Session {
        data {
            created: timestamp;
            good_for: interval;
        }
        effects {
            create {
                created = now();
                good_for = ti`PT1H`;
            }
        }
    }
    """
    compiler = mkcompiler(content)
    app = compiler.compile(p, bp)

    assert "/root.restrict/Session" in app.paths
    Res = app.get_resource("/root.restrict/Session")
    Repo = app.get_repository("/root.restrict/Session")
    assert issubclass(Res, Session)  # type: ignore
    instance = cast(AppResource, Res._create({}))
    repo = Repo(None, Res)  # type: ignore
    instance_json = instance._to_json("all")

    key = instance._get_underlying_spec("key")
    assert key is not None
    assert isinstance(key, FieldSpec)
    assert "create" in key.effects
    assert key.effects["create"] is not None
    assert instance.key == "RESTRICT_SESSION"  # type: ignore

    created = instance._get_underlying_spec("created")
    assert created is not None
    assert isinstance(created, FieldSpec)
    assert "create" in created.effects
    assert created.effects["create"] is not None
    assert isinstance(instance.created, datetime)  # type: ignore

    good_for = instance._get_underlying_spec("good_for")
    assert good_for is not None
    assert isinstance(good_for, FieldSpec)
    assert "create" in good_for.effects
    assert good_for.effects["create"] is not None
    assert instance.good_for == timedelta(hours=1)  # type: ignore

    instance = cast(Session, instance)
    assert instance._order == 2
    assert len(repo.get_middleware_dependencies()) == 1  # type: ignore
    mw = repo.get_middleware_dependencies()[0]  # type: ignore
    assert isinstance(mw, MiddlewareSpec)
    assert mw.name == "SessionMiddleware"
    assert mw.module == "starlette.middleware.sessions"
    assert mw.kwargs == {"secret_key": instance.session_secret, "https_only": True}

    session = {}
    scope = {"session": session}
    await repo.initialize_middleware(scope)  # type: ignore
    assert len(session) == 0

    await repo.run_middleware(scope)  # type: ignore
    session_value = await repo.get_object(scope, None)
    assert isinstance(session_value, Session)
    await repo.save_object(scope, None, instance)
    assert session[instance.key] == instance_json
    assert (await repo.get_object(scope, None))._to_json("all") == instance_json  # type: ignore


def test_starlette_session_https_only_cookies_not_in_development(p, bp):
    os.environ["RESTRICT_SESSION_SECRET"] = "fairchild"
    os.environ["RESTRICT_ENV"] = "development"

    content = """
    refer to </restrict/resources/starlette> as starlette
    use </restrict/types/time>

    override thing starlette.Session {
        data {
            created: timestamp;
            good_for: interval;
        }
        effects {
            create {
                created = now();
                good_for = ti`PT1H`;
            }
        }
    }
    """
    compiler = mkcompiler(content)
    app = compiler.compile(p, bp)
    Res = app.get_resource("/root.restrict/Session")
    instance = cast(Session, Res._create({}))

    assert instance.https_only is False


def test_starlette_global_adds_global_self_to_parameters(p, bp):
    os.environ["RESTRICT_SESSION_SECRET"] = "fairchild"
    os.environ["RESTRICT_ENV"] = "development"

    content = """
    refer to </restrict/resources/starlette> as starlette
    use </restrict/types/time>

    thing Whatever {
        data {
            value = global.test;
            ovalue = global.when;
        }
    }

    override thing starlette.Global {
        data {
            test: boolean;
            when: timestamp;
        }
        effects {
            create {
                test = true;
                when = ts`2000-01-01T00:00:00Z`;
            }
        }
    }
    """
    compiler = mkcompiler(content)
    app = compiler.compile(p, bp)
    Res = app.get_resource("/root.restrict/Whatever")
    instance = Res._create({})

    assert instance.value is True  # type: ignore
    assert instance.ovalue == datetime(2000, 1, 1, tzinfo=timezone.utc)  # type: ignore
