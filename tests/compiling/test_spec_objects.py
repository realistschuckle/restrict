from collections.abc import Callable
from typing import Any

import pytest

from restrict.compiler.compiled import AppResource, CompSpec, FieldSpec, RelSpec
from restrict.compiler.exceptions.runtime import (
    ComputedFieldAssignmentError,
    RestrictRuntimeErrorGroup,
)
from restrict.compiler.types import EffName, LayeredMapping, RuleName
from restrict.resources.specs import (
    SpecEffect,
    SpecField,
    SpecResource,
    SpecRule,
)
from restrict.types.builtins import Boolean

effects: dict[EffName, Callable[[LayeredMapping], Any] | None] = {
    "create": None,
    "modify": None,
    "delete": None,
}
rules: dict[RuleName, Callable[[LayeredMapping], Any] | None] = {
    "create": lambda _: True,
    "modify": None,
    "delete": None,
    "list": None,
    "details": lambda _: True,
}


def test_comp_spec_as_resource_calls_to_schema_correctly():
    layer = LayeredMapping()
    inner_layer = layer.layer()
    resource_layer = layer.layer()

    def other_init(self):
        self.__dict__["_eval_order"] = []

    def inner_init(self):
        inner_layer["self"] = self
        self.__dict__["_eval_order"] = ["_spec_inner_data"]

    def resource_init(self):
        resource_layer["self"] = self
        self.__dict__["_eval_order"] = [
            "_spec_inner",
            "_spec_other",
            "_spec_data",
        ]

    Inner = type(
        "Inner",
        (AppResource,),
        {
            "__init__": inner_init,
            "__qualname__": "/bingo/root.restrict/Inner",
            "_spec_inner_data": FieldSpec(
                inner_layer.layer(),
                "Resource",
                "inner_data",
                Boolean(),
                lambda _: True,
                effects,
                rules,
            ),
        },
    )
    Other = type(
        "Other",
        (AppResource,),
        {"__init__": other_init, "__qualname__": "/bingo/root.restrict/Other"},
    )
    Resource = type(
        "Resource",
        (AppResource,),
        {
            "__init__": resource_init,
            "__qualname__": "/bingo/root.restrict/Resource",
            "_spec_data": CompSpec(
                resource_layer.layer(),
                "Resource",
                "data",
                Inner(),
                lambda _: None,
                effects,
                rules,
            ),
            "_spec_inner": RelSpec(
                resource_layer.layer(),
                "Resource",
                "inner",
                False,
                "$Inner",
                effects,
                rules,
            ),
            "_spec_other": RelSpec(
                resource_layer.layer(),
                "Resource",
                "other",
                False,
                "$Other",
                effects,
                rules,
            ),
        },
    )
    layer["$Other"] = Other
    layer["$Inner"] = Inner
    layer["$Resource"] = Resource

    assert Resource()._to_schema("details") == {
        "$defs": {
            "bingo": {
                "root.restrict": {
                    "Inner": {
                        "properties": {
                            "inner_data": {"readOnly": False, "type": "boolean"}
                        },
                        "required": ["inner_data"],
                        "type": "object",
                    },
                    "Other": {
                        "properties": {},
                        "required": [],
                        "type": "object",
                    },
                }
            }
        },
        "$id": "/bingo/root.restrict/Resource",
        "$schema": "https://json-schema.org/draft/2020-12/schema",
        "properties": {
            "data": {"$ref": "#/$defs/bingo/root.restrict/Inner", "readOnly": True},
            "inner": {"$ref": "#/$defs/bingo/root.restrict/Inner"},
            "other": {"$ref": "#/$defs/bingo/root.restrict/Other"},
        },
        "required": ["inner", "other", "data"],
        "type": "object",
    }


def test_spec_field():
    field = SpecField(Boolean(), lambda _: 10, True)

    assert field.res == Boolean()
    assert field.compiled(LayeredMapping()) == 10
    assert field.is_computed
    with pytest.raises(NotImplementedError):
        field.res = Boolean()


def test_spec_effect():
    effect = SpecEffect(lambda _: 10, Boolean())

    assert effect.compiled(LayeredMapping()) == 10
    assert effect.res == Boolean()
    with pytest.raises(NotImplementedError):
        effect.res = Boolean()


def test_spec_rule():
    rule = SpecRule(lambda _: False)

    assert rule.compiled(LayeredMapping()) is False


def test_spec_resource():
    layer = LayeredMapping()
    specs = [
        FieldSpec(
            layer.layer(),
            "Foo",
            "field",
            Boolean(),
            lambda _: True,
            {},
            {
                "create": lambda d: d["boop"],
                "modify": None,
                "delete": None,
                "list": None,
                "details": None,
            },
        )
    ]
    specs = {"field": SpecField(Boolean(), lambda _: True, False)}
    spec_effects = {"field": SpecResource._field_effects({})}
    spec_rules = {
        "field": SpecResource._field_security({"create": lambda d: d["boop"]})
    }
    instance = type(
        "Foo",
        (SpecResource,),
        {
            "get_global_names": lambda: [],
            "specs": specs,
            "spec_effects": spec_effects,
            "spec_rules": spec_rules,
            "field_order": [],
        },
    )()

    assert instance.name == "Foo"
    assert instance.compiled_name() == "<compiled>/Foo"
    assert instance.resolve_path(["a"]) is None
    assert instance.to_schema("create") == {}

    assert "field" in instance.get_rules()["create"]
    field = instance.get_rules()["create"]["field"]
    assert field is not None
    assert field.compiled is not None
    assert field.compiled(layer.layer({"boop": 10})) == 10


def test_app_resource_hydrate_and_alter():
    layer = LayeredMapping()
    inner_layer = layer.layer()
    resource_layer = layer.layer()

    def inner_init(self):
        inner_layer["self"] = self
        self.__dict__["_eval_order"] = ["_spec_inner_data"]

    def resource_init(self):
        resource_layer["self"] = self
        self.__dict__["_eval_order"] = ["_spec_data", "_spec_inner"]

    Inner = type(
        "Inner",
        (AppResource,),
        {
            "__init__": inner_init,
            "__qualname__": "$Inner",
            "_spec_inner_data": FieldSpec(
                inner_layer.layer(),
                "Resource",
                "inner_data",
                Boolean(),
                lambda _: True,
                effects,
                rules,
            ),
        },
    )
    Resource = type(
        "Resource",
        (AppResource,),
        {
            "__init__": resource_init,
            "__qualname__": "$Resource",
            "_spec_inner": RelSpec(
                resource_layer.layer(),
                "Resource",
                "inner",
                False,
                "$Inner",
                effects,
                rules,
            ),
            "_spec_data": FieldSpec(
                resource_layer.layer(),
                "Resource",
                "data",
                Boolean(),
                lambda _: True,
                effects,
                rules,
            ),
        },
    )
    layer["$Inner"] = Inner
    layer["$Resource"] = Resource

    instance = Resource._hydrate({"data": True, "inner": {"inner_data": False}})

    assert instance._to_json("all") == {"data": True, "inner": {"inner_data": False}}

    assert instance._alter(None)._to_json("all") == {
        "data": True,
        "inner": {"inner_data": False},
    }


def boom(_):
    error = ComputedFieldAssignmentError("BOOM", "BOOM")
    raise RestrictRuntimeErrorGroup([error])


def test_handling_error_groups_while_running_constraints():
    layer = LayeredMapping()
    init_layer = layer.layer()
    field_layer = init_layer.layer()

    def resource_init(self):
        init_layer["self"] = self

    Resource = type(
        "Resource",
        (AppResource,),
        {
            "__init__": resource_init,
            "__qualname__": "/root.restrict/Resource",
            "_spec_data": FieldSpec(
                field_layer, "Resource", "data", Boolean(), boom, effects, rules
            ),
            "_eval_order": [
                "_spec_data",
            ],
        },
    )

    with pytest.raises(RestrictRuntimeErrorGroup) as exc_info:
        Resource._create({"data": True})

    assert len(exc_info.value.errors) == 1
    error = exc_info.value.errors[0]
    assert isinstance(error, ComputedFieldAssignmentError)
    assert error.is_fatal is False
    assert error.name == "BOOM"
    assert error.res == "BOOM"


def test_app_resource_captures_errors_in_sub_resource_constraints():
    layer = LayeredMapping()
    inner_layer = layer.layer()
    inner_field_layer = inner_layer.layer()

    resource_layer = layer.layer()
    resource_field_layer = resource_layer.layer()

    def inner_init(self):
        inner_layer["self"] = self

    def resource_init(self):
        resource_layer["self"] = self

    Inner = type(
        "Inner",
        (AppResource,),
        {
            "__init__": inner_init,
            "__qualname__": "$Inner",
            "_eval_order": ["_spec_inner_data"],
            "_spec_inner_data": FieldSpec(
                inner_field_layer,
                "Resource",
                "inner_data",
                Boolean(),
                boom,
                effects,
                rules,
            ),
        },
    )

    Resource = type(
        "Resource",
        (AppResource,),
        {
            "__init__": resource_init,
            "__qualname__": "$Resource",
            "_spec_inner": RelSpec(
                resource_field_layer,
                "Resource",
                "inner",
                False,
                "$Inner",
                effects,
                rules,
            ),
            "_spec_data": FieldSpec(
                resource_field_layer,
                "Resource",
                "data",
                Boolean(),
                lambda _: True,
                effects,
                rules,
            ),
            "_eval_order": [
                "_spec_data",
                "_spec_inner",
            ],
        },
    )
    layer["$Inner"] = Inner
    layer["$Resource"] = Resource

    with pytest.raises(RestrictRuntimeErrorGroup) as exc_info:
        Resource._create({"data": True, "inner": {"inner_data": False}})

    assert len(exc_info.value.errors) == 1
    error = exc_info.value.errors[0]
    assert isinstance(error, ComputedFieldAssignmentError)
    assert error.is_fatal is False
    assert error.name == "BOOM"
    assert error.res == "BOOM"


def test_unknown_property_names_do_nothing():
    layer = LayeredMapping()

    def resource_init(self):
        context = layer.layer({"self": self})
        field_layer = context.layer()
        field_layer = context.layer()
        self.__dict__["_spec_data"] = FieldSpec(
            field_layer, "Resource", "data", Boolean(), lambda _: True, effects, rules
        )
        self.__dict__["_eval_order"] = ["_spec_data"]

    Resource = type(
        "Resource",
        (AppResource,),
        {"__init__": resource_init, "__qualname__": "/root.restrict/Resource"},
    )

    Resource._create({"data": True, "unknown": 1})
