import os
from pathlib import Path

import pytest

from restrict.compiler.resolver import (
    FileResolutionError,
    ModuleResolutionError,
    RestrictFileResolver,
    Module,
)

restrict_module = Module({}, {}, {})


@pytest.fixture(scope="module")
def resolver() -> RestrictFileResolver:
    return RestrictFileResolver()


def test_file_path_must_be_absolute(resolver: RestrictFileResolver):
    path = Path("./this/is/relative.restrict")

    with pytest.raises(FileResolutionError) as exc_info:
        resolver.resolve_file(path)

    exc = exc_info.value
    assert exc.path == path
    assert exc.reason == "not an absolute path"
    assert exc.cause is None


def test_file_path_must_exist(resolver: RestrictFileResolver):
    path = Path("/does/not/exist").resolve()

    with pytest.raises(FileResolutionError) as exc_info:
        resolver.resolve_file(path)

    exc = exc_info.value
    assert exc.path == path
    assert exc.reason == "does not exist"
    assert isinstance(exc.cause, FileNotFoundError)


def test_file_path_must_not_be_directory(resolver: RestrictFileResolver):
    path = Path(os.path.expanduser("~"))

    with pytest.raises(FileResolutionError) as exc_info:
        resolver.resolve_file(path)

    exc = exc_info.value
    assert exc.path == path
    assert exc.reason == "could not access"
    assert isinstance(exc.cause, OSError)


def test_returns_file_content_when_it_exists(resolver: RestrictFileResolver):
    with open(__file__, "r") as f:
        expected = f.read()
    content = resolver.resolve_file(Path(__file__))

    assert content == expected


def test_module_posix_path_must_start_with_slash(
    resolver: RestrictFileResolver,
):
    path = Path("not/absolute")

    with pytest.raises(ModuleResolutionError) as exc_info:
        resolver.resolve_module(path)

    exc = exc_info.value
    assert exc.path == path
    assert exc.mod_path == ""
    assert exc.reason == "not an absolute POSIX path"
    assert exc.cause is None


def test_module_path_must_exist(resolver: RestrictFileResolver):
    path = Path("/does/not/exist")

    with pytest.raises(ModuleResolutionError) as exc_info:
        resolver.resolve_module(path)

    exc = exc_info.value
    assert exc.path == path
    assert exc.mod_path == "does.not.exist"
    assert exc.reason == "module cannot be loaded"
    assert isinstance(exc.cause, ModuleNotFoundError)


def test_module_must_have_restrict_module_export(
    resolver: RestrictFileResolver,
):
    path = Path("/unittest/mock")

    with pytest.raises(ModuleResolutionError) as exc_info:
        resolver.resolve_module(path)

    exc = exc_info.value
    assert exc.path == path
    assert exc.mod_path == "unittest.mock"
    assert exc.reason == "no restrict_module property"
    assert isinstance(exc.cause, AttributeError)


def test_restrict_module_must_be_instance_of_restrictmodule(
    resolver: RestrictFileResolver,
):
    mod_path = f"/{__name__.replace('.', '/')}"
    path = Path(mod_path).parent
    parent_mod = path.as_posix()[1:].replace("/", ".")

    with pytest.raises(ModuleResolutionError) as exc_info:
        resolver.resolve_module(path)

    exc = exc_info.value
    assert exc.path == path
    assert exc.mod_path == parent_mod
    assert exc.reason == "not a restrict.compiler.types.Module"
    assert exc.cause is None


def test_returns_module_when_it_exists(resolver: RestrictFileResolver):
    mod_path = f"/{__name__.replace('.', '/')}"
    content = resolver.resolve_module(Path(mod_path))

    assert content == restrict_module
