from pathlib import Path

import pytest

from restrict.compiler import RestrictCompiler
from restrict.compiler.compiled import App
from restrict.compiler.exceptions.runtime import (
    RestrictRuntimeErrorGroup,
    SecurityPreventedFieldAssignmentError,
)


@pytest.fixture
def app():
    base_path = Path(".").absolute()
    root = base_path / "examples" / "counter.restrict"
    compiler = RestrictCompiler()

    return compiler.compile(root, base_path)


@pytest.fixture
def Counter(app: App):
    return app.get_resource("/examples/counter.restrict/Counter")


def test_compiler_stores_app_resource_in_app(app):
    assert app.get_resource("/examples/counter.restrict/Counter") is not None


def test_counter_has_value_1_on_create(Counter):
    instance = Counter._create({})

    assert instance.value_ == 1


def test_counter_cannot_set_value(Counter):
    instance = Counter._create({})

    with pytest.raises(SecurityPreventedFieldAssignmentError) as exc_info:
        instance.value_ = 10

    assert exc_info.value.is_fatal is False
    assert exc_info.value.name == "value_"


def test_counter_cannot_create_with_value(Counter):
    with pytest.raises(RestrictRuntimeErrorGroup):
        Counter._create({"value_": 100})


def test_counter_can_update_on_modify(Counter):
    instance1 = Counter._create({})
    instance2 = Counter._create({})

    instance1._alter_field()

    assert instance1.value_ == 2
    assert instance2.value_ == 1

    instance1._alter_field()

    assert instance2.value_ == 1
    assert instance1.value_ == 3
