import inspect
from pathlib import Path
from unittest.mock import Mock, call

import pytest

from restrict.compiler import CompilationErrors, RestrictCompiler
from restrict.compiler.ast import (
    DataComputedField,
    File,
    Import,
    PipedExprList,
    Rel,
    Res,
)
from restrict.compiler.exceptions.compile import InvalidFilePathError
from restrict.compiler.parsers import PlyRestrictParser, RestrictParser
from restrict.compiler.resolver import FileResolver
from restrict.compiler.types import Module

from .test_resolver import restrict_module


def make_resolver(
    file_content: str | Exception | list[str | Exception],
    module_content: (Module | Exception | list[Module | Exception]),
) -> FileResolver:
    if (
        isinstance(file_content, list)
        or isinstance(file_content, Exception)
        or (inspect.isclass(file_content) and issubclass(file_content, Exception))
    ):
        resolve_file = Mock(side_effect=file_content)
    else:
        resolve_file = Mock(return_value=file_content)
    if (
        isinstance(module_content, list)
        or isinstance(module_content, Exception)
        or (inspect.isclass(module_content) and issubclass(module_content, Exception))
    ):
        resolve_module = Mock(side_effect=module_content)
    else:
        resolve_module = Mock(return_value=module_content)
    Resolver = type(
        "MockFileResolver",
        (FileResolver,),
        {"resolve_file": resolve_file, "resolve_module": resolve_module},
    )
    return Resolver()


def make_compiler(
    file_content: str | Exception | list[str | Exception],
    module_content: (Module | Exception | list[Module | Exception]) = restrict_module,
    ast_or_parser: File | list[File] | RestrictParser | None = None,
) -> tuple[RestrictCompiler, FileResolver, RestrictParser]:
    resolver = make_resolver(file_content, module_content)
    if ast_or_parser is None:
        parser = PlyRestrictParser()
    elif isinstance(ast_or_parser, RestrictParser):
        parser = ast_or_parser
    elif isinstance(ast_or_parser, list):
        parser = Mock()
        parser.parse = Mock(side_effect=ast_or_parser)
        parser.errors = []
    else:
        parser = Mock()
        parser.parse = Mock(return_value=ast_or_parser)
        parser.errors = []
    compiler = RestrictCompiler(resolver=resolver, parser=parser)
    return compiler, resolver, parser


@pytest.fixture
def base_path():
    return Path(".").absolute()


def test_root_path_must_be_relative_to_base_path(base_path):
    compiler, resolver, _ = make_compiler("Hello", ast_or_parser=[])

    with pytest.raises(InvalidFilePathError) as exc_info:
        compiler.compile(Path("/somewhere-else.restrict"), base_path)

    assert exc_info.value.path == Path("/somewhere-else.restrict")
    assert exc_info.value.base_path == base_path


def test_calls_resolve_from_file_resolver_and_passes_error(base_path):
    error = ValueError()
    compiler, resolver, _ = make_compiler(error)
    p = Path("./relative.restrict")
    with pytest.raises(InvalidFilePathError) as exc_info:
        compiler.compile(p, base_path)

    assert exc_info.value.path == p
    assert exc_info.value.base_path == base_path
    resolver.resolve_file.assert_not_called()  # type: ignore


def test_calls_parse_with_content_from_resolver(base_path):
    p = base_path / "content.restrict"
    parse = Mock(return_value=File(Path("/content.restrict"), [], []))
    errors = Mock(return_value=[])
    parser = type(
        "MockParser",
        (RestrictParser,),
        {"parse": parse, "errors": property(errors)},
    )()
    compiler, _, _ = make_compiler("this is file content", ast_or_parser=parser)  # type: ignore

    compiler.compile(p, base_path)

    parse.assert_called_once_with("this is file content", Path("/content.restrict"))


def test_raises_errors_from_parsing(base_path):
    expected = (Exception(), Exception())
    p = base_path / "content.restrict"
    parse = Mock(return_value=None)
    errors = Mock(return_value=expected)
    parser = type(
        "MockParser",
        (RestrictParser,),
        {"parse": parse, "errors": property(errors)},
    )()
    compiler, _, _ = make_compiler("this is file content", ast_or_parser=parser)  # type: ignore

    with pytest.raises(CompilationErrors) as exc_info:
        compiler.compile(p, base_path)

    assert exc_info.value.message == str(p)
    assert exc_info.value.exceptions == expected


def test_resolves_files_fifo_only_once(base_path):
    expected = [
        call(base_path / "root.restrict"),
        call(base_path / "one.restrict"),
        call(base_path / "two.restrict"),
        call(base_path / "three.restrict"),
    ]
    I = lambda p: Import(Path(p), "")  # noqa
    ast = [
        File(Path("/root.restrict"), [I("one"), I("two")], []),
        File(Path("/one.restrict"), [I("three"), I("two")], []),
        File(Path("/two.restrict"), [I("three")], []),
        File(Path("/three.restrict"), [], []),
    ]
    compiler, resolver, _ = make_compiler("", ast_or_parser=ast)

    compiler.compile(base_path / "root.restrict", base_path)

    resolver.resolve_file.assert_has_calls(expected)  # type: ignore


def test_resolves_modules_fifo_only_once(base_path):
    expected = [
        call(Path("/eins")),
        call(Path("/zwei")),
        call(Path("/drei")),
    ]
    I = lambda p: Import(Path(p), "")  # noqa
    ast = [
        File(base_path / "root.restrict", [I("one"), I("two")], []),
        File(base_path / "one.restrict", [I("three"), I("two"), I("/eins")], []),
        File(base_path / "two.restrict", [I("three"), I("/zwei"), I("/eins")], []),
        File(base_path / "three.restrict", [I("/eins"), I("/drei")], []),
    ]
    compiler, resolver, _ = make_compiler("", ast_or_parser=ast)

    compiler.compile(base_path / "root.restrict", base_path)

    resolver.resolve_module.assert_has_calls(expected)  # type: ignore


def test_compiler_raises_exception_for_errors_in_visitors(base_path):
    dcf = DataComputedField(PipedExprList())
    rel = Rel("", "", False, False, None, None, "")
    res = Res("", "", "", False, {"a": dcf}, {"a": rel}, {}, {}, {})
    ast = [
        File(base_path / "root.restrict", [], [res]),
    ]
    compiler, resolver, _ = make_compiler("", ast_or_parser=ast)

    with pytest.raises(CompilationErrors):
        compiler.compile(base_path / "root.restrict", base_path)
