from pathlib import Path
from unittest.mock import Mock

import pytest

import restrict.compiler.ast as ast
from restrict.compiler import RestrictCompiler
from restrict.compiler.types import ModuleDictionary
from restrict.types.numeric import restrict_module

from ..util import dump


@pytest.fixture
def examples_dir():
    return Path(__file__).parent.parent / "examples"


@pytest.fixture
def compiler():
    return RestrictCompiler()


@pytest.mark.skip("DO NOT REMOVE: Just a test to see dumped out stuff")
def test_counter_thing():
    mods = ModuleDictionary()
    mods[Path("/restrict/types/numeric")] = restrict_module
    ofile = ast.File(
        Path("/two.restrict"),
        [],
        [ast.Res("thing", "Unused", "", False, {}, {}, {}, {}, {})],
    )
    file = ast.File(
        Path("/one.restrict"),
        [ast.Import(Path("/restrict/types/numeric"), "")],
        [
            ast.Res("thing", "Bob", "", False, {}, {}, {}, {}, {}),
            ast.Res(
                "thing",
                "Counter",
                "",
                False,
                {
                    "value_3": ast.DataComputedField(
                        ast.PipedExprList(
                            [
                                ast.Ref(["value_2"]),
                                ast.Func(
                                    "add",
                                    [],
                                    [],
                                    [ast.PipedExprList([ast.Lit(2)])],
                                ),
                            ]
                        )
                    ),
                    "value_": ast.DataConstrainedField(
                        "integer",
                        "",
                        False,
                        False,
                        None,
                        ast.PipedExprList(
                            [
                                ast.Value(),
                                ast.Func(
                                    "gte",
                                    [],
                                    [],
                                    [ast.PipedExprList([ast.Lit(0)])],
                                ),
                            ]
                        ),
                    ),
                    "value_2": ast.DataComputedField(
                        ast.PipedExprList(  # type: ignore
                            [
                                ast.Ref(["value_"]),
                                ast.Func(
                                    "add",
                                    [],
                                    [],
                                    [
                                        ast.PipedExprList([ast.Lit(1)]),
                                    ],
                                ),
                            ]
                        ),
                    ),
                },
                {
                    "rel_1": ast.Rel("Bob", "", False, False, None, None, ""),
                },
                {  # type: ignore
                    "create": {
                        "value_": ast.EffectsComputedField(
                            ast.PipedExprList([ast.Lit(1)])
                        ),
                    },
                    "modify": {
                        "value_": ast.EffectsComputedField(
                            ast.PipedExprList(
                                [
                                    ast.Ref(["value_"]),
                                    ast.Func(
                                        "add",
                                        [],
                                        [],
                                        [ast.PipedExprList([ast.Lit(1)])],
                                    ),
                                ]
                            )
                        )
                    },
                },
                {  # type: ignore
                    "details": ast.SecurityConstraintField(
                        ast.PipedExprList([ast.Lit(True)]),
                    ),
                    "modify": {
                        "value_": ast.SecurityConstraintField(
                            ast.PipedExprList([ast.Lit(False)]),
                        ),
                    },
                },
                {  # type: ignore
                    "list": ast.Transition(
                        "Thingo",
                        "",
                        "",
                        "",
                        {
                            "a": ast.TransitionComputedField(
                                ast.PipedExprList([ast.Lit(4)])
                            )
                        },
                    )
                },
            ),
        ],
    )
    for r in file.resources:
        r.file = file
    for r in ofile.resources:
        r.file = ofile
    p = Path("/one.restrict")
    other = Path("/two.restrict")
    asts = {p: file, other: ofile}
    compiler = RestrictCompiler()
    errors = []
    compiler._run_steps(p, asts, mods, {}, errors, [p, other])
    from pprint import pprint

    print("ERRORS")
    pprint(errors)
    print("\nROOT")
    dump(asts[p])
    raise ValueError()


@pytest.mark.skip("DO NOT REMOVE: Just a test to see dumped out stuff")
def test_dump_from_source():
    content = """
        thing Thing {
            data {
                x: boolean;
            }
        }
    """
    root = Path("/root.restrict")
    resolver = Mock()
    resolver.resolve_file = Mock(return_value=content)
    parser = RestrictCompiler(resolver=resolver)
    asts, mod_paths, _, so = parser._parse_file_tree(root)
    mods = parser._load_extensions(mod_paths)
    parser._run_steps(root, asts, mods, {}, [], so)

    dump(asts[root])
    assert False
