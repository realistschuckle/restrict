from collections.abc import Sequence
from pathlib import Path
from typing import override

import pytest

from restrict.compiler.ast import File, Import
from restrict.compiler.compiler import RestrictCompiler
from restrict.compiler.exceptions.compile import RestrictError
from restrict.compiler.parsers import RestrictParser
from restrict.compiler.resolver import FileResolver
from restrict.compiler.types import Module


class StubResolver(FileResolver):
    def __init__(
        self,
        content_by_path: dict[Path, str],
        modules_by_path: dict[Path, Module],
    ):
        self.content_by_path = content_by_path
        self.modules_by_path = modules_by_path
        self.file_calls = []
        self.module_calls = []

    @override
    def resolve_file(self, path: Path) -> str:
        self.file_calls.append(path)
        return self.content_by_path[path]

    @override
    def resolve_module(self, path: Path) -> Module:
        self.module_calls.append(path)
        return self.modules_by_path[path]


class StubParser(RestrictParser):
    def __init__(self, files: dict[Path, File]):
        self.files = files
        self.parse_calls = []

    @override
    def parse(self, content: str, source: Path) -> File | None:
        self.parse_calls.append((content, source))
        return self.files[source]

    @property
    @override
    def errors(self) -> Sequence[RestrictError]:
        return []


@pytest.fixture
def resolver(base_path):
    contents = {
        base_path / "root.restrict": "root.restrict",
        base_path / "lib/source.restrict": "lib/source.restrict",
        base_path / "lib/other.restrict": "lib/other.restrict",
    }
    modules = {
        Path("/restrict/module"): Module({}, {}, {}),
    }

    return StubResolver(contents, modules)


@pytest.fixture
def parser():
    files = {
        Path("/root.restrict"): File(
            Path("/root.restrict"),
            [
                Import(Path("/restrict/module"), ""),
                Import(Path("lib/source.restrict"), ""),
                Import(Path("lib/other"), ""),
            ],
            [],
        ),
        Path("/lib/source.restrict"): File(Path("/lib/source.restrict"), [], []),
        Path("/lib/other.restrict"): File(Path("/lib/other.restrict"), [], []),
    }
    return StubParser(files)


@pytest.fixture
def compiler(resolver, parser):
    return RestrictCompiler(resolver=resolver, parser=parser)


@pytest.fixture
def base_path():
    return Path(".").absolute()


def test_parse_file_tree_returns_ast_with_path_absolute_from_base_path(
    compiler: RestrictCompiler,
    base_path: Path,
):
    root = base_path / "root.restrict"

    asts, *_ = compiler._parse_file_tree(root, base_path)

    assert Path("/root.restrict") in asts
    assert Path("/lib/source.restrict") in asts
    assert Path("/lib/other.restrict") in asts
    assert len(asts) == 3
    assert asts[Path("/root.restrict")].path == Path("/root.restrict")
    assert asts[Path("/lib/source.restrict")].path == Path("/lib/source.restrict")
    assert asts[Path("/lib/other.restrict")].path == Path("/lib/other.restrict")


def test_parse_file_tree_returns_mods_with_absolute_paths(
    compiler: RestrictCompiler,
    base_path: Path,
):
    root = base_path / "root.restrict"

    _, mods, *_ = compiler._parse_file_tree(root, base_path)

    assert Path("/restrict/module") in mods
    assert len(mods) == 1


def test_parse_file_tree_returns_sources_in_order(
    compiler: RestrictCompiler,
    base_path: Path,
):
    root = base_path / "root.restrict"

    _, _, _, source_order = compiler._parse_file_tree(root, base_path)

    assert source_order == [
        Path("/root.restrict"),
        Path("/lib/source.restrict"),
        Path("/lib/other.restrict"),
    ]
