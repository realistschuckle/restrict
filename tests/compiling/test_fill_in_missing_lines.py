from ..visitors.fake_module import OverrideMe


def test_restrict_module_resource_is_not_used_by_default():
    ome = OverrideMe()

    assert ome.used is False
