from pathlib import Path
from typing import cast

from restrict.compiler.ast import (
    DataComputedField,
    File,
    Func,
    PipedExprList,
    Ref,
    Res,
)
from restrict.compiler.types import (
    FuncParam,
    FuncParamRef,
)
from restrict.compiler.types import ModuleDictionary as RMD
from restrict.compiler.transformers import (
    ReplaceRefsWithFuncParamRefsTransformer,
)


def test_replaces_refs_with_func_param_refs():
    add = Func("", [], [], [PipedExprList([Ref(["y", "z"])])])
    func = Func(
        "",
        [],
        [FuncParam("x"), FuncParam("y")],
        [PipedExprList([Ref(["x"]), add])],
    )
    field = DataComputedField(PipedExprList([func]))  # type: ignore
    resource = Res("", "", "", False, {"a": field}, {}, {}, {}, {}, used=True)
    file = File(Path("."), [], [resource])
    visitor = ReplaceRefsWithFuncParamRefsTransformer({}, RMD(), {}, Path(""))

    ast, glob = visitor.visit_file(file)
    result = cast(Func, ast.resources[0].fields["a"].func[0])

    assert type(result.args[0][0]) is FuncParamRef
    assert result.args[0][0].path == []
    assert result.args[0][0].param == func.params[0]

    assert type(result.args[0][1]) is Func
    assert type(result.args[0][1].args[0][0]) is FuncParamRef
    assert result.args[0][1].args[0][0].path == ["z"]
    assert result.args[0][1].args[0][0].param == func.params[1]


def test_ignores_values_not_in_parameters():
    add = Func("", [], [], [PipedExprList([Ref(["z", "a"])])])
    func = Func(
        "",
        [],
        [FuncParam("x"), FuncParam("y")],
        [PipedExprList([Ref(["z"]), add])],
    )
    field = DataComputedField(PipedExprList([func]))  # type: ignore
    resource = Res("", "", "", False, {"a": field}, {}, {}, {}, {}, used=True)
    file = File(Path("."), [], [resource])
    visitor = ReplaceRefsWithFuncParamRefsTransformer({}, RMD(), {}, Path(""))

    ast, glob = visitor.visit_file(file)
    result = cast(Func, ast.resources[0].fields["a"].func[0])

    assert type(result.args[0][0]) is Ref
    assert result.args[0][0].path == ["z"]

    assert type(result.args[0][1]) is Func
    assert type(result.args[0][1].args[0][0]) is Ref
    assert result.args[0][1].args[0][0].path == ["z", "a"]
