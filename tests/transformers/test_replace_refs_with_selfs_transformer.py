from pathlib import Path

from restrict.compiler.ast import (
    Create,
    DataComputedField,
    DataConstrainedField,
    EffectsComputedField,
    EffEntries,
    EffName,
    File,
    Func,
    PipedExprList,
    Ref,
    Rel,
    Res,
    Rules,
    SecurityConstraintField,
    Self,
    Value,
)

from restrict.compiler.types import ModuleDictionary
from restrict.compiler.transformers import ReplaceRefsWithSelfsTransformer


def vis():
    return ReplaceRefsWithSelfsTransformer({}, ModuleDictionary(), {}, Path(""))


def _(*args):
    return PipedExprList(args)


def test_replaces_ref_in_effects():
    data: dict[str, DataConstrainedField | DataComputedField] = {
        "a": DataConstrainedField("int", "", False, False, None, _()),
        "b": DataConstrainedField("int", "", False, False, None, _()),
    }
    rels = {
        "c": Rel("Thing", "", False, False, None, None, ""),
    }
    effects: dict[EffName, EffEntries] = {
        "create": {"a": EffectsComputedField(_(Ref(["b"])))},
        "modify": {
            "b": EffectsComputedField(
                _(
                    Value(),
                    Func("add", [], [], [_(Ref(["a"]))]),
                    Func("add", [], [], [_(Ref(["b"]))]),
                    Func("add", [], [], [_(Ref(["c", "b"]))]),
                    Func("add", [], [], [_(Ref(["d"]))]),
                )
            ),
            "c": EffectsComputedField(
                _(
                    Create(
                        "Thing",
                        "",
                        {
                            "a": EffectsComputedField(_(Ref(["a"]))),
                            "b": EffectsComputedField(_(Ref(["b"]))),
                        },
                    )
                )
            ),
        },
    }
    res = Res("", "", "", False, data, rels, effects, {}, {})
    file = File(Path(""), [], [res])
    visitor = vis()

    result, glob = visitor.visit_file(file)
    effects = result.resources[0].effects

    effects["create"]["a"] = EffectsComputedField(_(Self(["b"])))
    effects["modify"]["b"] = EffectsComputedField(
        _(
            Value(),
            Func("add", [], [], [_(Self(["a"]))]),
            Func("add", [], [], [_(Self(["b"]))]),
            Func("add", [], [], [_(Self(["c", "b"]))]),
            Func("add", [], [], [_(Ref(["d"]))]),
        )
    )
    effects["modify"]["c"] = EffectsComputedField(
        _(
            Create(
                "Thing",
                "",
                {
                    "a": EffectsComputedField(_(Self(["a"]))),
                    "b": EffectsComputedField(_(Self(["b"]))),
                },
            )
        )
    )


def test_replaces_ref_in_security():
    data: dict[str, DataConstrainedField | DataComputedField] = {
        "a": DataConstrainedField("int", "", False, False, None, _()),
        "b": DataConstrainedField("int", "", False, False, None, _()),
        "d": DataConstrainedField("int", "", False, False, None, _()),
    }
    rels = {
        "c": Rel("Thing", "", False, False, None, None, ""),
    }
    security: Rules = {
        "list": SecurityConstraintField(
            _(Ref(["a"]), Func("", [], [], [_(Ref(["b"]))])),
        ),
        "details": {
            "c": SecurityConstraintField(
                _(Ref(["c", "a"]), Func("", [], [], [_(Ref(["c", "b"]))])),
            ),
            "d": SecurityConstraintField(
                _(Ref(["d"]), Func("", [], [], [_(Ref(["e"]))])),
            ),
        },
    }
    res = Res("", "", "", False, data, rels, {}, security, {})
    file = File(Path(""), [], [res])
    visitor = vis()

    result, glob = visitor.visit_file(file)
    security_ = result.resources[0].rules

    assert security_["list"] == SecurityConstraintField(
        _(Self(["a"]), Func("", [], [], [_(Self(["b"]))])),
    )
    assert security_["details"]["c"] == SecurityConstraintField(  # type: ignore
        _(Self(["c", "a"]), Func("", [], [], [_(Self(["c", "b"]))])),
    )
    assert security_["details"]["d"] == SecurityConstraintField(  # type: ignore
        _(Self(["d"]), Func("", [], [], [_(Ref(["e"]))])),
    )
