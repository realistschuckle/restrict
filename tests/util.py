import restrict.compiler.ast as ast
from restrict.compiler.exceptions.compile import InvalidFuncCallError
import restrict.compiler.types as types


def spaces(indent, depth, offset=0):
    return " " * (indent * (depth + offset))


def path(node):
    return node.path.as_posix()


def p(*args, end="\n"):
    print(*args, sep="", end=end)


def dump(node, indent=2, depth=0):
    levels = [spaces(indent, depth, i) for i in range(4)]
    match type(node):
        case ast.File:
            p(f"File[{path(node)}](")
            for imp in node.imports:
                dump(imp, indent, depth + 1)
            for resource in node.resources:
                dump(resource, indent, depth + 1)
            p(")")
        case ast.Import:
            p(levels[0], f"Import[{path(node)}]({node.prefix})")
        case ast.Res:
            if node.prefix:
                name = f"{node.prefix}.{node.name}"
            else:
                name = node.name
            p(levels[0], f"Res[{node.type} {name}](")
            if node.override and node.base is not None:
                p(levels[1], f"base={node.base.bridge.compiled_name},")
            elif node.override:
                p(levels[1], "base=NONE")
            p(levels[1], f"used={node.used},")

            if len(node.fields) > 0:
                p(levels[1], "data {")
                for name, field in node.fields.items():
                    p(levels[2], name, end="")
                    dump(field, indent, depth + 2)
                p(levels[1], "}")
                print()

            if len(node.dnc) > 0:
                p(levels[1], "dnc {")
                for name, field in node.dnc.items():
                    p(levels[2], name, end="")
                    dump(field, indent, depth + 2)
                p(levels[1], "}")
                print()

            if len(node.effects) > 0:
                p(levels[1], "effects {")
                for eff_name, eff_section in node.effects.items():
                    p(levels[2], eff_name, " {")
                    for name, field in eff_section.items():
                        p(levels[3], name, ": ", end="")
                        dump(field, indent, depth + 3)
                    p(levels[2], "}")
                p(levels[1], "}")

            if len(node.rules) > 0:
                for sec_name, sec_section in node.rules.items():
                    p(levels[1], sec_name, end="")
                    if isinstance(sec_section, dict):
                        p(" {")
                        for name, tx_field in sec_section.items():
                            p(levels[2], name, end="")
                            dump(tx_field, indent, depth + 2)
                        p(levels[1], "}")
                    else:
                        dump(sec_section, indent, depth + 1)
            p(levels[0], ")")
        case ast.DataConstrainedField:
            if node.prefix:
                t = f"{node.prefix}.{node.type}"
            else:
                t = node.type
            if node.collection:
                t = f"{node.collection.__name__}[{t}]"
            res = "NONE"
            if node.res:
                res = node.res.name
            func_res = "NONE"
            if len(node.func) > 0:
                try:
                    func_res = node.func.treeify().resolve_type()
                except (InvalidFuncCallError, ValueError):
                    func_res = "ERROR"
            p(f": DataConstrainedField[{t}](")
            p(levels[1], "is_optional=", "YES" if node.is_optional else "NO,")
            p(levels[1], "is_unique=", "YES" if node.is_unique else "NO,")
            p(levels[1], f"res={res},")
            p(levels[1], f"func_res={func_res},")
            p(levels[1], f"compiled={'YES' if node.compiled else 'NO'},")
            p(levels[1], "[")
            for f in node.func:
                dump(f, indent, depth + 2)
            p(levels[1], "]")
            p(levels[0], ")")
        case ast.DataComputedField:
            func_res = "NONE" if node.func_res is None else node.func_res
            p(" = DataComputedField(")
            p(levels[1], f"func_res={func_res},")
            p(levels[1], f"compiled={'YES' if node.compiled else 'NO'},")
            p(levels[1], "[")
            for f in node.func:
                dump(f, indent, depth + 2)
            p(levels[1], "]")
            p(levels[0], ")")
        case ast.EffectsComputedField:
            func_res = "NONE"
            if len(node.func) > 0:
                try:
                    func_res = node.func.treeify().resolve_type()
                except (InvalidFuncCallError, ValueError):
                    func_res = "ERROR"
            p("EffectsComputedField(")
            p(levels[1], f"res={node.res or 'NONE'},")
            p(levels[1], f"func_res={func_res},")
            p(levels[1], f"compiled={'YES' if node.compiled else 'NO'},")
            p(levels[1], "[")
            for f in node.func:
                dump(f, indent, depth + 2)
            p(levels[1], "]")
            p(levels[0], ")")
        case ast.SecurityConstraintField:
            func_res = "NONE"
            if len(node.func) > 0:
                try:
                    func_res = node.func.treeify().resolve_type()
                except (InvalidFuncCallError, ValueError):
                    func_res = "ERROR"
            p(" = SecurityConstraintField(")
            p(levels[1], f"func_res={func_res},")
            p(levels[1], f"compiled={'YES' if node.compiled else 'NO'},")
            p(levels[1], "[")
            for f in node.func:
                dump(f, indent, depth + 2)
            p(levels[1], "]")
            p(levels[0], ")")
        case ast.Ref:
            path_res = "NONE"
            if node.path_res:
                path_res = [x.name for x in node.path_res]

            p(levels[0], "Ref(")
            p(levels[1], "path=", node.path)
            p(levels[1], "path_res=", path_res)
            p(levels[0], "),")
        case types.FuncParamRef:
            p(levels[0], f"Param({node.param.name}, {node.path})")
        case ast.Func:
            if node.prefix:
                name = f"{node.prefix}.{node.name}"
            else:
                name = node.name
            func_name = "NONE"
            if node.res:
                func_name = node.res.name
            p(
                levels[0],
                f"Func[{name}][{', '.join(p.name for p in node.params)}](",
            )
            p(levels[1], "res=", func_name, ",")
            p(levels[1], "[")
            for arg in node.args:
                p(levels[2], "[")
                for f in arg:
                    dump(f, indent, depth + 3)
                p(levels[2], "],")
            p(levels[1], "]")
            p(levels[0], "),")
        case ast.Lit:
            res = "NONE" if node.res is None else node.res
            p(levels[0], f"Lit({node.value}, res={res}),")
        case ast.Value:
            p(levels[0], "Value(),")
        case ast.Rel:
            if node.prefix:
                t = f"{node.prefix}.{node.type}"
            else:
                t = node.type
            if node.collection is not None:
                t = f"{node.collection.__name__}[{t}]"
            card = (0, "*")
            if node.cardinality:
                card = node.cardinality
            res = "NONE"
            if node.res:
                res = node.res.name
            dir = ""
            if len(node.dir) > 0:
                dir = f" <{node.dir}>"
            p(f": DataConstrainedField[{t}{dir}](")
            p(levels[1], "is_optional=", "YES" if node.is_optional else "NO,")
            p(levels[1], "is_unique=", "YES" if node.is_unique else "NO,")
            p(levels[1], f"cardinality=[{card[0]}, {card[1]}],")
            p(levels[1], f"res={res},")
            p(levels[0], ")")
        case ast.Self:
            res = "NONE" if node.res is None else node.res
            path_res = "NONE"
            if node.path_res:
                path_res = [x.name for x in node.path_res]

            p(levels[0], "Self(")
            p(levels[1], "path=", node.path)
            p(levels[1], "res=", res)
            p(levels[1], "path_res=", path_res)
            p(levels[0], "),")
