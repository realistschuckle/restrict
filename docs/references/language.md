# Resource File Grammar

White space such as spaces, tabs, and new lines, are ignored for RESTrict
Framework files but used as token delimiters.

Some terminal rules have regular expressions denoted by // delimiters for
brevity.

::::{div} full-width

```{code} bnf
<file> : <opt_imports> <opt_resources>

<opt_imports> : <use> <opt_imports>
              | <refer_to> <opt_imports>
              | ""


;; Imports
<use> : "use" "<" <path> ">"

<refer_to> : "refer" "to" "<" <path> ">" "as" <id>

<path> : /[\/a-z0-9]+/

<opt_resources> : <resource> <opt_resources>
                | ""



;; Resource declaration
<resource> : <opt_override> <type> <name> "{" <body> "}"

<opt_override> : "override" | ""

<type> : "party" | "place" | "thing" | "role" | "moment" | "interval"

<body> : <data> <dnc> <effects> <workflow> <security>



;; Data section
<data> : "data" "{" <field> <opt_fields> "}"

<opt_fields> : <field> <opt_fields>
             | ""

<field> : <id> ":" <id> ";"
        | <id> ":" <constrained_field>
        | <calculated_field>



;; DNC section
<dnc> : "dnc" "{" <rel> <opt_rels> "}"

<opt_rels> : <rel> <opt_rels>
           | ""

<rel> : <opt_decorator> <id> ":" <name> ";"

<opt_decorator> : "<next>" | "<previous>" | "<root>" | "<details>"



;; Effects section 
<effects> : "effects" "{" <create_effect> <modify_effect> <delete_effect> "}"

<create_effect> : "create" "{" <calculated_field> <opt_calculated_fields> "}"

<modify_effect> : "modify" "{" <calculated_field> <opt_calculated_fields> "}"

<delete_effect> : "delete" "{" <calculated_field> <opt_calculated_fields> "}"



;; Security section 
<security> : "security" "{" <list> <details> <create> <modify> <delete> "}"

<list> : "list" ":" <expr> <opt_expr_pipeline> ";"
       | "list" "{" <constrained_field> <opt_constrained_field> <opt_starred_field> "}"

<details> : "details" ":" <expr> <opt_expr_pipeline> ";"
          | "details" "{" <constrained_field> <opt_constrained_field> <opt_starred_field> "}"

<create> : "create" ":" <expr> <opt_expr_pipeline> ";"
         | "create" "{" <constrained_field> <opt_constrained_field> <opt_starred_field> "}"

<modify> : "modify" ":" <expr> <opt_expr_pipeline> ";"
         | "modify" "{" <constrained_field> <opt_constrained_field> <opt_starred_field> "}"

<delete> : "delete" ":" <expr> <opt_expr_pipeline> ";"
         | "delete" "{" <constrained_field> <opt_constrained_field> <opt_starred_field>"}"

<opt_starred_field> : "*" ":" <expr> <opt_expr_pipeline> ";"


;; Workflow section 
<workflow> : "workflow" "{" <transition> <opt_transitions> "}"

<opt_transitions> : <transition> <opt_transitions>
                  | ""

<transition> : <opt_entrypoint> <method> ";"
             | <opt_entrypoint> <method> "->" <name> "#" <method> ";"
             | <opt_entrypoint> <method> "->" <name> "#" <method> "{" <calculated_field> <opt_calculated_fields> "}"
             | <opt_alias> <method> "->" <name> "#" <method> ";"

<method> : "list" | "details" | "create" | "modify" | "delete"

<opt_entrypoint> : "<entrypoint>"
                 | ""



;; Calculated fields
<opt_calculated_fields> : <calculated_field> <opt_calculated_fields>
                        | ""

<calculated_field> : <id> "=" <ext_expr> <opt_ext_expr_pipeline> ";"

<ext_expr> : <expr>
           | <mut_method> <name> "{" <calculated_field> <opt_calculated_fields> "}"

<opt_ext_expr_pipeline> : "|>" <ext_expr> <opt_ext_expr_pipeline>

<mut_method> : "create"
             | "modify"



;; Constrained fields
<opt_constrained_field> : <constrained_field> <opt_constrained_field>

<constrained_field> : <id> ":" <expr> <opt_expr_pipeline> ";"

<expr> : /(["'])(?:\\?.)*?\1/             ; quoted string literals
       | /(\.[0-9]+|[0-9]+\.[0-9]*)/      ; float literals
       | /[0-9]+/                         ; integer literals
       | "true"
       | "false"
       | "self"                           ; current resource state
       | "value"                          ; current submitted value
       | "selves"                         ; all resources of that type
       | <id> <opt_property> <opt_call>

<opt_expr_pipeline> : "|>" <expr> <opt_expr_pipeline>



;; Common expressions
<id> : /[a-z][a-z0-9_]*/

<name> : <opt_prefix> /[A-Z][A-Z0-9]*/

<opt_prefix> : <id> "."
             | ""

<opt_property> : "." <id> <opt_property>
               | ""

<opt_call> : <opt_params> <call>
           | ""

<call> : "(" <expr> <opt_arg_list> ")"

<opt_arg_list> : "," <expr> <opt_arg_list>
               | ""

<opt_params> : "[" <id> <opt_param_list> "]"
             | ""

<opt_param_list> : "," <id> <opt_param_list>
                 | ""
```

::::
