# Managing Reactive Mutations

There are two books that have an outsized impact on the design of the RESTrict
Framework's reactive mutation nature.

* [Reactive Design
  Patterns](https://www.manning.com/books/reactive-design-patterns)
  by Roland Kuhn with Brian Hanafee and Jamie Allen
* [Designing Data-Intensive
  Applications](https://www.oreilly.com/library/view/designing-data-intensive-applications/9781491903063/)
  by Martin Kleppmann

The word "asynchronous" is used a lot in Web programming. Let's make it very
clear with respect to the way the RESTrict Framework uses it.

:::{admonition} Asynchronous mutations
:class: important
All state modifications sent from the client are immediately accepted for
processing. The processing occurs _after_ the accepted response is sent to 
the client. The outcome of the processing is provided in a separate message.
:::

To understand how this occurs, the next page describes how the RESTrict
Framework handles client interactions.
