# Publishing State

:::{admonition} Code subject to change
:class: danger
The RESTrict Framework is currently in its early design and development
phase. The following protocol description will change.
:::

When the state of a resource is published to the client or other services,
the protocol has the following definition for a message.

* An entry indicating what kind of mutation has occurred (created, modified,
  deleted)
* The type of resource the message represents
* The URI for the resource
* For each attribute in the resource:
  * The name of the attribute
  * The type of the attribute
  * The value of the attribute
    * If the value is scalar value, then it is just that value
    * If the value is another resource, then it is a recursive definition OR a
      URI for the resource
  * A cryptographic hash of the value of the attribute

When the state of the resource is requested by a client, the protocol has the 
following definition for a message.

* The type of resource the message represents
* The URI for the resource
* For each attribute in the resource:
  * The name of the attribute
  * The type of the attribute
  * The value of the attribute
    * If the value is scalar value, then it is just that value
    * If the value is another resource, then it is a recursive definition OR a
      URI for the resource
  * A cryptographic hash of the value of the attribute

Novel to the RESTrict Framework is a cryptographic has of the attriute value. This
is done to allow non-persistent store detection of changed values.
