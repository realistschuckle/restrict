# Concepts

The RESTrict Framework bases its design on three major ideas.

* Resource declaration guided by a well-defined and robust modeling
  methodology
* Processing application state mutations should occur asynchronously in a
  reactive system
* Automatic syndication of state changes through a well-defined message 
  protocol

This section of the documentation describes the motivation for the RESTrict
Framework and the different pieces that go together to make it work.

## Not a silver bullet

The RESTrict Framework is _not_ easily applicable to all Web applications.
This is similar to Web frameworks like Ruby on Rails. As long as you can build
your application within its assumptions, it is _very_ easy to write a Rails 
app. The moment that you want to deviate outside of its assumptions, it can 
become prohibitively difficult.

The biggest obstacle people will face when using the RESTrict Framework is 
thinking in terms of **resources** and **side effects**. It takes a real effort
to change the way programmers approach problems. The RESTrict Framework is 
unrelenting in its demands of the programmer to do that.

## Who this is for

The RESTrict Framework is for programmers that have some of the following kind
of experience.

* Lots of "modeling in color" experience
* Lots of data modeling experience 
* Lots of systems design based on microservices
