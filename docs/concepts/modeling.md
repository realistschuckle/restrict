# Modeling RESTrict Resources

The RESTrict Framework is a hybrid functional and declarative programming
environment. This page discusses the trade-offs of a _functional-declarative
resource-oriented_ (FDRO) development environment. Once those are addressed,
this page dives into the modeling methodology that the RESTrict Framework
uses to allow you to build robust Web applications.

## Perceived trade-offs

This is a list of how traditional Web programming compares with FDRO.

Procedural programming vs modeling resources
: Unless you have had the pleasure of only programming Haskell your entire
  career, you've had to think procedurally. Object-oriented programming is
  procedural programming organized differently. Modeling resources is about
  changing the state of an application through the creation of new data,
  not about writing `for` loops and setting object fields.

Unrestricted design vs domain-neutral components
: Experience shows that programmers tend to like to have options about how
  to implement things. They like to start with a blank whiteboard and draw
  systems and software designs, growing them into existence. Domain-neutral
  component design takes a different approach. Instead, you begin with a
  structure and _remove_ the parts you don't need. The form helps programmers
  decide the function.

Monoliths vs reactive systems
: Web applications need to be responsive to the people using them. This means
  that the application should respond to input in a timely manner _all the
  time_. Monoliths, while easier to maintain, do not provide that type of
  assurance. The microservice revolution helped with popularizing reactive
  system. The RESTrict Framework makes this easy because all application
  state mutations occur in asynchronous processes _and_ provides a holistic
  view of how the application works.

## Methodology

The modeling method used by the RESTrict Framework is based on the design
method called
[Object Modeling in Color](https://en.wikipedia.org/wiki/Object_Modeling_in_Color)
created by Peter Coad, Eric Lefebvre, and Jeff De Luca as part of the
[Feature-Driven Design](https://en.wikipedia.org/wiki/Feature-driven_development)
agile methodology. Though the method was originally devised for
object-oriented programming, it is a natural and easy fit for
resource-oriented design.

There are eight archetypes of resources categorized into four colors.
(The  colors inspired the name "object modeling in color".) This section
introduces the concept of the four categories and the eight archetypes.

To introduce them, we'll use the example of the blog resources on the
[main page](blog-resources).

### Pink resources: moments, intervals, and details

:::{epigraph}
Something that one needs to work with and track for business or legal
reasons, something that occurs at a moment in time or over an interval
of time.

-- Peter Coad, Java Modeling in Color with UML
:::

When you write a Web application, there are data that define the actual
reason for the application to exist. In time tracking software, important
resources would include the _interval that you spend time working on a
project_ and the _invoice that defines an interval of time that you
spent time working on stuff for a client_.

Stephen R. Palmer offers some valuable advice on how to identify moments
and intervals in your application's resource domain.

> When looking for Moment-Interval classes for a particular piece of
> software, we need to consider things like significant events and
> activities, business transactions, steps in a process, or interactions
> within a business relationship that are within the scope [of] our software
> system, application, or component.

In the blog application, we'll start with the two most easily identifiable
resources, the `Post` and the `Comment`. These **define** the workflow of
the application.

1. A person makes a `Post` which is a moment in time, the time it is
   published
1. Other people each make a `Comment` related to the `Post` (or they don't)
   which is a moment in time, the time the comment is submitted

The `Post` **must** come before a `Comment`. Hence, a workflow.

:::{graphviz}
:caption: The `Post` and `Comment` resources in relation to one another
digraph G {
    rankdir=LR;
    node [fontname="Courier" shape=rect];
    edge [fontname="Courier"];
    fontname="Courier";
    nodesep=1.0;
    Post [fillcolor=pink style=filled];
    Comment [fillcolor=pink style=filled];
    Post -> Comment [headlabel="*" taillabel="1"]
}
:::

### Yellow resources: roles

:::{epigraph}
A role is a way of participation by a part (person or organization), place,
or thing. We like this... since many times a person or an organization is
eligible to play the same role (for example, owner) within a problem domain.

-- Peter Coad, Java Modeling in Color with UML
:::

Any software application that maintains security uses the word "role"
to define a set of permissions needed to perform an action. This is a
specific way of modeling that particular abstraction.

The interesting thing about roles in this modeling methodology is that they
can apply to persons, places, and things (which we'll get to next).

In a blogging application, we can make the decision that any person can make
a `Comment` on a `Post`. There's no particular role that needs to be
associated with someone that makes a `Comment`.

To make a `Post`, a person should have some kind of role. In our application,
let's call that role the `Writer` role.

:::{graphviz}
:caption: The `Writer` role must exist to create a `Comment`
digraph G {
    rankdir=LR;
    node [fontname="Courier" shape=rect style=filled];
    edge [fontname="Courier"];
    fontname="Courier";
    nodesep=1.0;
    Writer [fillcolor=yellow];
    Post [fillcolor=pink];
    Comment [fillcolor=pink];
    Post -> Comment [headlabel="*" taillabel="1"]
    Writer -> Post [headlabel="1" taillabel="1"];
    {rank=same; Post; Writer;}
}
:::

Now, we should have some questions about `Writer`.

* How does the `Writer` role get assigned?
* Is it important for our application to track the assignment for business or
  legal reasons?
* Can it be revoked?

Let's say the answers are "by an admin," "yes," and "yes." We'll defer the "by
the admin" part to later in this exercise. The answer "yes" to the second
question means that the assignment of the `Writer` role needs to be a moment
or an interval. The answer "yes" to the third question helps us identify that
it is an interval since it can be revoked. Therefore, we can introduce a new
interval into the design.

:::{graphviz}
:caption: A `WriterAssignment` creates a `Writer` role
digraph G {
    rankdir=LR;
    node [fontname="Courier" shape=rect style=filled];
    edge [fontname="Courier"];
    fontname="Courier";
    nodesep=1.0;
    WriterAssignment [fillcolor=pink];
    Writer [fillcolor=yellow];
    Post [fillcolor=pink];
    Comment [fillcolor=pink];
    WriterAssignment -> Writer [headlabel="1" taillabel="1"];
    Post -> Comment [headlabel="*" taillabel="1"];
    Writer -> Post [headlabel="1" taillabel="1"];
    {rank=same; Post; Writer;}
}
:::

### Green resources: parties, places, and things

:::{epigraph}
A party (meaning a person or an organization), place, or thing is someone or
something who plays different roles. A person might be both an employee and a
customer. A place might be both a retail outlet and a wholesale outlet. A
thing might play one role in a manufacturing process and a different role in
a purchasing process.

-- Peter Coad, Java Modeling in Color with UML
:::

Green resources, parties, places, and things, are often the easiest thing for
object-oriented programmers to identify. They've been trained to "find nouns"
in their problem domain and turn them into classes.

Quite often, green resources are some kind of "reference resources", resources
that exist across the entire problem domain. Usually, the same people are
actors across the entire system, creating and accessing data, interacting with
the system.

When we diagram green resources, we will often duplicate them to show how they
interact with moments, intervals, and details. That does not mean that they
are the same occurrence of the resource. Instead, they're added for clarity.
If you need to specify that two resources are the same occurrence, you can use
the same method as the UML does: give the resource a label of its occurrence.

In our domain, we have people writing `Post`s and `Comment`s. So, let's add
a `Person` resource to the diagram.

:::{graphviz}
:caption: Add the `Person` resource and its relationships
digraph G {
    rankdir=LR;
    node [fontname="Courier" shape=rect style=filled];
    edge [fontname="Courier"];
    fontname="Courier";
    nodesep=1.0;
    Person1 [fillcolor=lightgreen label="a:Person"];
    Person2 [fillcolor=lightgreen label="Person"];
    Person3 [fillcolor=lightgreen label="a:Person"]
    WriterAssignment [fillcolor=pink]
    Writer [fillcolor=yellow];
    Post [fillcolor=pink];
    Comment [fillcolor=pink];
    WriterAssignment -> Writer [headlabel="1" taillabel="1"];
    Post -> Comment [headlabel="*" taillabel="1"]
    Writer -> Post [headlabel="1" taillabel="1"];
    Person1 -> WriterAssignment [headlabel="1" taillabel="1"];
    Person2 -> Comment [headlabel="?" taillabel="1"];
    Person3 -> Writer [headlabel="1" taillabel="1" style=dashed]
    {rank=same; Post; Writer; Person3;}
    {rank=same; Person1; WriterAssignment;}
    {rank=same; Person2; Comment;}
}
:::

You can see two `Person` boxes with the `a:` label. Those refer to the 
same occurrence. If Noor is the `Person` participating in the 
`WriterAssignment`, then Noor is the `Person` associated to the 
`Writer` that comes from the `WriterAssignment`.

The dashed line indicates that the `Person` with the `Writer` role is 
calculated from a previous relationship. In this case, it's the `Writer → WriterAssignment → Person` calculation.

### Blue resources: descriptions

:::{epigraph}
[Description is] a catalog-entry-like description. It is a collection of
values that apply again and again. It also provides behavior across the
collection of all things that correspond to its description.

-- Peter Coad, Java Modeling in Color with UML
:::

The RESTrict Framework uses descriptions as a way to provide behavior 
that is inherent to the framework outside of the scope of resource domain.

One example of this is for authentication. Authentication is an 
orthogonal concern to what a Web application does. It adds access to the 
data and actions available to an actor in the system; it rarely provides
application-specific behavior.

The RESTrict Framework provides a `PasswordAccountDescription` resource 
that provides password credentials.

:::{graphviz}
:caption: Add password account behavior for the `Person`
digraph G {
    rankdir=LR;
    node [fontname="Courier" shape=rect style=filled];
    edge [fontname="Courier"];
    fontname="Courier";
    nodesep=1.0;
    PasswordAccountDescription1 [fillcolor=deepskyblue label="PasswordAccountDescription"]
    PasswordAccountDescription2 [fillcolor=deepskyblue label="PasswordAccountDescription"]
    Person1 [fillcolor=lightgreen label="Person"];
    Person2 [fillcolor=lightgreen label="Person"];
    Person3 [fillcolor=lightgreen label="Person"];
    WriterAssignment [fillcolor=pink]
    Writer [fillcolor=yellow];
    Post [fillcolor=pink];
    Comment [fillcolor=pink];
    WriterAssignment -> Writer [headlabel="1" taillabel="1"];
    PasswordAccountDescription1 -> Person1 [headlabel="*" taillabel="1"];
    PasswordAccountDescription2 -> Person2 [headlabel="*" taillabel="1"];
    Post -> Comment [headlabel="*" taillabel="1"]
    Person3 -> Writer [headlabel="1" taillabel="1" style=dashed];
    Writer -> Post [headlabel="1" taillabel="1"];
    Person1 -> WriterAssignment [headlabel="1" taillabel="1"];
    Person2 -> Comment [headlabel="?" taillabel="1"];
    {rank=same; Post; Writer; Person3;}
    {rank=same; Person1; WriterAssignment; PasswordAccountDescription1}
    {rank=same; Person2; Comment; PasswordAccountDescription2}
}
:::

## The domain-neutral component

:::{epigraph}

How do you make a statue of an elephant? Get the biggest granite block you 
can find and chip away everything that doesn’t look like an elephant.

-- Boy's Life, December 1963
:::

To follow this modeling method, we recommend the following steps.

1. Identify the moments and intervals in the resource domain 
1. For each moment and interval, apply the domain-neutral component 

The domain neutral component provides a structure that you identify
the pieces that you need, then remove the stuff you don't. That's 
often much nicer than using the "blank page" method.

For example, let's look at the `Post` moment. Once we figured out that the 
`Post` moment exists, we could plop it into the middle of the domain-neutral
component template.

:::{graphviz}
:caption: Domain-neutral component with the `Post` moment
digraph G {
    rankdir=LR;
    node [fontname="Courier" shape=rect style=filled];
    edge [fontname="Courier"];
    fontname="Courier";
    nodesep=0.5;

    Desc1 [fillcolor=deepskyblue label="Description"];
    Desc2 [fillcolor=deepskyblue label="Description"];
    Desc3 [fillcolor=deepskyblue label="Description"];
    PPT1 [fillcolor=lightgreen label="P/P/T"];
    PPT2 [fillcolor=lightgreen label="P/P/T"];
    PPT3 [fillcolor=lightgreen label="P/P/T"];
    Role1 [fillcolor=yellow label="Role"];
    Role2 [fillcolor=yellow label="Role"];
    Role3 [fillcolor=yellow label="Role"];

    Next [fillcolor=pink];
    Previous [fillcolor=pink ];
    Post [fillcolor=pink];
    Detail [fillcolor=pink];

    Desc1 -> PPT1 -> Role1;
    Desc2 -> PPT2 -> Role2;
    Role1 -> Post;
    Post -> Role2 [dir=back];
    Post -> Detail [dir=back];
    Detail -> Role3 [dir=back];
    Role3 -> PPT3 [dir=back];
    PPT3 -> Desc3 [dir=back];

    Previous -> Post -> Next [style=dashed];

    edge [style="invis"];
    Role2 -> Next;

    {rank=same; Post; Detail; Role3; PPT3; Desc3; }
    {rank=same; PPT1; Role1; Desc1; }
    {rank=same; PPT2; Role2; Desc2; Next; }
}
:::

Now that we have the `Post` in the middle, we can identify the 
following participants in the moment.

* It has no _Previous_ moment or interval
* The _Next_ moment or interval is the `Comment`
* It has one participant, the `Writer` role 
* It has no moment or interval details that make up the `Post`

:::{graphviz}
:caption: Fill in known items
digraph G {
    rankdir=LR;
    node [fontname="Courier" shape=rect style=filled];
    edge [fontname="Courier"];
    fontname="Courier";
    nodesep=0.5;

    Desc1 [fillcolor=deepskyblue label="PasswordAccountDescription"];
    Desc2 [style="dotted" label="Description"];
    Desc3 [style="dotted" label="Description"];
    PPT1 [fillcolor=lightgreen label="Person"];
    PPT2 [style="dotted" label="P/P/T"];
    PPT3 [style="dotted" label="P/P/T"];
    Role1 [fillcolor=yellow label="Writer"];
    Role2 [style="dotted" label="Role"];
    Role3 [style="dotted" label="Role"];

    Next [fillcolor=pink label="Comment"];
    Previous [style="dotted"];
    Post [fillcolor=pink];
    Detail [style="dotted"];

    Desc1 -> PPT1 -> Role1;
    Desc2 -> PPT2 -> Role2 [style=dotted];
    Role1 -> Post;
    Post -> Role2 [dir=back style=dotted];
    Post -> Detail [dir=back style=dotted];
    Detail -> Role3 [dir=back style=dotted];
    Role3 -> PPT3 [dir=back style=dotted];
    PPT3 -> Desc3 [dir=back style=dotted];

    Previous -> Post [style=dotted];
    Post -> Next [style=dashed xlabel="next"];

    edge [style="invis"];
    Role2 -> Next;

    {rank=same; Post; Detail; Role3; PPT3; Desc3; }
    {rank=same; PPT1; Role1; Desc1; }
    {rank=same; PPT2; Role2; Desc2; Next; }
}
:::

Finally, erase the stuff you don't need.

:::{graphviz}
:caption: Final DNC for `Post`
digraph G {
    rankdir=LR;
    node [fontname="Courier" shape=rect style=filled];
    edge [fontname="Courier"];
    fontname="Courier";
    nodesep=0.5;

    Desc1 [fillcolor=deepskyblue label="PasswordAccountDescription"];
    PPT1 [fillcolor=lightgreen label="Person"];
    Role1 [fillcolor=yellow label="Writer"];

    Next [fillcolor=pink label="Comment"];
    Post [fillcolor=pink];

    Desc1 -> PPT1 -> Role1;
    Role1 -> Post;

    Post -> Next [style="dashed" xlabel="next"];

    {rank=same; PPT1; Role1; Desc1; Post; }
    {rank=same; Next; }
}
:::

