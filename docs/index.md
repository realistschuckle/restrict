# RESTrict Framework

The RESTrict Framework is a different way to think about constructing Web
applications. It defines a new paradigm, **functional-declarative hybrid,
resource-oriented** development, a hybrid of declarative and functional
programming. It is the hope that this new paradigm engenders a new mental
model for Web applications, one that more naturally fits the _hypertext as
the engine of application state_.

::::{grid} 1 1 2 3
:gutter: 3

:::{grid-item-card} Langauge Definition
:link: references/language
:link-type: doc

Check out the EBNF version of the RESTrict Framework's resource definition
files.
:::

:::{grid-item-card} Hello World!
:link: examples/blog
:link-type: doc

Review how to create a simple blog backend.
:::

:::{grid-item-card} Rationale
:link: concepts/index
:link-type: doc

Why do we need yet another framework? Have a read about why the RESTrict
Framework was created.
:::
::::
