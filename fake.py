from typing import Any


class RestrictRuntimeError(RuntimeError): ...


class RestrictRuntimeErrorGroup(RuntimeError): ...


class AppResource: ...


fields = {"_Thing__a": {}, "_Thing__b": {}}
eval_order_name = "_Thing__$eval_order"


class Thing(AppResource):
    def __init__(self, state={}):
        setattr(self, eval_order_name, ["_Thing__b", "_Thing_b"])
        for name, spec in fields.items():
            setattr(self, name, spec)
        for name, value in state.items():
            setattr(self, name, value)

    @staticmethod
    def create(state: dict[str, Any]):
        self = Thing()
        self.__set_state_and_validate(state)
        return self

    @staticmethod
    def modify(old_state: dict[str, Any], state: dict[str, Any]):
        self = Thing(old_state)
        self.__set_state_and_validate(state)
        return self

    def __set_state_and_validate(self, state: dict[str, Any]):
        for name, value in state.items():
            setattr(self, name, value)
        errors = []
        for name in getattr(self, eval_order_name):
            try:
                getattr(self, name).run_constraints()
            except RestrictRuntimeError as rre:
                errors.append(rre)
        if len(errors) > 0:
            raise RestrictRuntimeErrorGroup(errors)

    @property
    def b(self):
        return getattr(self, "_Thing__b").get()

    @b.setter
    def b(self, value):
        getattr(self, "_Thing__b").set(value)

    @property
    def a(self):
        return getattr(self, "_Thing__a").get()
